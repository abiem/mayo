# Project Mayo

## Source Control Guidelines

1. Commits should be made on master or on your own branch(es); never push to someone else's branch unless you have agreement to do so

2. Try to keep distinct fixes/features or logical chunks of work in separate commits. This makes it easier to drop/pick commits later, if say we don't want to add feature X but want to keep feaure Y. It also makes it easier to find out where a bug got added.

3. Recommended reading on writing commit messages: https://chris.beams.io/posts/git-commit/

## iOS app on-boarding

This app uses Cocoapods for dependencies. This repo does not store the cocoapod dependency code (i.e. the folder `Pods/`) - it just stores the Podfile and Podfile.lock.

On first checkout of this project OR if you switch to a branch that has different pod dependencies, you may have to run `pod install` in the Mayo directory. It's also worth periodically running `pod repo update` (before running `pod install`).

Possible improvement: we could write a small shell script run action to check if `pod install` is necessary during build (and prompt the user if it is).

## Other docs in this repo

* [Allen's Project Mayo info](project.README.md)
