//
//  Mayo_ios_clientTests.swift
//  Mayo-ios-clientTests
//
//  Created by Daniel Lee on 4/19/20.
//  Copyright © 2020 abiem design. All rights reserved.
//

import XCTest
@testable import Mayo_ios_client
import Firebase

class AuthServiceFirebaseImplTests: XCTestCase, AuthServiceDelegate {
    
    
    fileprivate let authService = AuthServiceFirebaseImpl()
    fileprivate var signInError: Error? // sign in user error
    fileprivate var user: AuthUser? // sign in user result
    fileprivate var signInExpactation: XCTestExpectation?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        FirebaseApp.configure()
        authService.delegate = self
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        signInError = nil
        user = nil
    }

    /**
     Given a auth service with Firebase implementation
     When user do signin anonymously
     Then it should provide a AuthUser with auth token
     */
    func test_loginAnonymously() throws {
        // Given
        let expectation = XCTestExpectation(description: "Signing into Firebase Async")
        signInExpactation = expectation
        
        //When
        authService.signInAnonymously()
        
        //Then
        wait(for: [signInExpactation!], timeout: 10, enforceOrder: true)
        
        // Given known id
        authService.signOut()
        let secondSignInExpactation = XCTestExpectation(description: "Signing into Firebase Async second trial")
        signInExpactation = secondSignInExpactation
        let id = self.user!.uid
        
        //When
        authService.signInAnonymously()
        
        //Then
        wait(for: [signInExpactation!], timeout: 10, enforceOrder: true)
        let secondLoginUserId = self.user!.uid
        XCTAssertNotEqual(id, secondLoginUserId)
    }

//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

    // MARK: - AuthServiceDelegate
    func signInCompletion(authService: AuthServiceProtocol, user: AuthUser?, error: Error?){
        self.user = user
        
        XCTAssertNotNil(user?.uid)
        XCTAssertNotNil(user?.token)
        XCTAssertNil(error)
        
        signInExpactation?.fulfill()
    }
}
