//
//  QuestTests.swift
//  Mayo-ios-clientUITests
//
//  Created by Eric Wolak on 4/28/19.
//  Copyright © 2019 abiem design. All rights reserved.
//

import XCTest

class QuestTests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launchArguments.append("-onboardingHasBeenShown")
        app.launchArguments.append("true")
        app.launchArguments.append("-onboardingTask1Viewed")
        app.launchArguments.append("true")
//        app.launchArguments.append("-onboardingTask2Viewed")
//        app.launchArguments.append("true")
        app.launchArguments.append("-onboardingTask3Viewed")
        app.launchArguments.append("true")
        app.launchArguments.append("-showNotificationRequestForTesting")
        app.launchArguments.append("true")
        app.launch()
    }

    func testPosting() {
        XCTAssertTrue(app.staticTexts["task_description1"].isHittable)
        app/*@START_MENU_TOKEN@*/.staticTexts["task_description1"]/*[[".otherElements[\"quests\"]",".staticTexts[\"So our AI is a bit bored, help us by sending a message!\"]",".staticTexts[\"task_description1\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.swipeRight()
        
        app.textViews["task_description_entry"].tap()
        app.textViews["task_description_entry"].typeText("this is a test")
        app.buttons["post"].tap()
        // TODO: more assertions about the task actually getting posted
    }
}
