//
//  OnboardingTests.swift
//  Mayo-ios-clientUITests
//
//  Created by Eric Wolak on 3/24/19.
//  Copyright © 2019 Factor13 LLC. All rights reserved.
//

import XCTest

class OnboardingTests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        continueAfterFailure = false
    }

    func testTutorial() {
        app = XCUIApplication()
        app.launchArguments.append("-onboardingHasBeenShown")
        app.launchArguments.append("false")
        app.launch()
        
        let carousel = app.otherElements["carousel"]
        carousel.swipeLeft()
        carousel.swipeLeft()
        carousel.swipeLeft()
        app.buttons["Got it"].tap()
        
        app.terminate()
    }
    
    func testNotificationPermissionOnThirdCard() {
        app = XCUIApplication()
        app.launchArguments.append("-onboardingHasBeenShown")
        app.launchArguments.append("true")
        app.launchArguments.append("-onboardingTask1Viewed")
        app.launchArguments.append("false")
        app.launchArguments.append("-onboardingTask2Viewed")
        app.launchArguments.append("false")
        app.launchArguments.append("-onboardingTask3Viewed")
        app.launchArguments.append("false")
        app.launch()
        
        let carousel = app.otherElements["quests"]
        carousel.swipeLeft()
        carousel.swipeLeft()
        
        let button = app.buttons["Sure"]
        let exists = NSPredicate(format: "exists == 1")
        
        expectation(for: exists, evaluatedWith: button, handler: nil)
        waitForExpectations(timeout: 1, handler: nil)

        app.buttons["Sure"].tap()
        app.terminate()
    }
}
