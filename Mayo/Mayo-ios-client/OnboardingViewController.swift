//
//  OnboardingViewController.swift
//  Mayo-ios-client
//
//  Created by Lakshmi Kodali on 20/12/17.
//  Copyright © 2017 Weijie. All rights reserved.
//

import APNGKit
import CoreLocation
import Firebase
import FLAnimatedImage
import iCarousel
import QuartzCore
import UIKit

class OnboardingViewController: UIViewController {
    // Outlets
    @IBOutlet var mBackgroundAnimation: UIImageView!
    @IBOutlet var mCarousel: iCarousel!
    //  @IBOutlet weak var mImageView: FLAnimatedImageView!
    @IBOutlet var mImageView: APNGImageView!
    // Instance
    var playingThanksAnim = false
    var locationManager: CLLocationManager!
    let mThanksImageListArray: NSMutableArray = []
    let mSecondPinImageListArray: NSMutableArray = []
    let mThirdPinImageListArray: NSMutableArray = []
    var thanksImage: APNGImage?
    // rotation key animation
    let kRotationAnimationKey = "com.mayo.rotationanimationkey"

    // CAlayerAnimation
    let animation = CAKeyframeAnimation()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadGifImages()
      mCarousel.isScrollEnabled = false

			if UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) != nil {
				checkFakeTaskStatus()
			}
			
//        if UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) == nil {
//            Auth.auth().signInAnonymously { user, error in
//                if error != nil {
//                    print("an error occured during auth")
//                    return
//                }
//
//							let uid = user!.user.uid
//							let defaults = UserDefaults.standard
//							if let fcmToken = defaults.object(forKey: "fcmToken") {
//								Database.database().reference().child("users/\(uid)/deviceToken").setValue(fcmToken)
//							}
//							self.checkFakeTaskStatus()
//            }
//        } else {
//            checkFakeTaskStatus()
//        }
    }

    override func viewDidAppear(_ animated: Bool) {
      // should be called when back from safari view controller? but not :(
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        self.showUserThankedAnimation()
      }
    }

    func loadGifImages() {
        let path = Bundle.main.path(forResource: "bump", ofType: "png")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!))
            thanksImage = APNGImage(data: data, progressive: false)

        } catch {
            print("Something went Wrong")
        }
    }

    func checkFakeTaskStatus() {
			if let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)
			{
				Database.database().reference().child("users").child(userId).child("demoTaskShown").observeSingleEvent(of: .value, with: { snapshot in
            if let isIntroTasksDone = snapshot.value as? Bool {
                if isIntroTasksDone == true {
                  let defaults = UserDefaults.standard
                  defaults.set(false, forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.shown)
                  defaults.set(false, forKey: Constants.SAMPLE_POST.SWIPE_LEFT.shown)
                  defaults.set(false, forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.shown)
                  defaults.set(false, forKey: Constants.SAMPLE_POST.HISTORY_TITLE.shown)
                  
                  defaults.set(false, forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed)
                  defaults.set(false, forKey: Constants.SAMPLE_POST.SWIPE_LEFT.viewed)
                  defaults.set(false, forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.viewed)
                  defaults.set(false, forKey: Constants.SAMPLE_POST.HISTORY_TITLE.viewed)

              }
            }
        })
			}
    }

    // Mark :- location Permission
    func askForLocationAuth() {
//        if CLLocationManager.locationServicesEnabled() {
//            // if location services are enables, ask for
//            // access to location
//            locationManager = CLLocationManager()
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.allowsBackgroundLocationUpdates = true
//
//            if CLLocationManager.authorizationStatus() != .authorizedAlways {
//                locationManager.requestAlwaysAuthorization()
//            } else if CLLocationManager.locationServicesEnabled() {
//                locationManager.startUpdatingLocation()
//            }
//        } else {
//            gotoMainViewController()
//        }
			UserLocation.shared.requestAlwaysAuthorization(callback: { (status: CLAuthorizationStatus) -> Void in
				self.gotoMainViewController()
			})
    }

    // Mark :- Navigate
    func gotoMainViewController() {
			let MainViewController = storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let navViewController = UINavigationController(rootViewController: MainViewController)
      navViewController.modalPresentationStyle = .fullScreen
        present(navViewController, animated: true, completion: nil)

        // set onboarding has been shown to true
        // inside user defaults
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "onboardingHasBeenShown")
        Analytics.logEvent(AnalyticsEventTutorialBegin, parameters: [:])
    }

    func sequenceOfThanks() {
        DispatchQueue.global(qos: .background).async {
            for countValue in 1 ... 51 {
                let imageName: String = "fux00\(countValue).png"
                let image = UIImage(named: imageName)?.cgImage
                self.mThanksImageListArray.add(image!)
            }
        }
    }

    func sequenceOfSecondPin() {
        DispatchQueue.global(qos: .background).async {
            for countValue in 1 ... 71 {
                let imageName: String = "fatpin1.00\(countValue).png"
                let image = UIImage(named: imageName)?.cgImage
                self.mSecondPinImageListArray.add(image!)
            }
        }
    }

    func sequenceOfThirdPin() {
        DispatchQueue.global(qos: .background).async {
            for countValue in 1 ... 80 {
                let imageName: String = "fatpin2.00\(countValue).png"
                let image = UIImage(named: imageName)?.cgImage
                self.mThirdPinImageListArray.add(image!)
            }
        }
    }

    // Mark :- Ripple Animation
    func showRippleAnimation(_: UIImage?, _ pBackgroundImage: UIImage) {
        mImageView.contentMode = .center
        mBackgroundAnimation.image = pBackgroundImage
        mBackgroundAnimation.transform = CGAffineTransform.identity
        mBackgroundAnimation.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        UIView.commitAnimations()
        UIView.animate(withDuration: Constants.THANKS_RIPPLE_ANIMATION_DURATION, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
            self.mBackgroundAnimation.transform = CGAffineTransform(scaleX: 5, y: 5)
        }, completion: { _ in
            self.mBackgroundAnimation.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)

        })
    }

    // Mark :- Thanks Image
    func showUserThankedAnimation() {
        if playingThanksAnim { return }
        playingThanksAnim = true
        mImageView.contentMode = .scaleAspectFit
        flareAnimation(view: mBackgroundAnimation, duration: Constants.THANKS_ANIMATION_DURATION)
        if let image = thanksImage {
            mImageView.image = image
            mImageView.startAnimating()
        }
    }

    func showSecondPinAnimation() {
        let path = Bundle.main.path(forResource: "ripple", ofType: "png")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!))
            mImageView.image = APNGImage(data: data, progressive: true)
            mImageView.startAnimating()

        } catch {
            print("Something went Wrong")
        }
    }

    func showThirdPinAnimation() {
        let path = Bundle.main.path(forResource: "pin1", ofType: "png")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!))
            let image = APNGImage(data: data, progressive: true)
            mImageView.image = image
            mImageView.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.84) {
                self.showFourthPin()
            }
        } catch {
            print("Something went Wrong")
        }
    }

    func showFourthPin() {
        let path = Bundle.main.path(forResource: "pin2", ofType: "png")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!))
            let image = APNGImage(data: data, progressive: true)
            mImageView.image = image
            mImageView.startAnimating()

        } catch {
            print("Something went Wrong")
        }
    }

    // Start Flare Animation for thanks
    func flareAnimation(view: UIView, duration: Double = 1) {
        mBackgroundAnimation.isHidden = true
        mBackgroundAnimation.image = #imageLiteral(resourceName: "flareImage")
        if view.layer.animation(forKey: kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float(.pi * 2.0)
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity
            view.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if !self.playingThanksAnim { return }
            self.mBackgroundAnimation.isHidden = false
        }
    }

    func stopAnimationAnimation() {
        mImageView.image = nil
        mImageView.stopAnimating()
        mBackgroundAnimation.layer.removeAnimation(forKey: kRotationAnimationKey)
        mBackgroundAnimation.image = nil
        playingThanksAnim = false
    }
}
