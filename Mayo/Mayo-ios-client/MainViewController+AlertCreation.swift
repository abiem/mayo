//
//  MainViewController+AlertCreation.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit

// MARK: - MainViewController (alert creation)
extension MainViewController {
    func showAlertForCompletion() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (_: UIAlertAction) in
            // This is called when the user presses the cancel button.
            print("You've pressed the cancel button")
        }
        let actionMarkAsComplete = UIAlertAction(title: "Mark post as done", style: .default) { (_: UIAlertAction) in
            // This is called when the user presses the complete button.
            self.createMarkCompleteView()
        }
        alert.addAction(actionMarkAsComplete)
        alert.addAction(actionCancel)

        present(alert, animated: true, completion: nil)
    }
    
    //alerts for when email is correct and link has been sent
    func presentRightEmailAlert(){
        let correctEmailAlert = UIAlertController(title: "Thanks for Verifying!", message: "Just tap the link in the email from Mayo and you're all set :)", preferredStyle: .alert)
        correctEmailAlert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        correctEmailAlert.addAction(UIAlertAction(title: "Open Mail", style: .default, handler: { (action) in
            if let url = URL(string: "message://"){
                if(UIApplication.shared.canOpenURL(url)){
                    UIApplication.shared.open(url)
                }
                else{
                    let cantOpenMailAppAlert = UIAlertController(title: "Can't Open the Mail App", message: "Try logging in to your email from a web browser!", preferredStyle: .alert)
                    cantOpenMailAppAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                    self.present(cantOpenMailAppAlert, animated: true, completion: nil)
                }
            }
        }))
        self.present(correctEmailAlert, animated: true, completion: nil)
    }
    
    //alerts for when email field is empty or not a valid email
    func presentWrongEmailAlert(){
        let wrongEmailAlert = UIAlertController(title: ".edu Email Required", message: "To identify your student status, please provide a .edu email.", preferredStyle: .alert)
        wrongEmailAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        wrongEmailAlert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (action) in
            self.verifyEmail()
        }))
        self.present(wrongEmailAlert, animated: true, completion: nil)
    }
    
    func pleaseTurnOnGeodataAlert() {
        let alert = UIAlertController(title: "Don't have your location", message: "Please, turn on location sharing", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}

