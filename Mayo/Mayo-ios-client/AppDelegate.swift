//
//  AppDelegate.swift
//  Mayo-ios-client
//
//  Created by abiem  on 4/8/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import IQKeyboardManagerSwift
import UIKit
import AlamofireNetworkActivityLogger

import UserNotifications
import Firebase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var ref: DatabaseReference!
    var mainVC: MainViewController!
    let gcmMessageIDKey = "gcm.message_id"
    
    var authService: AuthServiceProtocol = AuthServiceFirebaseImpl()
    
    func application(_: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        authService.delegate = self
        
        NSLog("didFinishLaunchingWithOptions")
        NSLog("%@", launchOptions ?? "")
        if let _ = launchOptions?[UIApplication.LaunchOptionsKey.location] {
            NSLog("for location")
            
            var lastLocationsArr: [String] = [String]()
            if UserDefaults.standard.object(forKey: "last_background_updates") != nil{
                lastLocationsArr = UserDefaults.standard.object(forKey: "last_background_updates") as! [String]
            }
            lastLocationsArr.append("\(Date())")
            UserDefaults.standard.set(lastLocationsArr, forKey: "last_background_updates")
            
            UserLocation.shared.backgroundUpdate()
            //            let locationManager: CLLocationManager = CLLocationManager()
            //            locationManager.delegate = self
            //            locationManager.allowsBackgroundLocationUpdates = true
            //            locationManager.pausesLocationUpdatesAutomatically = false
            //            locationManager.startUpdatingLocation()
        }
        else{
            UserLocation.shared.foregroundUpdate()
        }
        
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 75
        
        #if DEBUG
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
        #endif
        
        // setup firebase
        FirebaseApp.configure()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(notification:)), name: NSNotification.Name(rawValue: NSNotification.Name.InstanceIDTokenRefresh.rawValue), object: nil)
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        // sign into Firebase using impl with helper so we get the side-effects like
        // storing of user id and token
        authService.signInAnonymously()
        
//        Auth.auth().signInAnonymously() { user, error in
//            print("Signed in, user = \(user), error = \(error)")
//            if error != nil {
//                print("an error occurred during auth")
//                return
//            }
//            // user is signed in
//        }
//        authService.signInAnonymously()
        
        
        // check if user has gone through the onboarding
        // and that the user has given access to
        // notifications and location
        let userDefaults = UserDefaults.standard
        let onboardingHasBeenShown = userDefaults.bool(forKey: "onboardingHasBeenShown")
        
        if onboardingHasBeenShown {
            //			Uncomment this if you need to get onboarding task for debugging
            //			let defaults = UserDefaults.standard
            //			defaults.set(false, forKey: Constants.ONBOARDING_TASK1_VIEWED_KEY)
            //			defaults.set(false, forKey: Constants.ONBOARDING_TASK2_VIEWED_KEY)
            //			defaults.set(false, forKey: Constants.ONBOARDING_TASK3_VIEWED_KEY)
            
            // if the user has given access to all of the authorizations
            // present main
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            mainVC = (storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController)
            let navViewController = UINavigationController(rootViewController: mainVC)
            window?.rootViewController = navViewController
            
        } else {
            // else present authorization
        }
        return true
    }
    
    func applicationDidEnterBackground(_: UIApplication) {
        // hide keyboard if its on the screen
        window?.endEditing(true)
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings: UNNotificationSettings) in
            if settings.authorizationStatus == UNAuthorizationStatus.notDetermined {
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = 1
                }
            } else {
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = 0
                }
            }
        }
        
        UserLocation.shared.backgroundUpdate()
        //        let mgr = CLLocationManager()
        //        mgr.delegate = self
        //        mgr.startMonitoringSignificantLocationChanges()
    }

    // based on code at https://firebase.google.com/docs/dynamic-links/ios/receive
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // TODO what here? This is referred user running app?
            // Is this block only entered if this was a dynamic link we're restoring with?
        }

        if handled {
            return true
        }
        return userActivity.webpageURL.flatMap(handleEmailVerification(withURL:))!
    }

    // based on code at https://firebase.google.com/docs/dynamic-links/ios/receive
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return application(app, open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: "")
    }

    // based on code at https://firebase.google.com/docs/dynamic-links/ios/receive
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        NSLog("G02 URL given to app = \(url)")
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            if let linkString = dynamicLink.url?.absoluteString {
                if linkString.contains(ReferralConstants.REFERRAL_URL_PARAMATER) {
                    
                    NSLog("G02 Got a deep link: \(linkString)")

                    UserDefaults.standard.setValue("", forKey: Constants.USER_WAS_REFERRED_BY_USER_ID)

                    ReferralUIPresenter.showThanks()
                    
                    return true
                }
            }
        }

        return false
    }

    //locally set current user as a verified student when email is clicked, also updates firebase
    func handleEmailVerification(withURL url: URL) -> Bool {
        let link = url.absoluteString
        
        if Auth.auth().isSignIn(withEmailLink: link) {
            
            guard let email = UserDefaults.standard.value(forKey: "email") as? String else { return false }
            Auth.auth().signIn(withEmail: email, link: link, completion: { (result, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                UserDefaults.standard.set(true, forKey: "isStudent")
                self.ref.child("users").child(Auth.auth().currentUser!.uid).child("hasVerified").setValue(true)
            })
            return true
        }
        return false
    }
    
    func applicationWillEnterForeground(_: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        UserLocation.shared.foregroundUpdate()
    }
    
    func applicationDidBecomeActive(_: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //		LocationMonitor.instance.applicationResumed()
    }
    
    func applicationWillResignActive(_: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        //		LocationMonitor.instance.applicationPaused()
    }
    
    func applicationWillTerminate(_: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //		UserLocation.shared.backgroundUpdate()
        //        if mainVC != nil {
        //            mainVC.startReceivingSignificantLocationChanges()
        //        }
    }
    
    // [START receive_message]
    func application(_: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler _: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        ref = Database.database().reference()
        if let aps = userInfo["aps"] as? NSDictionary {
            if let _ = aps["alert"] as? NSDictionary {
                if let notification_type = userInfo["notification_type"] {
                    print("notification_type = \(notification_type)")
                    
                    switch Int(notification_type as! String)! {
                    // Process Message push notificaton.
                    case Constants.NOTIFICATION_MESSAGE:
                        
                        print("Message Push Notification Clicked")
                        
                        if let senderId = userInfo["sender_id"] as? String {
                            if let currentUserId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)
                            {
                                if senderId == currentUserId {
                                    return
                                } else {
                                    processMessageNotification(userInfo: userInfo)
                                }
                            } else {
                                //                                Auth.auth().signInAnonymously { _, error in
                                //                                    if error != nil {
                                //                                        print("an error occured during auth")
                                //                                        return
                                //                                    }
                                //
                                //                                    let currentUserId = Auth.auth().currentUser?.uid
                                //                                    if senderId == currentUserId {
                                //                                        return
                                //                                    } else {
                                //                                        self.processMessageNotification(userInfo: userInfo)
                                //                                    }
                                //                                }
                            }
                        }
                        
                    // Process topic complete push notification.
                    case Constants.NOTIFICATION_TOPIC_COMPLETED:
                        
                        print("Task Completed Push Notification Clicked")
                        
                        if let channelId = userInfo["channelId"] {
                            print("channelId: \(channelId)")
                            Messaging.messaging().unsubscribe(fromTopic: "/topics/\(channelId)")
                        }
                        
                    // Process Thanks push notification.
                    case Constants.NOTIFICATION_WERE_THANKS:
                        
                        print("Thanks Push Notification Clicked")
                        
                        if mainVC != nil {
                            mainVC.showUserThankedAnimation()
                        }
                        
                    case Constants.NOTIFICATION_NEARBY_TASK:
                        processNewTaskQuery(userInfo: userInfo)
                        if let channelId = userInfo["taskID"] {
                            // Check task Detail
                            ref.child("tasks").child(channelId as! String).observeSingleEvent(of: .value, with: { snapshot in
                                if let dicTask = snapshot.value as? [String: Any] {
                                    // Check Task Status
                                    if dicTask["completed"] as! Bool == true {
                                        // Show Alert Task Expired
                                        self.taskExpireAlert()
                                        
                                    } else {
                                        self.processNewTaskQuery(userInfo: userInfo)
                                    }
                                }
                            })
                        }
                        
                    default:
                        break
                    }
                }
            }
        }
        
        print("received message in delegate")
        print("received notification \(userInfo)")
        // completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func processNewTaskQuery(userInfo: [AnyHashable: Any]) {
        if let taskID = userInfo["taskID"] {
            let state = UIApplication.shared.applicationState
            if state == .active {
                let currentViewController = getCurrentViewController()
                if currentViewController is MainViewController {
                    (currentViewController as! MainViewController).carouselScroll(taskID as! String)
                }
                return
            }
            let when = DispatchTime.now() + 5 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                let currentViewController = self.getCurrentViewController()
                if currentViewController is MainViewController {
                    (currentViewController as! MainViewController).carouselScroll(taskID as! String)
                }
            }
        }
    }
    
    func processMessageNotification(userInfo: [AnyHashable: Any]) {
        //        let state = UIApplication.shared.applicationState
        //        if state == .active {
        //
        //            // foreground
        //            return
        //        }
        
        // Get Current ViewController.
        let currentViewController = getCurrentViewController()
        
        if let channelId = userInfo["channelId"] {
            // Check task Detail
            ref.child("tasks").child(channelId as! String).observeSingleEvent(of: .value, with: { snapshot in
                if let dicTask = snapshot.value as? [String: Any] {
                    // Check Task Status
                    var taskComplete = false
                    if let taskStatus = dicTask["completed"] as? Bool {
                        taskComplete = taskStatus
                    }
                    if let task_description = userInfo["task_description"] {
                        var chatVC: ChatViewController!
                        var needToPush = false
                        if currentViewController is MainViewController {
                            (currentViewController as! MainViewController).carouselScroll(channelId as! String)
                            // Move to ChatViewController.
                            chatVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController)
                            needToPush = true
                        } else if currentViewController is ChatViewController {
                            self.getNavigationController()?.popViewController(animated: false)
                            chatVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController)
                            needToPush = true
                        }
                        
                        if chatVC == nil {
                            chatVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController)
                            needToPush = true
                        }
                        
                        chatVC.title = task_description as? String
                        chatVC.channelTopic = task_description as? String
                        chatVC.channelId = channelId as? String
                        chatVC.isCompleted = taskComplete
                        
                        self.ref = Database.database().reference()
                        let channelsRef = self.ref?.child("channels")
                        
                        if let chatChannelRef = channelsRef?.child(channelId as! String) {
                            chatVC.channelRef = chatChannelRef
                            if needToPush == true {
                                self.getNavigationController()?.pushViewController(chatVC, animated: true)
                            }
                            
                        } else {
                            //                            Auth.auth().signInAnonymously { _, error in
                            //                                if error != nil {
                            //                                    print("an error occured during auth")
                            //                                    return
                            //                                }
                            //
                            //                                self.ref = Database.database().reference()
                            //                                let channelsRef = self.ref?.child("channels")
                            //                                if let chatChannelRef = channelsRef?.child(channelId as! String) {
                            //                                    chatVC.channelRef = chatChannelRef
                            //                                    if needToPush == true {
                            //                                        self.getNavigationController()?.pushViewController(chatVC, animated: false)
                            //                                    } else {
                            //                                        // chatVC.reloadChat()
                            //                                    }
                            //                                }
                            //                            }
                        }
                    }
                    if taskComplete {
                        self.taskExpireAlert()
                    }
                }
                
            })
        }
    }
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.saveFcmToken(fcmToken: result.token)
            }
        }
    }
    
    func saveFcmToken(fcmToken: String){
        let defaults = UserDefaults.standard
        defaults.setValue(fcmToken, forKey: "fcmToken")
        
        if let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
            // save device token for push notifications
            Database.database().reference().child("users/\(userId)/deviceToken").setValue(fcmToken)
        }
    }
    
    
    //	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //		print("APNs token retrieved: \(deviceToken)")
    //
    //		Messaging.messaging().apnsToken = deviceToken as Data
    //	}
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("fcmToken eror: \(error)")
    }
    
    // Returns the most recently presented UIViewController (visible)
    func getCurrentViewController() -> UIViewController? {
        // If the root view is a navigation controller, we can just return the visible ViewController
        if let navigationController = getNavigationController() {
            return navigationController.visibleViewController
        }
        
        // Otherwise, we must get the root UIViewController and iterate through presented views
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            
            // Each ViewController keeps track of the view it has presented, so we
            // can move from the head to the tail, which will always be the current view
            while currentController.presentedViewController != nil {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }
    
    // Returns the navigation controller if it exists
    func getNavigationController() -> UINavigationController? {
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController {
            return navigationController as? UINavigationController
        }
        return nil
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    // shows notifications when app is in the foreground
    func userNotificationCenter(_: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let currentViewController = getCurrentViewController()
        let notificationTitle = notification.request.content.title
        
        // TODO: if current view controller is not nil
        if let viewController = currentViewController {
            // if the current user is in main view controller
            // and the user was thanked, show the you were thanked animation
            if viewController is MainViewController, notificationTitle == "You were thanked!" {
                let mainViewController = viewController as! MainViewController
                mainViewController.showUserThankedAnimation()
            }
            
            // if current view controller is in a chat view controller
            if viewController is ChatViewController {
                // if the current chat view controller is the same id as the current notification's id
                // don't show the notification
                
                // get data from notification for the channel id that the notification was sent from
                if let channelId = notification.request.content.userInfo["channelId"] {
                    let chatViewController = viewController as! ChatViewController
                    let channelIdString = channelId as! String
                    
                    // if current user is in the same channel as where the notification was sent from
                    if let chatChannelId = chatViewController.channelId {
                        // don't send a message
                        if channelIdString == chatChannelId {
                            return
                        } else {
                            // if current user is in a different channel than where the notification was sent from
                            // send a system notification to the other channel
                            completionHandler([.alert, .badge, .sound])
                            return
                        }
                    }
                }
            } else {
                // if the user is on the home screen
                // always show the system notification and bring the user to the correct task based on the notification
                completionHandler([.alert, .badge, .sound])
            }
        }
    }
    
    //    MARK: - location delegates
    
    // Application is in terminated
    //    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //        let usersGeoFire = GeoFire(firebaseRef: Database.database().reference().child("users_locations"))
    //        if locations.count > 0 {
    //			if let uid = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
    //				usersGeoFire.setLocation(locations.last!, forKey: uid)
    //			}
    //        }
    //    }
    //
    //    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
    //        print("Error from location manager: \(error)")
    //    }
    
    // Task Expire Alert
    func taskExpireAlert() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            CMAlertController.sharedInstance.showAlert(subtitle: Constants.sTASK_EXPIRED_ERROR,
                                                       buttons: ["OK"])
        }
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        
        print("fcmToken: \(fcmToken)")
        self.saveFcmToken(fcmToken: fcmToken)
    }
    
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

// referral link fetching
extension AppDelegate {
    func fetchReferralLink() {
        let referralLinkFetcher = ReferralLinkFirebaseImpl()

        //result is type (Result<String>)
        referralLinkFetcher.fetchReferralLink() { result in
            switch result {
            case .success(let referralLink):                
                UserDefaults.standard.set(referralLink, forKey: UserDefaultsReferralURLForUserKey)
            case .failure(let error):
                NSLog("failed to fetch referral link, err = \(error)")
                UserDefaults.standard.set("(NO LINK FOUND)", forKey: UserDefaultsReferralURLForUserKey)
            }
        }
    }
}

extension AppDelegate: AuthServiceDelegate {
    func signInCompletion(authService: AuthServiceProtocol, user: AuthUser?, error: Error?) {
        debugPrint("####Current UID \(user?.uid ?? "(userID is nil)")")

        DynamicLinks.performDiagnostics(completion: nil)
        
        if error == nil {
            fetchReferralLink()
        }
    }
    
//    private func updateAuthUserId(user: AuthUser){
//        let uid = user.uid
//        debugPrint("####Current UID \(uid)")
//        let defaults = UserDefaults.standard
//        print("app userId: \(defaults.string(forKey: Constants.CURRENT_USER_ID))")
//        if (defaults.string(forKey: Constants.CURRENT_USER_ID) == nil)
//        {
//            self.ref = Database.database().reference()
//            let userRef = self.ref.child("users").childByAutoId()
//            print("userRef: \(userRef)")
//
//            if let _  = userRef.key {
//                print("userid: \(uid)")
//                defaults.setValue(uid, forKey: Constants.CURRENT_USER_ID)
//
//                self.ref.child("users").child(uid).observeSingleEvent(of: .value, with: { snapshot in
//
//                    // check if the current user has a score set
//                    if snapshot.hasChild("score") {
//                        let value = snapshot.value as? NSDictionary
//                        let score = value?["score"] as? Int // ?? 0
//                        print("user already has a score of \(String(describing: score)))")
//
//                        // if current user does not have a score set
//                    } else {
//                        // set score to 0
//                        print("user does not have points yet")
//                        self.ref.child("users/\(uid)/score").setValue(0)
//                        print("user score is set to 0")
//                    }
//                })
//
//                if let fcmToken = UserDefaults.standard.object(forKey: "fcmToken") {
//                    self.ref.child("users/\(uid)/deviceToken").setValue(fcmToken)
//                }
//            }
//        }
//    }
}
