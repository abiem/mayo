//
//  File.swift
//  Mayo-ios-client
//
//  Created by Lakshmi Kodali on 12/12/17.
//  Copyright © 2017 Weijie. All rights reserved.
//

import Cluster
import Foundation
import MapKit

class BorderedClusterAnnotationView: StyledClusterAnnotationView {
    let mborderColor: UIColor

    init(annotation: MKPointAnnotation?, reuseIdentifier: String?, style: ClusterAnnotationStyle, borderColor: UIColor) {
        mborderColor = borderColor
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier, style: style)
    }

    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func configure() {
        super.configure()
        switch style {
        case .image:
            layer.borderWidth = 0
        case .color:
            backgroundColor = .clear
            image = #imageLiteral(resourceName: "cluster")
            countLabel.font = UIFont.systemFont(ofSize: 18)
            countLabel.adjustsFontSizeToFitWidth = true
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if case .color = style {
            var frameLabel = self.countLabel.frame
            frameLabel.size.height = frameLabel.height - 8
            self.countLabel.frame = frameLabel
        }
    }
}
