//
//  ViewController.swift
//  Mayo-ios-client
//
//  Created by abiem  on 4/8/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import Alamofire
import AVFoundation
import AVKit
import APNGKit
import Cluster
import Firebase
import GeoFire
import iCarousel
import MapKit
import SafariServices
import UIKit
import UserNotifications

class MainViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet var mCarouselHeight: NSLayoutConstraint!
    @IBOutlet var carouselView: iCarousel!
    @IBOutlet var mapView: MKMapView!
	@IBOutlet var mBackgroundAnimation: UIImageView!
	@IBOutlet var mImageView: APNGImageView!
	
    //MARK: - Properties
    var lastUpdatedTime: Date?
    var pointsLabel: UILabel!
//    var thanksAnimImageView: APNGImageView!
//    var flareAnimImageView: UIImageView!
    var safariVC: SFSafariViewController?
    var playingThanksAnim = false
    var completedTask: Task?
    var heightShadow = 0
    // user Task ID
    var userTaskId: String?
    
    // MARK: Subviews tags
    let COMPLETION_VIEW_TAG = 98
    let USERS_HELPED_BUTTON_TAG = 99
    let NO_USERS_HELPED_BUTTON_TAG = 100
    let CURRENT_USER_TEXTVIEW_TAG = 101
    let POINTS_GRADIENT_VIEW_TAG = 102
    let POINTS_GRADIENT_VIEW_LABEL_TAG = 103
    let POST_NEW_TASK_BUTTON_TAG = 104
    let POINTS_PROFILE_VIEW_TAG = 105
    let CURRENT_USER_CANCEL_BUTTON = 106
    let LOADER_VIEW = 107
    let VIEW_COUNT_TAG = 108
    let POST_TYPE_TAG = 109

    // MARK: z index for map annotations
    let ANNOTATION_TOP_INDEX = 7.0
    let CLUSTER_TASK_ANNOTATION_Z_INDEX = 6.0
    let FOCUS_MAP_TASK_ANNOTATION_Z_INDEX = 4.0
    let STANDARD_MAP_TASK_ANNOTATION_Z_INDEX = 3.0
    let STANDARD_MAP_EXPIRE_TASK_ANNOTATION_Z_INDEX = 5.0

    // constants for onboarding tasks
    let ONBOARDING_TASK_1_DESCRIPTION = "Mayo posts from others look like this. Swipe left to see more, swipe right to create your own. 🤜🏻✨🤛🏻"
    let ONBOARDING_TASK_2_DESCRIPTION = "If you can lend a hand, tap to send a message. Give it a try now for a free Mayo point!"
    let ONBOARDING_TASK_3_DESCRIPTION = "Tap on the top right of the screen to see Mayo points & rewards."
    
    

    // rotation key animation
    let kRotationAnimationKey = "com.mayo.rotationanimationkey"

    /// save the last index for the carousel view
    var lastCardIndex: Int?

    // constants for time
    let SECONDS_IN_HOUR = 3600

    // constants for time
    let SECONDS_IN_FIFTEEN = 900

    // constants for time
    let SECONDS_IN_DAY = 86400

    // Contant Time for location update
    let LOCATION_UPDATE_IN_SECOND = 600

    // Constant minimum Chat history
    let CHAT_HISTORY = 5

    // flag to check if swiped left to add new item
//    var newItemSwiped = true

    /// chat channels
    var chatChannels: [String] = []

    /// nearby users.
    var nearbyUsers: [String] = []

    /// boolean check for if keyboard is on screen
    var keyboardOnScreen = false

    /// Check Push Notification
    var mShowNotification = true

    /// query distance for getting nearby tasks and users in meters
    let queryDistance = exitAreaRadius

    /// current user uid
    var currentUserId: String?

    /// checks if the current user saved current task
    var currentUserTaskSaved = false

    // user location coordinates
//    var userLatitude: CLLocationDegrees?
//    var userLongitude: CLLocationDegrees?

    // users to thank array
    var usersToThank: [String: Bool] = [:]
	var thanksImage: APNGImage?
	
    // firebase ref
    var ref: DatabaseReference?

    var usersRef: DatabaseReference?
    var currentUserHandle: DatabaseHandle?
    var mCurrentUserTaskActivity: DatabaseHandle?

    var taskViewRef: DatabaseReference?

    var tasksRef: DatabaseReference?
    var channelsRef: DatabaseReference?

    var tasksExpireObserver: DatabaseHandle?
    var tasksRefHandle: DatabaseHandle?
    var tasksCircleQuery: GFCircleQuery?
    var usersCircleQuery: GFCircleQuery?
    var tasksCircleQueryHandle: FirebaseHandle?

    var tasksDeletedCircleQueryHandle: FirebaseHandle?
    var usersCircleQueryHandle: FirebaseHandle?

    var usersDeletedCircleQueryHandle: FirebaseHandle?
    var usersMovedCircleQueryHandle: FirebaseHandle?
    var usersExitCircleQueryHandle: FirebaseHandle?
    var usersEnterCircleQueryHandle: FirebaseHandle?

    // create location manager variable
//    var locationManager: CLLocationManager!

    // Notification center
    private var notification: NSObjectProtocol?

    // tasks array for nearby tasks
    var tasks = [Task?]() {
        willSet {
            print("AAA")

            let ddd = newValue.compactMap { $0 }
            let cas = ddd.contains { task -> Bool in
                task.taskDescription == "G"
            }
            if cas {
                print("bbbb")
            }
        }
    }

    // geofire
    var tasksLocationsRef: DatabaseReference?
    var usersLocationsRef: DatabaseReference?
    var tasksGeoFire: GeoFire?
    var usersGeoFire: GeoFire?

    // task self destruct timer
    var expirationTimer: Timer?
    var locationUpdateTimer: Timer?
    var fakeUsersTimer: Timer?
    var fakeUsersCreated = false
    
    // Checks
    var isLocationNotAuthorised = false
    var isLoadingFirebase = false
    
    /// Cluster
    let clusterManager = ClusterManager()
    
    ///
    var mTaskDescription: String?
    
    /// expired task point
    var expiredAnnotation = CustomExpireTask()
    
    /// menuItems
    let menuItems: [String] = [
        "Spread Mayo",
        "Send feedback",
        "Support and FAQ",
        "Visit our website",
        "Terms of Use"
    ]

    // indicator
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    var mTaskScore = 0

    deinit {
        // get rid of observers when denit
    }

    // MARK: - ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {

		UserLocation.shared.delegate = self
		// Check for location when user come foreground
        observeUserLocationAuth()

		// Check internet Connection
		DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
			let networkStatus = Reachbility.sharedInstance
			networkStatus.startNetworkReachabilityObserver()
		}
		
        if isLoadingFirebase {
            startLoaderAnimation()
        }
        // center map to user's location when map appears
        if let location = UserLocation.shared.getLocation() {
            mapView.setCenter(location.coordinate, animated: true)
        }
        // start updationg users icons
        startUpdationForUserLocation()

        // Check Fake tasks are available
        //  if checkFakeTakViewed() != true {
        let defaults = UserDefaults.standard
        
        // TODO: idk if this is necessary 
        let boolForTask2 = defaults.bool(forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed)
        
        // Remove ExampleTask if completed
        // when coming back from chatVC
		if let userId = defaults.string(forKey: Constants.CURRENT_USER_ID) {
			if boolForTask2 == true {
				for (index, element) in tasks.enumerated() {
                    if element?.taskDescription == Constants.SAMPLE_POST.EXAMPLE_TASK.desc {
						UpdatePointsServer(1, userId)
						mTaskScore = mTaskScore + 1
						usersRef?.child(userId).child("score").setValue(mTaskScore)
						removeOnboardingFakeTask(carousel: carouselView, cardIndex: index, userId: (element?.taskID)!)
						DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
							self?.showUserThankedAnimation()
						}
						pointsLabel.text = String(2)
                        
                        // Add swipe left sample post
                        // add at index 1 because there might be history posts
                        addSamplePost(Constants.SAMPLE_POST.SWIPE_LEFT.shown, at: 1)
                        carouselView.scrollToItem(at: 1, animated: true)
                        
                        
					}
				}
			}
		}

        if UIApplication.shared.applicationIconBadgeNumber == 1 {
            showNotificationAlert(prompt: "Hello again! Want to be notified of new posts around you in the future?")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserLocation.shared.requestAlwaysAuthorization(callback: { (status: CLAuthorizationStatus) -> Void in
            print("Asking permission in mainVC")
            return
        })
        
        let viewHeight = UIScreen.main.bounds.size.height <= 568 ? 232 : 264
        mCarouselHeight.constant = CGFloat(viewHeight)

//        clusterManager.cellSize = nil
        clusterManager.maxZoomLevel = 12
        clusterManager.minCountForClustering = 4
        clusterManager.clusterPosition = .average
        clusterManager.shouldRemoveInvisibleAnnotations = false

        // set current user id
        currentUserId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)

        // reset users to thank dictionary
        usersToThank = [:]

        // show user's position
        mapView.showsUserLocation = true
        // turn off compass on mapview
        mapView.showsCompass = false

        // setup mapview delegate
        mapView.delegate = self

        // setup firebase/geofire
        ref = Database.database().reference()
        tasksRef = ref?.child("tasks")
        channelsRef = ref?.child("channels")
        usersRef = ref?.child("users")
        tasksLocationsRef = ref?.child("tasks_locations")
        usersLocationsRef = ref?.child("users_locations")
        taskViewRef = ref?.child("task_views")
        tasksGeoFire = GeoFire(firebaseRef: tasksLocationsRef!)
        usersGeoFire = GeoFire(firebaseRef: usersLocationsRef!)

		loadGifImages()

        // get user Location
//        locationManager = CLLocationManager()
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.distanceFilter = 20
//        locationManager.stopMonitoringSignificantLocationChanges()
//		if #available(iOS 11.0, *) {
//			locationManager.showsBackgroundLocationIndicator = false
//		} else {
//			// Fallback on earlier versions
//		}
//        locationManager.startUpdatingLocation()

        // set region that is shown on the map
//        setupLocationRegion()
		let span = MKCoordinateSpan(latitudeDelta: 0.0015, longitudeDelta: 0.0015)
		let center = mapView.centerCoordinate
		mapView.region = MKCoordinateRegion(center: center, span: span)
		mapView.camera = MKMapCamera(lookingAtCenter: center, fromDistance: CLLocationDistance(800), pitch: 45, heading: 0)
		mapView.userTrackingMode = .follow


        // get updated user location coordinates
        getCurrentUserLocation()

        // setup carousel view
        carouselView.type = iCarouselType.linear
        carouselView.isPagingEnabled = true
        carouselView.bounces = true
        carouselView.bounceDistance = 0.2
        carouselView.scrollSpeed = 1.5
		carouselView.isUserInteractionEnabled = true
		// disable accessibility so tests can click on stuff inside it
        carouselView.isAccessibilityElement = false


        // MARK: create Points view on top right
        let pointsShadowGradientView = createPointsView()
        view.addSubview(pointsShadowGradientView)
        
        if onboarding2Done() {
            print("viewDidLoad onboarding2Done ??")
            getPreviousTask()
            initUserAuth()
        } else {
            let defaults = UserDefaults.standard
            
            isLoadingFirebase = false
            
            
            
            // add Example Task
            if !defaults.bool(forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed) {
                addSamplePost(Constants.SAMPLE_POST.EXAMPLE_TASK.shown, at: tasks.count - 1)
                carouselView.scrollToItem(at: tasks.count - 1, animated: true)
                
            }
            
            
            // if swipe left has been shown before but not done yet
            if defaults.bool(forKey: Constants.SAMPLE_POST.SWIPE_LEFT.shown) && !defaults.bool(forKey: Constants.SAMPLE_POST.SWIPE_LEFT.viewed) {
                addSamplePost(Constants.SAMPLE_POST.SWIPE_LEFT.shown, at: 1)
                carouselView.scrollToItem(at: 1, animated: true)
                
            }
            
            // if first post task live has been shown
            if defaults.bool(forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.shown) && !defaults.bool(forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.viewed) {
                addSamplePost(Constants.SAMPLE_POST.FIRST_POST_LIVE.shown, at: 1, shift: !defaults.bool(forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed))
            }
                
            // if history title card now shown yet and not viewed (dismissed) then show
            if !defaults.bool(forKey: Constants.SAMPLE_POST.HISTORY_TITLE.viewed) {
                addSamplePost(Constants.SAMPLE_POST.HISTORY_TITLE.shown, at: self.tasks.count - 1)
            }
                
                
            initUserAuth()
        }
    }
	
    override func viewWillAppear(_: Bool) {
        // hides navigation bar for home viewcontroller
        navigationController?.isNavigationBarHidden = true
        
//        LocationMonitor.instance.addDelegate(delegate: self)
        checkNotificationPermission()
    }

    override func viewWillDisappear(_: Bool) {
        // show navigation bar on chat view controller
        navigationController?.isNavigationBarHidden = false
        locationUpdateTimer?.invalidate()
        locationUpdateTimer = nil

        if let notification = notification {
            NotificationCenter.default.removeObserver(notification)
        }

        UserLocation.shared.delegate = nil
//        LocationMonitor.instance.removeDelegate(delegate: self)
    }

// MARK: - Other
	func loadGifImages() {
		let path = Bundle.main.path(forResource: "bump", ofType: "png")
		do {
			let data = try Data(contentsOf: URL(fileURLWithPath: path!))
			thanksImage = APNGImage(data: data, progressive: false)
			
		} catch {
			print("Something went Wrong")
		}
	}

    func initUserAuth() {
        if currentUserId == nil { return }

        // create task for current user
        // and also set channel for chat for current user's chat
        if tasks.count == 0 || !onboarding2Done() {
            print("current user task created")

			if let loc = UserLocation.shared.getLocation() {
                // TODO: fix
                let timeStamp = Date().toMillis()

                let task = Task(userId: currentUserId!,
                                taskDescription: "loading",
//                                latitude: userLatitude!,
//                                longitude: userLongitude!,
								latitude: loc.coordinate.latitude,
								longitude: loc.coordinate.longitude,
                                completed: false,
                                timeCreated: Date(),
                                timeUpdated: Date(),
                                taskID: "\(timeStamp!)",
                                recentActivity: false,
                                userMovedOutside: false,
                                postType: "Asking"
                            )

                tasks.insert(task, at: 0)
                carouselView.reloadData()
            }
            currentUserTaskSaved = false
        }
        if !onboarding2Done(), tasks.count > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
				self?.carouselView.scrollToItem(at: 1, animated: true)
			}
        }
        // if no chat channels
        // append current user's channel
        if tasks.count > 0 {
            print("current user chat channel appended")
            // TODO: fix
            let task = tasks.last as? Task
            chatChannels.append((task?.taskID!)!)
        }

        // query for tasks nearby
//        if let userLatitude = self.userLatitude, let userLongitude = self.userLongitude {
//            queryTasksAroundCurrentLocation(latitude: userLatitude, longitude: userLongitude)
//
//            // query for users nearby
//            queryUsersAroundCurrentLocation(latitude: userLatitude, longitude: userLongitude)
//        }
		queryTasksAroundCurrentLocation()
		queryUsersAroundCurrentLocation()
        

        // observe for points
        observeForCurrentUserPoints()

        // add current user's location to geofire
        addCurrentUserLocationToFirebase()
    }

    func observeForCurrentUserPoints() {
        // create observer for current user's points
		if let currentUserId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
			currentUserHandle = usersRef?.child(currentUserId).observe(.value, with: { snapshot in

				let value = snapshot.value as? NSDictionary
				let userPoints = value?["score"] as? Int ?? 0

				print("current user just got points \(userPoints)")

				// update the points label if they get a point
				self.pointsLabel.text = String(userPoints)
			})
		}
    }

    func checkIfNoPoints() {
        // TODO: if the current user has no points
        // make the points gradient view invisible

        // if the user has points make the points gradient view visible
    }
    
    //MARK: - Points/Menu View - refer to MainViewController+PointsProfile.swift
    
    
    

    // generate random weight interval
//    func generateWeightedTimeInterval() -> Int {
//        // 20% 1 min
//        // 20% 2-3 min
//        // 60% 4-6 min
//        let weights = [1, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6]
//        let randomIndex = generateRandomNumber(endingNumber: 20)
//        let selectedWeight = weights[randomIndex]
//
//        // generate random number/seconds for time interval from 1 to 60
//        let randomSeconds = 1 + generateRandomNumber(endingNumber: 60)
//
//        // get the random time interval with weight and seconds
//        let randomTime = randomSeconds * selectedWeight
//
//        return randomTime
//    }

    //MARK: - Map Stuff
    // setup pins for nearby tasks
    func addMapPin(task: Task, carouselIndex: Int) {
        // add pin for task
        if carouselIndex == 0 {
            let annotation = CustomFocusTaskMapAnnotation(currentCarouselIndex: carouselIndex, taskUserId: task.taskID!)
            annotation.coordinate = CLLocationCoordinate2D(latitude: task.latitude, longitude: task.longitude)
            annotation.style = .color(#colorLiteral(red: 0, green: 0.5901804566, blue: 0.758269012, alpha: 1), radius: 30)
            mapView.addAnnotation(annotation)
        } else if task.completed {
            // FIXME: doesn't work
            print("Added history title pin")
            let annotation = CustomExpireTask()
            annotation.coordinate = CLLocationCoordinate2D(latitude: task.latitude, longitude: task.longitude)
            clusterManager.add(annotation)
        } else {
            print("added annot with taskId : \(task.userId)")
            let annotation = CustomTaskMapAnnotation(currentCarouselIndex: carouselIndex, taskUserId: task.taskID!)
            annotation.coordinate = CLLocationCoordinate2D(latitude: task.latitude, longitude: task.longitude)
            annotation.style = .color(#colorLiteral(red: 0, green: 0.5901804566, blue: 0.758269012, alpha: 1), radius: 30) // .image(#imageLiteral(resourceName: "newNotificaitonIcon"))
            clusterManager.add(annotation)
        }

        clusterManager.reload(mapView: mapView)
    }

    // query users nearby
//    func queryUsersAroundCurrentLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
	func queryUsersAroundCurrentLocation() {
   		fakeUsersCreated = false
        // Query locations at latitude, longitutde with a radius of queryDistance
        // 200 meters = .2 for geofire units
//        let center = CLLocation(latitude: latitude, longitude: longitude)
		if let center = UserLocation.shared.getLocation() {
        usersCircleQuery = usersGeoFire?.query(at: center, withRadius: queryDistance / 1000)

        usersEnterCircleQueryHandle = usersCircleQuery?.observe(.keyEntered, with: { (key: String?, location: CLLocation?) in
            guard let userId = key,
                let location = location else { return }
            print("userId '\(userId)' entered the search and is at location '\(location)'")

            self.usersRef?.child(userId).child("updatedAt").observeSingleEvent(of: .value, with: { snapshot in
                if let lastUpdateTime = snapshot.value as? String {
                    let currentDate = Date()
                    let dateformatter = DateStringFormatterHelper()
                    let userLastUpdate = dateformatter.convertStringToDate(datestring: lastUpdateTime)
                    // check user active from 3 days
                    if currentDate.seconds(from: userLastUpdate) < self.SECONDS_IN_DAY * 3 {
                        if !self.nearbyUsers.contains(userId), userId != UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
                            // Create marker for user
                            if self.nearbyUsers.contains(userId) == false {
                                self.nearbyUsers.append(userId)
                                // TODO: will be added in a future build?
//                                self.addUserPin(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, userId: userId, updatedTime: userLastUpdate)
                            }
                        }
                    }
                }

            })

            let when = DispatchTime.now() + 6 // change 6 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                if self.nearbyUsers.count <= 6, self.fakeUsersCreated == false {
                    self.fakeUsersCreated = true
                   // self.createFakeUsers()
                }
            }

        })

        // remove users circle when it leaves
        usersExitCircleQueryHandle = usersCircleQuery?.observe(.keyExited, with: { (key: String!, _: CLLocation!) in
            print("user \(String(describing: key)) left the area")
            // Remove observer
            print("user observer removed")
            let key1 = key?.replacingOccurrences(of: "Optional(\"", with: "")
            let userId = key1?.replacingOccurrences(of: "\")", with: "")

            if userId == self.currentUserId {
                self.updateNearBytask()
            }

            // remove user in the nearby userlist.
            var index = 0
            for userKey in self.nearbyUsers {
                if userKey == userId {
                    self.nearbyUsers.remove(at: index)
                    break
                }

                index = index + 1
            }

            // loop through the user annotations and remove it
            for annotation in self.mapView.annotations {
                if annotation is CustomUserMapAnnotation {
                    let customUserAnnotation = annotation as! CustomUserMapAnnotation
                    if customUserAnnotation.userId == userId {
                        let viewAnnotation = self.mapView.view(for: annotation)
                        UIView.animate(withDuration: 2, animations: {
                            viewAnnotation?.alpha = 0
                        }, completion: { _ in
                            self.mapView.removeAnnotation(customUserAnnotation)
                        })
                    }
                }
            }

        })

        // update user location when it moves
        usersMovedCircleQueryHandle = usersCircleQuery?.observe(.keyMoved, with: { (key: String!, location: CLLocation!) in
            print("user \(String(describing: key)) moved ")
            let key1 = key?.replacingOccurrences(of: "Optional(\"", with: "")
            let userId = key1?.replacingOccurrences(of: "\")", with: "")

            // loop through the user annotations and remove it
            for annotation in self.mapView.annotations {
                if annotation is CustomUserMapAnnotation {
                    let customUserAnnotation = annotation as! CustomUserMapAnnotation
                    // view for annotation
                    let viewAnnotation = self.mapView.view(for: annotation)
                    if customUserAnnotation.userId == userId {
                        viewAnnotation?.image = #imageLiteral(resourceName: "greenDot")

                        customUserAnnotation.lastUpdatedTime = Date()
                        UIView.animate(withDuration: 1, animations: {
                            customUserAnnotation.coordinate = location.coordinate
                            viewAnnotation?.alpha = 1
                        })
                    }
                }
            }
            if !self.nearbyUsers.contains(userId!), userId != UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
                self.nearbyUsers.append(userId!)
                self.addUserPin(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, userId: userId!, updatedTime: Date())
            }

        })
		}
    }

    // after pin at the deleted index is removed, update the
    // pins and their carousel index
    func updatePinsAfterDeletion(deletedIndex: Int) {
        for annotation in mapView.annotations {
            // if annotation is customTaskMapAnnotation
            if annotation is CustomTaskMapAnnotation {
                let customAnnotation = annotation as! CustomTaskMapAnnotation
                if let carouselIndex = customAnnotation.currentCarouselIndex {
                    if carouselIndex > deletedIndex {
                        // if the annotation's carousel is greater than the deleted index decrease by 1
                        customAnnotation.currentCarouselIndex = customAnnotation.currentCarouselIndex! - 1
                    }
                }
            }

            // if annotation is customFocusTaskMapAnnotation
            if annotation is CustomFocusTaskMapAnnotation {
                let customFocusAnnotation = annotation as! CustomFocusTaskMapAnnotation
                if let carouselIndex = customFocusAnnotation.currentCarouselIndex {
                    if carouselIndex > deletedIndex {
                        // if the annotation's carousel is greater than the deleted index decrease by 1
                        customFocusAnnotation.currentCarouselIndex = customFocusAnnotation.currentCarouselIndex! - 1
                    }
                }
            }
        }
    }

    func addUserPin(latitude: CLLocationDegrees, longitude: CLLocationDegrees, userId: String, updatedTime: Date) {
        // check that the pin is not the same as current user
        if userId != UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
            let userAnnotation = CustomUserMapAnnotation(userId: userId, date: updatedTime)
            userAnnotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.addAnnotation(userAnnotation)
        }
    }

    // get current user's location
    func getCurrentUserLocation() {
        print("getcurrentUserLocation hit")
//        // get user location coordinates
//        if locationManager.location?.coordinate.latitude != nil, locationManager.location?.coordinate.longitude != nil {
//            userLatitude = locationManager.location?.coordinate.latitude
//            userLongitude = locationManager.location?.coordinate.longitude
//        } else {
//            let defaults = UserDefaults.standard
//            guard let archived = defaults.object(forKey: Constants.LOCATION) as? Data,
//                let location = NSKeyedUnarchiver.unarchiveObject(with: archived) as? CLLocation else {
//                userLatitude = Constants.DEFAULT_LAT
//                userLongitude = Constants.DEFAULT_LNG
//                showLocationAlert()
//                return
//            }
//            userLatitude = location.coordinate.latitude
//            userLongitude = location.coordinate.longitude
//        }
//        if tasks.count > 0 {
//            if let currentTask = self.tasks[0] {
//                if userLatitude != nil, userLongitude != nil, currentTask.taskDescription == "" {
//                    currentTask.latitude = userLatitude!
//                    currentTask.longitude = userLongitude!
//                    tasks[0] = currentTask
//                }
//            }
//        }
		
		if let loc = UserLocation.shared.getLocation() {
			print("current lat = \(String(describing: loc.coordinate.latitude))")
			print("current lng = \(String(describing: loc.coordinate.longitude))")
			if tasks.count > 0 {
				if let currentTask = self.tasks[0] {
					if currentTask.taskDescription == "" {
						currentTask.latitude = loc.coordinate.latitude
						currentTask.longitude = loc.coordinate.longitude
					}
				}
			}
		}
    }

//    // sets up the desplay region
//    func setupLocationRegion() {
//        print("setupLocationRegionHit")
//        // get current location
//        getCurrentUserLocation()
//
//        // setup zoom level for mapview
//
//        let span = MKCoordinateSpan(latitudeDelta: 0.0015, longitudeDelta: 0.0015)
//
//        if userLatitude != nil, userLongitude != nil {
//            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: userLatitude!, longitude: userLongitude!), span: span)
//            mapView.setRegion(region, animated: true)
//
//            // setup mapview viewing angle
//            let userCoordinate = CLLocationCoordinate2D(latitude: userLatitude!, longitude: userLongitude!)
//            let mapCamera = MKMapCamera(lookingAtCenter: userCoordinate, fromDistance: CLLocationDistance(800), pitch: 45, heading: 0)
//            mapView.setCamera(mapCamera, animated: true)
//        }
//    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if segue.identifier == "mainToChatVC" {
            let chatViewController = segue.destination as! ChatViewController

            // set sendername to empty string for now
            chatViewController.senderDisplayName = ""

            // get the current index of carousel view
            let channelIndex = carouselView.currentItemIndex

            // get colors from gradient view
            let shadowView = carouselView.currentItemView!
            let gradientView = shadowView.subviews[0] as! GradientView
            _ = gradientView.startColor
            _ = gradientView.endColor

            // use the index to fetch the id of the chat channel

            // pass the task description for the current task
            let currentTask = tasks[channelIndex]!

            // setup display name and title of chat view controller
            chatViewController.title = currentTask.taskDescription
            chatViewController.isCompleted = currentTask.completed

            if currentTask.taskDescription == Constants.SAMPLE_POST.EXAMPLE_TASK.desc {
                chatViewController.isFakeTask = true
                chatViewController.isOwner = true
            }

            chatViewController.channelTopic = currentTask.taskDescription
            let chatChannelId = currentTask.taskID
            // pass chat channel id to the chat view controller
            chatViewController.channelId = chatChannelId
            if currentTask.userId == currentUserId {
                chatViewController.isOwner = true
            }
            // set channel and ref for chat view controller
            chatViewController.channelRef = channelsRef?.child(chatChannelId!)
            print("channel list; \(chatChannels)")
        }
    }

    func observeUserLocationAuth() {
        notification = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) {
            [unowned self] _ in
            self.checkNotificationPermission()
            self.addCurrentUserLocationToFirebase()
            if self.carouselView.currentItemIndex == 0 {
                self.mapView.setUserTrackingMode(.follow, animated: true)
            }
            let authorizationStatus = CLLocationManager.authorizationStatus()
            if authorizationStatus == .denied || authorizationStatus == .notDetermined, self.isLocationNotAuthorised {
                // User has not authorized access to location information.
                self.showLocationAlert()
            }
            self.updateNearBytask()
        }
    }

    func sortMessageArray(_ pMessages: NSDictionary) -> [NSDictionary] {
        var messageArray = [NSDictionary]()
        let helper = DateStringFormatterHelper()
        for message in pMessages.allKeys {
            var messageDic = pMessages[message] as! [String: Any]
            messageDic["dateCreated"] = helper.convertStringToDate(datestring: messageDic["dateCreated"] as! String)
            messageArray.append(messageDic as NSDictionary)
        }
        messageArray.sort {
            item1, item2 in
            let date1 = item1["dateCreated"] as! Date
            let date2 = item2["dateCreated"] as! Date
            return date1.compare(date2) == ComparisonResult.orderedDescending
        }
        return messageArray
    }

    func removeCarousel(_ index: Int) {
        UIView.transition(with: carouselView!,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { () -> Void in
                              self.carouselView.itemView(at: index)?.alpha = 0
                          },
                          completion: { _ in
                              if self.carouselView.itemView(at: index) != nil {
                                  self.carouselView.removeItem(at: index, animated: true)
                              }

                              DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                                  self.carouselView.reloadData()
                              }

        })
    }

    // Remove marker for tasks
    func removeAnnotationForTask(_ taskID: String) {
        for annotation in mapView.annotations {
            if annotation is CustomTaskMapAnnotation {
                // loops through the tasks array and find the corresponding task
                let customAnnotation = annotation as! CustomTaskMapAnnotation

                // check if the task has the same id as the annotation
                if taskID == customAnnotation.taskUserId {
                    clusterManager.remove(annotation)
                    mapView.removeAnnotation(annotation)
                    break
                }

            } else if annotation is CustomFocusTaskMapAnnotation {
                // loops through the tasks array and find the corresponding task
                let customAnnotation = annotation as! CustomFocusTaskMapAnnotation

                // check if the task has the same id as the annotation
                if taskID == customAnnotation.taskUserId {
                    clusterManager.remove(annotation)
                    mapView.removeAnnotation(annotation)
                    break
                }
            }
        }
        clusterManager.reload(mapView: mapView)
    }

    // Start timer to change user location icon
    func startUpdationForUserLocation() {
        locationUpdateTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            for annotation in self.mapView.annotations {
                if annotation is CustomUserMapAnnotation {
                    let annotationCustom = annotation as! CustomUserMapAnnotation
                    let viewAnnotation = self.mapView.view(for: annotation)
                    let imageAnnotation = self.getUserLocationImage(Date().seconds(from: annotationCustom.lastUpdatedTime ?? Date()))
                    if imageAnnotation != nil {
                        viewAnnotation?.image = imageAnnotation
                    } else {
                        UIView.animate(withDuration: 0.5, animations: {
                            viewAnnotation?.alpha = 0.0
                        }, completion: { _ in
                            self.mapView.removeAnnotation(annotation)
                        })
                    }
                }
            }
        }
    }

    func needSetExpiredTask(complition: @escaping ((Bool) -> Void)) {
        let currentTask = tasks[0]
        tasksRef?.child((currentTask?.taskID)!).child("timeUpdated").observe(.value, with: { snapshot in
            if let timeUpdated = snapshot.value as? String {
                let currentTime = Date()
                let dateformatter = DateStringFormatterHelper()
                let timeUpdatedDate = dateformatter.convertStringToDate(datestring: timeUpdated)
                let timeDifference = currentTime.seconds(from: timeUpdatedDate)
                if timeDifference < self.SECONDS_IN_HOUR {
                    complition(false)
                } else {
                    complition(true)
                }
            }
        })
    }

//    // Start updating location at Significant Changes
//    func startReceivingSignificantLocationChanges() {
//        let authorizationStatus = CLLocationManager.authorizationStatus()
//        if authorizationStatus != .authorizedAlways {
//            // User has not authorized access to location information.
//            return
//        }
//
//        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
//            // The service is not available.
//            return
//        }
//        locationManager.delegate = self
//        locationManager.stopUpdatingHeading()
//        locationManager.stopUpdatingLocation()
//        locationManager.startUpdatingLocation()
//    }

    //function to check if user is a student, checks firebase
    func checkIfUserIsAStudent(posterID: String, completion: @escaping (_ isStudent: Bool) -> Void) {
        usersRef?.child(posterID).observeSingleEvent(of: .value, with: { snapshot in
            
            
            var isStudent: Bool = false
            
            let isStudentSnapshot = snapshot.childSnapshot(forPath: "userVerified")
            if let isStudentValue = isStudentSnapshot.value as? Bool {
                if(isStudentValue == true){
                    isStudent = true
                }
            }
            
            completion(isStudent)
            
        })
    }

    // get rid of annotation when user deletes annotation
    func removeCurrentUserTaskAnnotation() {
        // loop through annotation in map view
        for annotation in mapView.annotations {
            // if one of them is a customCurrentUserTaskAnnotation
            if annotation is CustomCurrentUserTaskAnnotation {
                // get rid of it
                let annotationView = mapView.view(for: annotation)
                UIView.animate(withDuration: 0.5, animations: {
                    annotationView?.alpha = 0.0
                }, completion: { _ in
                    self.mapView.removeAnnotation(annotation)
                })
            }
        }
    }
    
    // action for chat to go to chat window
    @objc
    func goToChat(sender _: UIButton) {
        //        if locationManager.location?.coordinate.latitude == nil && locationManager.location?.coordinate.longitude == nil {
        //            showLocationAlert()
        //            return
        //        }
        // if the keyboard is out
        // remove it
        if view.frame.origin.y != 0 {
            UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
        }

        performSegue(withIdentifier: "mainToChatVC", sender: nil)
    }

    // action for chat to show referral link generator UI
    @objc
    func goToReferNewUserUI(sender _: UIButton) {
        ReferralUIPresenter.showReferralUI(onViewController: self)
    }
    
    // function called when carousel view scrolls
//    func carouselDidScroll(_ carousel: iCarousel) {
//        if view.frame.origin.y != 0 {
//            UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
//            view.frame.origin.y = 0
//        }

        // populate new task creation card
//        if carousel.scrollOffset < 0.15, newItemSwiped == false {
//            newItemSwiped = true
//            let defaults = UserDefaults.standard
//            let boolForTask3 = defaults.bool(forKey: Constants.ONBOARDING_TASK3_VIEWED_KEY)
//            if boolForTask3 == false {
//                for (index, task) in tasks.enumerated() {
//                    if task?.taskDescription == ONBOARDING_TASK_3_DESCRIPTION {
//                        checkAndRemoveOnboardingTasks(carousel: carousel, cardIndex: index)
//                    }
//                }
//            }
//            UIView.animate(withDuration: 1, animations: {
//                carousel.reloadItem(at: 0, animated: true)
//            })
//        }
//    }

    /// function to test map annotations error
    func testMapAnnotations() {
        // print out all the annotations and their indexes
        print("new line ------")
        for annotation in mapView.annotations {
            if annotation is CustomFocusTaskMapAnnotation {
                let customAnnotation = annotation as! CustomFocusTaskMapAnnotation
                print("customFocusTaskMapAnnotation \(customAnnotation.currentCarouselIndex!) \(customAnnotation.taskUserId!)")
            } else if annotation is CustomTaskMapAnnotation {
                let customAnnotation = annotation as! CustomTaskMapAnnotation
                print("customTaskMapAnnotation \(customAnnotation.currentCarouselIndex!) \(customAnnotation.taskUserId!)")
            } else if annotation is CustomCurrentUserTaskAnnotation {
                let customAnnotation = annotation as! CustomCurrentUserTaskAnnotation
                print("customCurrentUserTaskAnnotation \(customAnnotation.currentCarouselIndex!)")
            }
        }
        // print out the current carousel index
        print(carouselView.currentItemIndex)
        // print out the tasks in tasks array and their associated index and user id
        for (index, task) in tasks.enumerated() {
            print("Item \(index): \(String(describing: task?.userId))")
        }
    }

    // update map annotations after the tasks/indexes are changed
    func updateMapAnnotationCardIndexes() {
        // loop through each annotation and check if they are task or focus task annotations
        for annotation in mapView.annotations {
            if annotation is CustomTaskMapAnnotation {
                // loops through the tasks array and find the corresponding task
                let customAnnotation = annotation as! CustomTaskMapAnnotation

                for (index, task) in tasks.enumerated() {
                    // check if the task has the same id as the annotation
                    if let taskUserId = customAnnotation.taskUserId {
                        if taskUserId == task?.taskID {
                            // if they match update the annotation with the correct index
                            customAnnotation.currentCarouselIndex = index
                            print("index set \(index)")
                            break
                        }
                    }
                }

            } else if annotation is CustomFocusTaskMapAnnotation {
                // loops through the tasks array and find the corresponding task
                let customAnnotation = annotation as! CustomFocusTaskMapAnnotation

                for (index, task) in tasks.enumerated() {
                    // check if the task has the same id as the annotation
                    if let taskUserId = customAnnotation.taskUserId {
                        if taskUserId == task?.taskID {
                            // if they match update the annotation with the correct index
                            customAnnotation.currentCarouselIndex = index
                            break
                        }
                    }
                }
            }
        }
    }
}
