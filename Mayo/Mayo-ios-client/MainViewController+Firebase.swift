//
//  MainViewController+Firebase.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import Firebase

// MARK: - Firebase interfacing
extension MainViewController {
    func removeFirebaseLoader() {
        if isLoadingFirebase != false {
            isLoadingFirebase = false
//            if userLatitude != nil, userLongitude != nil {
            if let loc = UserLocation.shared.getLocation() {
              // TODO: fix
                let timeStamp = Date().toMillis()
                tasks.append(
//                    Task(userId: currentUserId!, taskDescription: "", latitude: userLatitude!, longitude: userLongitude!, completed: false, timeCreated: Date(), timeUpdated: Date(), taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false)
                    Task(userId: currentUserId!, taskDescription: "", latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude, completed: false, timeCreated: Date(), timeUpdated: Date(), taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false, postType: "Asking")
                )
                carouselView.insertItem(at: 1, animated: true)
            }
            tasks.remove(at: 0)
            carouselView.removeItem(at: 0, animated: true)
            carouselView.reloadItem(at: 0, animated: false)
        }
        indicatorView.isHidden = true
    }

    // add current user's location to firebase/geofire
    func addCurrentUserLocationToFirebase() {
        getCurrentUserLocation()
        if let location = UserLocation.shared.getLocation(),
            let uid = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)
        {
            usersGeoFire?.setLocation(location, forKey: uid)

            // Updated Time
            let dateformatter = DateStringFormatterHelper()
            let stringDate = dateformatter.convertDateToString(date: NSDate() as Date)
            usersRef?.child(uid).child("updatedAt").setValue(stringDate)
        }
    }

    func checkTaskParticipation(_ pTask: Task, callBack: @escaping (_ participation: Bool) -> Void) {
        if let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
            usersRef?.child(userId).child("taskParticipated").child("tasks").observeSingleEvent(of: .value, with: { snapshot in
                if let tasksParticipated = snapshot.value as? [String] {
                    if tasksParticipated.contains(pTask.taskID!) {
                        callBack(true)
                    } else {
                        callBack(false)
                    }
                } else {
                    callBack(false)
                }
            })
        }
    }

    func checkTaskRecentActivity(_ pTask: Task, callBack: @escaping (_ completed: Bool) -> Void) {
        tasksRef?.child((pTask.taskID)!).child("recentActivity").observeSingleEvent(of: .value, with: { snapshot in
            if let activity = snapshot.value as? Bool {
                callBack(activity)
            } else {
                callBack(false)
            }
        })
    }

    // update last 5 locations at backend
    func UpdateUserLocationServer() {
        usersRef?.child(currentUserId!).child("location").observeSingleEvent(of: .value, with: { snapshot in

            if let location = UserLocation.shared.getLocation() {
                let dateformatter = DateStringFormatterHelper()
                let stringDate = dateformatter.convertDateToString(date: NSDate() as Date)

                let location = ["lat": location.coordinate.latitude,
                                "long": location.coordinate.longitude,
                                "updatedAt": stringDate] as [String: Any]

                if var lastLocations = snapshot.value as? [Any] {
                    // append location if any
                    if lastLocations.count >= self.CHAT_HISTORY {
                        lastLocations.removeFirst()
                        lastLocations.append(location)
                        self.setUserinfo(lastLocations, "location", self.currentUserId!)
                    } else {
                        lastLocations.append(location)
                        self.setUserinfo(lastLocations, "location", self.currentUserId!)
                    }
                } else {
                    self.setUserinfo([location], "location", self.currentUserId!)
                }
            }
            UserLocation.shared.endBackgroundTask()
        })
    }

    // Update information At Firebase
    func setUserinfo(_ Value: [Any], _ child: String, _ user: String) {
        usersRef?.child(user).child(child).setValue(Value)
    }

    // update Friends at user Nodes
    func addFriend(_ friendID: String) {
        usersRef?.child(currentUserId!).child("friends").observeSingleEvent(of: .value, with: { snapshot in
            if var arrFriends = snapshot.value as? [String] {
                if !arrFriends.contains(friendID) {
                    arrFriends.append(friendID)
                    self.usersRef?.child(self.currentUserId!).child("friends").setValue(arrFriends)
                }
            } else {
                self.usersRef?.child(self.currentUserId!).child("friends").setValue([friendID])
            }

        })
    }

    // Update points at Firebase Server
    func UpdatePointsServer(_ points: Int, _ user: String) {
        let currentUserTask = tasks[0]
        usersRef?.child(user).child("scoreDetail").observeSingleEvent(of: .value, with: { snapshot in

            // Convert date to String
            let dateformatter = DateStringFormatterHelper()
            let stringDate = dateformatter.convertDateToString(date: NSDate() as Date)

            var dicPoints = [String: Any]()
            dicPoints["points"] = points
            dicPoints["taskID"] = currentUserTask?.taskID
            dicPoints["createdDate"] = stringDate
            if var points = snapshot.value as? [Any] {
                points.append(dicPoints)
                self.setUserinfo(points, "scoreDetail", user)
            } else {
                self.setUserinfo([dicPoints], "scoreDetail", user)
            }

        })
    }

    // Remove task and Update completeion details at Firebase
    func removeTaskAfterComplete(_ currentUserTask: Task) {
        // Remove Notification
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["taskExpirationNotification"])
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["taskExpirationNotification"])
        // reset expiration timer
        expirationTimer = nil
        // invalidate the current timer
        expirationTimer?.invalidate()
        UserLocation.shared.removeAllObservingRegions()

        let taskMessage = currentUserTask.taskDescription
        print("taskMessage \(currentUserTask.taskDescription) \(taskMessage)")

        // Send Push notification If task is Completed
        // filter Admin and thank users users
        if currentUserTask.completeType == Constants.STATUS_FOR_THANKED || currentUserTask.completeType == Constants.STATUS_FOR_NOT_HELPED {
            channelsRef?.child(currentUserTask.taskID!).child("users").observeSingleEvent(of: .value, with: { snapshot in
                if let users = snapshot.value as? [String: Any] {
                    for user in Array(users.keys) {
                        if !Array(self.usersToThank.keys).contains(user), self.currentUserId != user {
                            self.usersRef?.child(user).child("deviceToken").observeSingleEvent(of: .value, with: { snapshot in
                                if let token = snapshot.value as? String {
                                    PushNotificationManager.sendNotificationToDevice(deviceToken: token, channelId: currentUserTask.taskID!, taskMessage: taskMessage)
                                }
                            })
                        }
                    }
                    // reset the dictionary
                    self.usersToThank = [:]
                }
            })
        } else {
            // reset the dictionary
            usersToThank = [:]
        }

        // remove observer for time
        if tasksExpireObserver != nil {
            tasksRef?.child(currentUserTask.taskID!).child("timeUpdated").removeObserver(withHandle: tasksExpireObserver!)
        }
        // reload Carousel
        if tasks.count > 0 {
            tasks.remove(at: 0)
        }

        let timeStamp = Date().toMillis()
        let currentUserKey = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)

        currentUserTaskSaved = false
        //      self.carouselView.insertItem(at: 0, animated: true)
        carouselView.reloadData()
        //        self.carouselView.reloadItem(at: 0, animated: true)
//    currentUserTask.updateFirebaseTask()
        // Update task as Complete
        // create new date formatter
        let dateformatter = DateStringFormatterHelper()

        // convert timeCreated and timeUpdated to string
        let updateDate = dateformatter.convertDateToString(date: Date())

        let taskUpdate = ["completed": currentUserTask.completed,
                          "createdby": currentUserTask.userId,
                          "endColor": currentUserTask.endColor ?? "",
                          "startColor": currentUserTask.startColor ?? "",
                          "taskDescription": currentUserTask.taskDescription,
                          "taskID": currentUserTask.taskID ?? "",
                          "timeCreated": currentUserTask.timeCreatedString,
                          "timeUpdated": updateDate,
                          "completeType": currentUserTask.completeType ?? "",
                          "helpedBy": currentUserTask.helpedBy ?? [],
                          "userMovedOutside": currentUserTask.userMovedOutside,
                          "recentActivity": currentUserTask.recentActivity,
                          "postType": currentUserTask.postType ?? "Other"
            ] as [String: Any]

        tasksRef?.child(currentUserTask.taskID!).setValue(taskUpdate)

        let newTask = Task(userId: currentUserKey!, taskDescription: "", latitude: UserLocation.shared.getLocation()!.coordinate.latitude, longitude: UserLocation.shared.getLocation()!.coordinate.longitude, completed: false, taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false, postType: "Asking")

        if tasks.count > 0, tasks[0]?.taskDescription != "" {
//            newItemSwiped = true
            tasks.insert(newTask, at: 0)

//            tasks.insert(Task(userId: currentUserKey!, taskDescription: "", latitude: (locationManager.location?.coordinate.latitude) ?? userLatitude!, longitude: (locationManager.location?.coordinate.longitude) ?? userLongitude!, completed: false, taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false), at: 0)
        } else {
//            newItemSwiped = true
            tasks.append(newTask)

//            tasks.append(Task(userId: currentUserKey!, taskDescription: "", latitude: (locationManager.location?.coordinate.latitude) ?? userLatitude!, longitude: (locationManager.location?.coordinate.longitude) ?? userLongitude!, completed: false, taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false))
        }

        UserDefaults.standard.set(nil, forKey: Constants.PENDING_TASKS)
    }

    // update Task views count at firebase
    func updateViewsCount(_ task: Task) {
        let taskID = task.taskID!
        taskViewRef?.child(taskID).observeSingleEvent(of: .value, with: { snapshot in
            if let arrTasksdetail = snapshot.value as? [String: Any] {
                if var taskViews = arrTasksdetail["users"] as? [String] {
                    //only update if task is not completed
                    if (!taskViews.contains(self.currentUserId!) && (task.completed == false) ) {
                        taskViews.append(self.currentUserId!)
                        let tasksViewUpdate = ["users": taskViews, "count": taskViews.count] as [String: Any]
                        // update at server
                        self.taskViewRef?.child(taskID).setValue(tasksViewUpdate)
                        
                    }
                } else {
                    let tasksViewUpdate = ["users": [self.currentUserId], "count": 1] as [String: Any]
                    // update at server
                    self.taskViewRef?.child(taskID).setValue(tasksViewUpdate)
                }
                
            } else {
                let tasksViewUpdate = ["users": [self.currentUserId], "count": 1] as [String: Any]
                // update at server
                self.taskViewRef?.child(taskID).setValue(tasksViewUpdate)
            }
        })
    }
    
    func getCurrentTaskViewCount(taskID: String, completion: @escaping (_ count: Int) -> Void) {
        taskViewRef?.child(taskID).observeSingleEvent(of: .value, with: { snapshot in
            if let arrTasksdetail = snapshot.value as? [String: Any] {
                if let taskViews = arrTasksdetail["users"] as? [String] {
                    completion(taskViews.count)
                }
                else{
                    completion(1)
                }
            }
        })
    }

    func removeHistoryTasks() {
        // user Moved to another place
        nearbyUsers.removeAll()
        // remove all users
        mapView.annotations.forEach {
            if $0 is CustomUserMapAnnotation {
                self.mapView.removeAnnotation($0)
            }
        }
        tasks = tasks.filter {
            $0?.completed == false
        }
        print(tasks.count)
        carouselView.reloadData()
    }

    func sortTaskAccordingToTime() {
        if tasks.count > 0 {
            let currentTask = tasks[0]
            tasks.remove(at: 0)
            let completedTasks = tasks.filter {
                $0?.completed == true
            }
            let sortedCompletedTasks = completedTasks.sorted(by: { Int(($0?.taskID!)!) ?? 0 > Int(($1?.taskID)!) ?? 0 })

            let activeTasks = tasks.filter {
                $0?.completed == false
            }
            let sortedActiveTasks = activeTasks.sorted(by: { Int(($0?.taskID!)!) ?? 0 > Int(($1?.taskID)!) ?? 0 })

            tasks.removeAll()
            tasks.append(currentTask)
            tasks += sortedActiveTasks
            tasks += sortedCompletedTasks
            carouselView.reloadData()
        }
    }

    func updateNearBytask() {
        removeHistoryTasks()
//        if userLatitude == nil, userLongitude == nil { return }
//        // remove user observers
//        usersCircleQuery?.removeObserver(withFirebaseHandle: usersExitCircleQueryHandle!)
//        usersCircleQuery?.removeObserver(withFirebaseHandle: usersEnterCircleQueryHandle!)
//        usersCircleQuery?.removeObserver(withFirebaseHandle: usersMovedCircleQueryHandle!)
//        queryUsersAroundCurrentLocation(latitude: userLatitude!, longitude: userLongitude!)
//
//        //task Moved to new Location
//        tasksCircleQuery?.removeObserver(withFirebaseHandle: tasksDeletedCircleQueryHandle!)
//        tasksCircleQuery?.removeObserver(withFirebaseHandle: tasksCircleQueryHandle!)
//        queryTasksAroundCurrentLocation(latitude: userLatitude!, longitude: userLongitude!)
        
        if UserLocation.shared.getLocation() != nil {
            // remove user observers
            usersCircleQuery?.removeObserver(withFirebaseHandle: usersExitCircleQueryHandle!)
            usersCircleQuery?.removeObserver(withFirebaseHandle: usersEnterCircleQueryHandle!)
            usersCircleQuery?.removeObserver(withFirebaseHandle: usersMovedCircleQueryHandle!)
            queryUsersAroundCurrentLocation()
            
            //task Moved to new Location
            tasksCircleQuery?.removeObserver(withFirebaseHandle: tasksDeletedCircleQueryHandle!)
            tasksCircleQuery?.removeObserver(withFirebaseHandle: tasksCircleQueryHandle!)
            queryTasksAroundCurrentLocation()
        }
    }
}
