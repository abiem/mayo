//
//  MainViewController+Animation.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UI animation
extension MainViewController {
    
    /// fist bump with radiating backgound ?
    func showUserThankedAnimation() {
        if playingThanksAnim == true { return }
//        let viewFrame = view.frame
//        thanksAnimImageView = APNGImageView(frame: CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.size.width, height: viewFrame.size.height))
//        flareAnimImageView = UIImageView(frame: CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.size.width, height: viewFrame.size.height))
//        flareAnimImageView.image = #imageLiteral(resourceName: "flareImage")
//        flareAnimImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
//
//        thanksAnimImageView.contentMode = .scaleAspectFit
//        flareAnimImageView.contentMode = .scaleAspectFill

//        let imageListArray: NSMutableArray = []
//        for countValue in 1 ... 51 {
//            let imageName: String = "fux00\(countValue).png"
//            let image = UIImage(named: imageName)
//            imageListArray.add(image!)
//        }
        flareAnimation(view: mBackgroundAnimation, duration: Constants.THANKS_ANIMATION_DURATION)

//        thanksAnimImageView.animationImages = imageListArray as? [UIImage]
//        thanksAnimImageView.animationDuration = Constants.THANKS_ANIMATION_DURATION
//        view.addSubview(flareAnimImageView)
//        view.addSubview(thanksAnimImageView)

        if let image = thanksImage {
            playingThanksAnim = true
            mImageView.isHidden = false
            mBackgroundAnimation.isHidden = false
            
            mImageView.image = image
            mImageView.startAnimating()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.THANKS_ANIMATION_DURATION + 1) {
            self.afterThanksAnimation()
        }
    }

    // Start Flare Animation for thanks
    func flareAnimation(view: UIView, duration: Double = 1) {
        mBackgroundAnimation.isHidden = true
        mBackgroundAnimation.image = #imageLiteral(resourceName: "flareImage")
        if view.layer.animation(forKey: kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float(.pi * 2.0)
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity
            view.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
        }
    }

    // Stop flare Animation
    func stopFlareAnimation(view: UIView) {
        if view.layer.animation(forKey: kRotationAnimationKey) != nil {
            view.layer.removeAnimation(forKey: kRotationAnimationKey)
        }
    }

    func afterThanksAnimation() {
//        stopFlareAnimation(view: flareAnimImageView)
        mImageView.image = nil
        mImageView.stopAnimating()
        mImageView.isHidden = false
        mBackgroundAnimation.layer.removeAnimation(forKey: kRotationAnimationKey)
        mBackgroundAnimation.image = nil
        mBackgroundAnimation.isHidden = false
        playingThanksAnim = false
//        thanksAnimImageView.image = nil
//        flareAnimImageView.removeFromSuperview()
//        thanksAnimImageView.stopAnimating()
//        thanksAnimImageView.removeFromSuperview()
//        playingThanksAnim = false
    }
    
    func startLoaderAnimation() {
        if let animationView = self.view.viewWithTag(self.LOADER_VIEW) {
            animationView.alpha = 0.3
            UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
                animationView.alpha = 0.8
            }, completion: nil)
        }
    }
}
