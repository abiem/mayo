//
//  AuthServiceProtocol.swift
//  Mayo-ios-client
//
//  Created by Daniel Lee on 4/17/20.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation

class AuthUser{
    
    private(set) var uid: String
    private(set) var token: String
    private(set) var refreshToken: String
    
    init(uid: String, token: String, refreshToken: String){
        self.uid = uid
        self.token = token
        self.refreshToken = refreshToken
    }
}

protocol AuthServiceDelegate: AnyObject{
    func signInCompletion(authService: AuthServiceProtocol, user: AuthUser?, error: Error?)
}

protocol AuthServiceProtocol {
    /**
     Current user to THIS instance of the AuthService
     */
    var currentUser: AuthUser? {get}
    var delegate: AuthServiceDelegate? {get set}
    
    /**
     Allow user to sign in without providing any credentials
     */
    func signInAnonymously()
    
    /**
     Sign out current user
     */
    func signOut()
}

enum AuthServiceError: Error{
    /**
     Unknown issue has happened. Please provide more detail in String
     */
    case unknown(reason: String)
    /**
     If we are not able to obtain an auth token for the user
     */
    case noAuthTokenObtained
    
    /**
     Unable to obtain the refresh token
     */
    case noRefreshTokenObtained
}
