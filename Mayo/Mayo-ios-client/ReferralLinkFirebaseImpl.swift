//
// Created by Alex Hunsley on 03/05/2020.
// Copyright (c) 2020 abiem design. All rights reserved.
//

import Foundation
import Alamofire

public enum ReferralLinkFirebaseImplError: Error {
    case badJSONReturned
}

class ReferralLinkFirebaseImpl {
    var fetchCalled = false
    var request: Request?

    func fetchReferralLink(completion: @escaping (Result<String>) -> Void) {
        if fetchCalled {
            NSLog("Refusing to run fetchReferralLink after it was already called in this instance")
            completion(Result.failure(ReferralLinkError.fetchCalledMultipleTimes))
            return
        }

        fetchCalled = true

        // Docs at: https://docs.api.heymayo.com/#operation/getReferralLink
        let refURL = "https://api.heymayo.com/user/getReferralLink"

        guard let token = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_TOKEN) else {
            print("ERROR: no token found for Firebase current user")
            completion(Result.failure(ReferralLinkError.noUserTokenFound))
            return
        }

        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(token)",
        ]
        print("Made headers: \(headers)")

        request = Alamofire.request(refURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseJSON { response in

                    if let error = response.result.error as? AFError {
                        print("Request failed with error: \(error)")
                        completion(Result.failure(error))
                        return
                    }

                    if let result = response.result.value {
                        let JSON = result as! NSDictionary
                        print(JSON)

                        guard let jsonData = JSON["data"] as? NSDictionary,
                              let jsonFullLink = jsonData["fullLink"] as? String else {
                            completion(Result.failure(ReferralLinkFirebaseImplError.badJSONReturned))
                            return
                        }

                        completion(Result.success(jsonFullLink))
                    }
                }
    }

    func cancel() {
        request?.cancel()
    }
}
