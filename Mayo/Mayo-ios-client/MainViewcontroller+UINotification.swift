//
//  MainViewcontroller+UINotification.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit

// MARK: - MainViewController (UI Notifications)

extension MainViewController {
    override func touchesBegan(_: Set<UITouch>, with _: UIEvent?) {
        if view.frame.origin.y != 0 {
            UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
        }
    }
    
    @IBAction func actionMapStartFollow(_: UIButton) {
        mapView.setUserTrackingMode(.follow, animated: true)
    }

    @objc
    func onboardingTaskSwiped(_: UIGestureRecognizer) {
        // check if current card is an onboarding card
        let currentCardIndex = carouselView.currentItemIndex
        // if it is delete it
        lastCardIndex = currentCardIndex
        //        self.checkAndRemoveOnboardingTasks(carousel: self.carouselView, cardIndex: currentCardIndex)
    }

    // action for when users complete task
    // and some users helped
    @objc
    func handleUsersHelpedButtonPressed(sender _: UIButton) {
        let completionView = view.viewWithTag(COMPLETION_VIEW_TAG)

        // fade out completion view before removing
        UIView.animate(withDuration: 1, animations: {
            completionView?.alpha = 0
        }) { _ in

            // scroll to first item if there is one
            if self.carouselView.itemView(at: 1) != nil {
                self.carouselView.scrollToItem(at: 1, animated: false)
            }

            let usersToThankCopy = self.usersToThank
            var currentUserTask = self.tasks[0]!
            // update Complete task Status
            if let currentTask = self.getSavedTask() {
                currentTask.recentActivity = currentUserTask.recentActivity
                currentUserTask = currentTask
            }

            currentUserTask.completed = true
            currentUserTask.completeType = Constants.STATUS_FOR_THANKED
            currentUserTask.helpedBy = Array(usersToThankCopy.keys)

            self.carouselView.removeItem(at: 0, animated: true)
            self.removeTaskAfterComplete(currentUserTask)

            // thank the users that are in the thank users dictionary
            for userId in usersToThankCopy.keys {
                self.usersRef?.child(userId).observeSingleEvent(of: .value, with: { snapshot in

                    // get the user dictionary
                    let value = snapshot.value as? NSDictionary

                    // get the user's current score
                    let userScore = value?["score"] as? Int

                    // get the user's current deviceToken
                    let deviceToken = value?["deviceToken"] as? String

                    // increment the score
                    if userScore != nil {
                        let newScore = userScore! + 1
                        // add friend
                        self.addFriend(userId)
                        // update points
                        self.UpdatePointsServer(1, userId)
                        // send update to the user's score
                        self.usersRef?.child(userId).child("score").setValue(newScore)
                        print("new score set")
                    }

                    // send the user a notification that they were thanked
                    if deviceToken != nil, deviceToken != self.currentUserId {
                        PushNotificationManager.sendYouWereThankedNotification(deviceToken: deviceToken!, currentUserTask.taskDescription)
                    }

                }, withCancel: { error in
                    print(error.localizedDescription)
                })
            }

            completionView?.removeFromSuperview()
            // toggle carouselView to visible if hidden
            if self.carouselView.isHidden == true {
                self.carouselView.isHidden = false
            }
            // scroll to first item if there is one
            if self.carouselView.itemView(at: 1) != nil {
                self.carouselView.scrollToItem(at: 1, animated: false)
            }
        }
    }

    // action for when users complete task
    // and no users helped
    @objc
    func handleNoOneHelpedButtonPressed() {
        let completionView = view.viewWithTag(COMPLETION_VIEW_TAG)

        // fade out completion view before removing
        UIView.animate(withDuration: 1, animations: {
            completionView?.alpha = 0
        }) { _ in

            // reset the users to thank dictionary
            self.usersToThank = [:]

            completionView?.removeFromSuperview()
            // toggle carouselView to visible if hidden
            if self.carouselView.isHidden == true {
                self.carouselView.isHidden = false
            }
            // scroll to first item if there is one
            if self.carouselView.itemView(at: 1) != nil {
                self.carouselView.scrollToItem(at: 1, animated: false)
            }
        }
        var currentUserTask = tasks[0]!
        if let currentTask = self.getSavedTask() {
            currentTask.recentActivity = currentUserTask.recentActivity
            currentUserTask = currentTask
        }
        currentUserTask.completed = true
        currentUserTask.completeType = Constants.STATUS_FOR_NOT_HELPED
        carouselView.removeItem(at: 0, animated: true)
        removeTaskAfterComplete(currentUserTask)
    }

    // action handler for chat messages
    @objc
    func handleCompletionViewChatUserMessageButtonPressed(sender: UIButton) {
        // get the userId from the button
        let chatUserId = sender.layer.value(forKey: "userId") as? String

        // toggle the alpha of sender
        if sender.alpha == 0.5 {
            sender.alpha = 1

            // add the chatUserId into the users to thank array
            if chatUserId != nil {
                usersToThank[chatUserId!] = true
            }

            let usersHelpedButton = view.viewWithTag(USERS_HELPED_BUTTON_TAG) as! UIButton

            // if users helped button is faded
            if usersHelpedButton.alpha == 0.5 {
                // set to full opacity
                usersHelpedButton.alpha = 1
            }

            // if users helped button is disabled, enable it
            if usersHelpedButton.isEnabled == false {
                usersHelpedButton.isEnabled = true
            }

        } else {
            sender.alpha = 0.5

            // remove the chatUserId from the users to thank dictionary
            if chatUserId != nil {
                usersToThank.removeValue(forKey: chatUserId!)
            }
        }
    }
}
