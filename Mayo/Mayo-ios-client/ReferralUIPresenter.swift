//
//  ReferralThanksPresenter.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 06/05/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit

class ReferralUIPresenter {
    // show UI for getting a referral link to give to someone
    static func showReferralUI(onViewController viewController: UIViewController) {
        let storyboard = UIStoryboard(name: "Referral", bundle: nil)
        let refVC = storyboard.instantiateViewController(withIdentifier: "ReferralViewController")

        // make iOS not grey out background or make our VC appear smaller
        refVC.modalPresentationStyle = .overCurrentContext

        viewController.present(refVC, animated: true, completion: nil)
    }

    // show thanks UI for new user who was referred to Mayo app
    static func showThanks() {
        CMAlertController.sharedInstance.showAlert(supertitle: "Thanks for hopping on!",
                title: "Looks like someone referred you to Mayo. You'll both receive 3 Mayo Points after you've created your first post.",
                image: #imageLiteral(resourceName: "spread-mayo-hero.png"),
                subtitle: "Keep spreading Mayo to make it even better.",
                textColour: UIColor.black,
                backgroundColour: UIColor.white,
                buttonForegroundColour: UIColor.white,
                buttonBackgroundColour: UIColor.hexStringToUIColor(hex: "#46bf65"),
                buttons: ["Awesome"],
                style: .presentWithZoom)
    }
}
