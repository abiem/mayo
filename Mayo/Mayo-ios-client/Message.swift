//
import Foundation
//  Message.swift
//  Mayo-ios-client
//
//  Created by abiem  on 5/29/17.
//  Copyright © 2017 abiem. All rights reserved.
//
import JSQMessagesViewController

class Message: NSObject, JSQMessageData {
    var senderId_: String!
    var senderDisplayName_: String!
    var date_: NSDate
    var isMediaMessage_: Bool
    var hash_: Int = 0
    var text_: String
    var colorIndex: Int?
    var isStudent: Bool?

    init(senderId: String, senderDisplayName: String?, text: String, colorIndex: Int) {
        senderId_ = senderId
        senderDisplayName_ = senderDisplayName
        date_ = NSDate()
        isMediaMessage_ = false

        let randomNum: UInt32 = arc4random_uniform(100_000)
        let randomInt: Int = Int(randomNum)
        hash_ = randomInt

        text_ = text
        self.colorIndex = colorIndex
    }

    func senderId() -> String! {
        return senderId_
    }

    func senderDisplayName() -> String! {
        return senderDisplayName_
    }

    func date() -> Date {
        return date_ as Date
    }

    func isMediaMessage() -> Bool {
        return isMediaMessage_
    }

    func messageHash() -> UInt {
        return UInt(hash_)
    }

    func text() -> String! {
        return text_
    }
}
