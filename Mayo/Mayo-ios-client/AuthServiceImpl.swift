//
//  AuthServiceImpl.swift
//  Mayo-ios-client
//
//  Created by Daniel Lee on 4/18/20.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import FirebaseAuth

class AuthServiceFirebaseImpl: AuthServiceProtocol{
    var currentUser: AuthUser? {
        return Auth.auth().currentUser.map{ u  in
            return AuthUser(uid: u.uid, token: "")
        }
    }
    
    var delegate: AuthServiceDelegate?
    
    func loginAnonymously() {
        
    }
}

