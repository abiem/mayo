//
//  AuthServiceImpl.swift
//  Mayo-ios-client
//
//  Created by Daniel Lee on 4/18/20.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import Firebase
import UIKit

/**
 This is the Firebase implementation of the AuthService.
 */
class AuthServiceFirebaseImpl: AuthServiceProtocol{

    func signOut() {
        try! Auth.auth().signOut()
    }
    
    
    static let shared = AuthServiceFirebaseImpl()
    
    var currentUser: AuthUser? {
        
        guard let token = authIdToken else {return nil} // if there's no id. The user is invalid
        guard let refreshToken = refreshToken else {return nil} // if there's no id. The user is invalid
        
        return Auth.auth().currentUser.map{ u  in
            return AuthUser(uid: u.uid, token: token, refreshToken: refreshToken)
        }
    }
    
    private var authIdToken: String?
    private var refreshToken: String? 
    
    weak var delegate: AuthServiceDelegate?
    
    func signInAnonymously() {
        // Sign in anonymously with Firebase Auth
        Auth.auth().signInAnonymously { [weak self] (auth, error)  in
            self?.signInHelper(auth: auth, error: error)
        }
    }
    
    private func signInHelper(auth: AuthDataResult?, error: Error?){
        if let error = error {
            self.delegate?.signInCompletion(authService: self, user: nil, error: error)
            return
        }
        
        // Verify
        guard let auth = auth else {
            self.delegate?.signInCompletion(authService: self, user: nil, error: AuthServiceError.unknown(reason: "Unable to login user with Firebase Auth"))
            return
        }
        
        self.signInTokenHelper(with: auth)
    }
    
    private func signInTokenHelper(with auth: AuthDataResult) {
        auth.user.getIDTokenForcingRefresh(false) { [weak self] (token, idTokenError) in
            guard let service = self else {return}
            // Check for auth token validity
            if let idTokenError = idTokenError {
                service.delegate?.signInCompletion(authService: service, user: nil, error: idTokenError)
                return
            }
            service.authIdToken = token
            
            // Check for refresh token validity
            guard let refreshToken = Auth.auth().currentUser?.refreshToken else {
                service.delegate?.signInCompletion(authService: service, user: nil, error: AuthServiceError.noRefreshTokenObtained)
                return
            }
            service.refreshToken = refreshToken
            
            // final success callback to the delegate
            guard let user = service.currentUser else {return}
            
            // the completion might want to use the user ID or token, so we store
            // those before calling completion
            service.storeUserIDAndToken(authUser: user)
            
            service.delegate?.signInCompletion(authService: service, user: user, error: nil)

            // not sure if this *must* be called after the completion, but keeping it that way for now
            service.tryInitUserDocument(authUser: user)
        }
    }
}

extension AuthServiceFirebaseImpl {
    /**
     Stores the user ID and token if the user doesn't already exist
     */
    fileprivate func storeUserIDAndToken(authUser: AuthUser) {

        // We always update the uid and token, even if we already have a uid stored.
        // This is experimental change because if we only stored uid and token if
        // we didn't already have a uid, we were using an expired token sometimes.
        let ref = Database.database().reference()
        let userRef = ref.child("users").child(authUser.uid) // use user id as the document id
        print("userRef: \(userRef)")

        if let refId = userRef.key {
            print("####Current UID: \(authUser.uid) with created new document ID: \(refId)")

            let defaults = UserDefaults.standard

            defaults.setValue(authUser.uid, forKey: Constants.CURRENT_USER_ID)
            defaults.setValue(authUser.token, forKey: Constants.CURRENT_USER_TOKEN)
        }
    }
    
    /**
     Inits a new User Document on the Firebase if the user doesn't already exist
     */
    fileprivate func tryInitUserDocument(authUser: AuthUser) {
        
        let defaults = UserDefaults.standard
        let uid = defaults.string(forKey: Constants.CURRENT_USER_ID)

        if (uid == nil) // only do that if the user does not have existing record
        {
            let ref = Database.database().reference()
            let userRef = ref.child("users").child(authUser.uid) // use user id as the document id
            print("userRef: \(userRef)")
            
            if let refId = userRef.key {
                print("####Current UID: \(authUser.uid) with created new document ID: \(refId)")

                ref.child("users").child(refId).observeSingleEvent(of: .value, with: { snapshot in
                    
                    // check if the current user has a score set
                    if snapshot.hasChild("score") {
                        let value = snapshot.value as? NSDictionary
                        let score = value?["score"] as? Int // ?? 0
                        print("user already has a score of \(String(describing: score)))")

                        // if current user does not have a score set
                    } else {
                        // set score to 0
                        print("user does not have points yet")
                        ref.child("users/\(refId)/score").setValue(0)
                        print("user score is set to 0")
                    }
                    
                    //MIGRATION - this is migration work
                    if !snapshot.hasChild("uid"){
                        ref.child("users/\(refId)/uid").setValue(authUser.uid)
                    }
                })
            }
        }
        else {
            debugPrint("####Current uid: \(authUser.uid) with document id of \(String(describing: uid))")
        }
    }
}
