//
//  File.swift
//  Mayo-ios-client
//
//  Created by Lakshmi Kodali on 20/12/17.
//  Copyright © 2017 Weijie. All rights reserved.
//

import Foundation
import iCarousel
import UIKit
import Firebase
import SafariServices

extension OnboardingViewController: iCarouselDelegate, iCarouselDataSource, UITextViewDelegate {
    func numberOfItems(in _: iCarousel) -> Int {
        return 4
    }

    func carousel(_: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        // carouselCurrentItemIndexDidChange isn't called for showing the first card (index 0),
        // so we trigger an animation for first card here instead
//        if index == 0 {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                self.showUserThankedAnimation()
//            }
//        }
        let view = UIView()
        let viewHeight = UIScreen.main.bounds.size.height <= 568 ? 180 : 212
        let viewWidth = UIScreen.main.bounds.size.height <= 568 ? 265 : 335
        view.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        let cardView = UIView(frame: CGRect(x: 0, y: 10, width: viewWidth, height: viewHeight))
        
        cardView.roundCorners()
        
        cardView.backgroundColor = Color.shared.cardBackgroundColor

        let label = UILabel()
        label.frame = CGRect(x: 20, y: 10, width: cardView.frame.size.width - 40, height: 50)
        label.text = Constants.INTRO_TITLE_ARRAY[index]
        let fontSize = UIScreen.main.bounds.size.height <= 568 ? 20 : 24
        label.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        label.textColor = Color.shared.cardTextColor
        
       
        
        
        label.textAlignment = .left
        label.numberOfLines = 0
        
        label.sizeToFit()
        
        let eulaTv = createEulaTitleText(cardView)
        if index == 0 {
            cardView.addSubview(eulaTv)
        } else {
            cardView.addSubview(label)
        }
        

        // Create Button
        let button = UIButton()
        button.frame = CGRect(x: 20, y: cardView.frame.size.height - 60, width: cardView.frame.size.width - 40, height: 45)
        button.setTitle(Constants.INTRO_BUTTON_TITLE_ARRAY[index], for: .normal)
        button.tag = index

        button.backgroundColor = Color.shared.buttonBackgroundColor
        button.setTitleColor(Color.shared.buttonTextColor, for: .normal)
        button.setTitleColor(Color.shared.buttonTextColor, for: .selected)

        button.layer.cornerRadius = 4

        cardView.addSubview(button)
        button.addTarget(self, action: #selector(moveToNextIndex), for: .touchUpInside)

        view.addSubview(cardView)

        view.backgroundColor = UIColor.clear
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 10)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 40.0

        return view
    }

    @objc func moveToNextIndex(_: UIButton) {
        // since we want the cards to be swippable after first card
        // we just set it true when button is clicked
        self.mCarousel.isScrollEnabled = true
        if mCarousel.currentItemIndex == Constants.INTRO_TITLE_ARRAY.count - 1 {
            showAllowLocationAlert()
        } else {
            // SCROLL_DURATION is 0.4 in iCarousel.m
            mCarousel.scroll(byNumberOfItems: 1, duration: 0.4)
        }
    }

    func showAllowLocationAlert() {
        CMAlertController.sharedInstance.showAlert(subtitle: Constants.INTRO_LOCATION_ALERT_TITLE, buttons: ["Got It"]) { sender in
            self.askForLocationAuth()
        }
    }

    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        // We don't stop the bump fist animation when going from 0 to 1
        if carousel.currentItemIndex != 1 {
            stopAnimationAnimation()
        }
        
        // show fist bump animation if we go to first card
        if carousel.currentItemIndex == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.showUserThankedAnimation()
                        }
        }
        
        mBackgroundAnimation.isHidden = false
        mBackgroundAnimation.transform = CGAffineTransform.identity
        if carousel.currentItemIndex == 2 {
            showSecondPinAnimation()
        }
        else if carousel.currentItemIndex == 3 {
            mThanksImageListArray.removeAllObjects()
            showThirdPinAnimation()
        }
    }

    func carousel(_: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.spacing {
            return value * 1.03
        }

        return value
    }
    
    /// Create Eula TextView for first onboarding card
    func createEulaTitleText(_ container: UIView) -> UITextView {
        let tv = UITextView()
        tv.frame = CGRect(x: 20, y: 10, width: container.frame.size.width - 40, height: 50)
        let text = Constants.INTRO_TITLE_ARRAY[0]
        let fontSize = CGFloat(20)
        
        let attriString = NSMutableAttributedString(
                string: text,
                attributes: [
                    .font : UIFont.systemFont(ofSize: fontSize)
        ])
        
        let r = (text as NSString).range(of: "Terms of Use")
        
        let attr : [NSAttributedString.Key : Any] = [
            .font : UIFont.boldSystemFont(ofSize: fontSize),
            .link : "https://www.heymayo.com/eula",
        ]
        
        attriString.addAttributes(attr, range: r)
        tv.attributedText = attriString
        
        // make link string green (blue by default)
        tv.linkTextAttributes = [
            .foregroundColor : Color.shared.accentGreen
        ]
        tv.isEditable = false
        tv.sizeToFit()
        tv.isScrollEnabled = false
        tv.delegate = self
        
        return tv
    }
    
    /// function from UITextViewDelegate Protocol
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "https://www.heymayo.com/eula" {
            let safariVC = SFSafariViewController(url: URL)
            present(safariVC, animated: true, completion: nil)
        }
        return false
            
      }
}
