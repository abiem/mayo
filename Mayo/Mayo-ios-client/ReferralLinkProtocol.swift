//
// Created by Alex Hunsley on 03/05/2020.
// Copyright (c) 2020 abiem design. All rights reserved.
//

import Foundation
import Alamofire

enum ReferralLinkError: Error {
    // the service couldn't find the user token with which to make the request
    case noUserTokenFound
    // user of the service called fetchReferralLink more than once on same instance
    case fetchCalledMultipleTimes
}

protocol ReferralLinkProtocol {
    // fetch the referral link for current user
    static func fetchReferralLink(completion: @escaping (Result<String>) -> Void)
    // cancel any current referral link fetch request
    func cancel()
}
