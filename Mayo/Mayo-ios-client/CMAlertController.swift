//
//  CMAlertController.swift
//  Mayo-ios-client
//
//  Created by Lakshmi Kodali on 08/12/17.
//  Copyright © 2017 Weijie. All rights reserved.
//

// Show Custom Alert on Top View Controller

import UIKit
import SwiftyMarkdown

// Constants to customise Alert
let sALERT_WIDTH = UIScreen.main.bounds.size.height <= 568 ? 286 : 335 // Int(UIScreen.main.bounds.size.width * 0.9)
let sALERT_BACKGROUND_COLOR = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
let sALERT_BUTTON_HEIGHT = 40
let sALERT_BUTTON_Y_PADDING = 6
let sALERT_LABEL_HEIGHT = 40
let sALERT_MARGIN_FROM_LEFT = 20
let sALERT_MARGIN_FROM_TOP = 20
let sALERT_SHADOW_WIDTH = 0
let sALERT_SHADOW_HEIGHT = 20
let sALERT_SHADOW_OPACITY = 0.45
let sALERT_SHADOW_RADIUS = 30
let sALERT_DIAGONAL_START = "FFFFFF"
let sALERT_DIAGONAL_END = "EAFCFF"
let sALERT_TITLE_FONT_NAME = "SanFranciscoText-Regular"

// IMPORTANT if you use markdown styled text (strings beginning "markdown:"), the sizes set by these three
// definitions are not used - you must handle the text size yourself by using e.g. '# heading'
let sALERT_SUPERTITLE_FONT_SIZE = UIScreen.main.bounds.size.height <= 568 ? 17 : 25
let sALERT_TITLE_FONT_SIZE = UIScreen.main.bounds.size.height <= 568 ? 13 : 17
let sALERT_LABEL_FONT_SIZE = UIScreen.main.bounds.size.height <= 568 ? 13 : 17

/**
 Callback for button Action
 @response UIButton optional
 */
typealias CompletionHandler = (UIButton?) -> Void

enum CMAlertControllerPresentationStyle {
    case presentInstantly
    case presentWithZoom
}

class CMAlertController: NSObject {
    static let sharedInstance = CMAlertController()

    /**
     Background Blur view
     */
    private var mAlertBackgroundView = UIView()
    private var completion: CompletionHandler?

    private var presentationStyle: CMAlertControllerPresentationStyle?

    private var limitImageHeight: Int?

    // MARK: - Public API

    /**
     Show Alert on Top View Controller
     @param title Title for Custom Alert (optional)
     @param image Image to be shown between title and subtitle (optional)
     @param subtitle Subtitle for Custom Alert (optional)
     @param buttons Array of buttons titles
     @param alertStyle Style of alert presentation (optional, defaults to presentInstantly)
     */
    func showAlert(supertitle: String? = nil,
                   title: String? = nil,
                   titleColour: UIColor = Color.shared.alertTextColor,
                   image: UIImage? = nil,
                   limitImageHeight: Int? = nil,
                   subtitle: String? = nil,
                   textColour: UIColor = Color.shared.alertTextColor,
                   backgroundColour: UIColor = Color.shared.alertBackgroundColor,
                   buttonForegroundColour: UIColor = Color.shared.buttonTextColor,
                   buttonBackgroundColour: UIColor = Color.shared.buttonBackgroundColor,
                   buttons: [String],
                   style: CMAlertControllerPresentationStyle = .presentInstantly,
                   completion: CompletionHandler? = nil) {

        self.completion = completion
        
        guard let parentView = UIApplication.shared.keyWindow, !parentView.subviews.contains(mAlertBackgroundView) else {
            NSLog("CMAlertController.createAlertView - WARNING not showing alert (title: \(String(describing: title))) because either " +
                    "1) it is already showing, or 2) there is no superview")
            return
        }

        let alertView = buildAlertView(parentView: parentView,
                supertitle: supertitle,
                title: title,
                titleColour: titleColour,
                image: image,
                limitImageHeight: limitImageHeight,
                subtitle: subtitle,
                textColour: textColour,
                backgroundColour: backgroundColour,
                buttonForegroundColour: buttonForegroundColour,
                buttonBackgroundColour: buttonBackgroundColour,
                buttons: buttons)

        show(alertView: alertView, style: style)
    }

    private func show(alertView: UIView, style: CMAlertControllerPresentationStyle) {
        mAlertBackgroundView.addSubview(alertView)

        if style == .presentInstantly {
            return
        }

        // .presentWithZoom
        alertView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)

        // zoom out to 1.05x scale
        UIView.animate(withDuration: 0.75,
                        delay: 0.0,
                        options: .curveEaseOut,
                        animations: {
            alertView.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
        }) { completed in
            // bounce back to 1x scale
            UIView.animate(withDuration: 0.2,
                    delay: 0.0,
                    options: .curveEaseIn,
                    animations: {
                        alertView.transform = CGAffineTransform.identity
                    })
        }
    }

    private func buildAlertView(parentView: UIWindow,
                                supertitle: String?,
                                title: String?,
                                titleColour: UIColor,
                                image: UIImage?,
                                limitImageHeight: Int? = nil,
                                subtitle: String?,
                                textColour: UIColor,
                                backgroundColour: UIColor,
                                buttonForegroundColour: UIColor,
                                buttonBackgroundColour: UIColor,
                                buttons: [String]) -> UIView {
        parentView.addSubview(mAlertBackgroundView)
        setToSuperView(mAlertBackgroundView, pParentView: parentView)
        mAlertBackgroundView.backgroundColor = sALERT_BACKGROUND_COLOR

        let alertView = UIView()
        alertView.backgroundColor = backgroundColour

        alertView.roundCorners()

        // Add Shadow
        alertView.layer.shadowColor = UIColor.black.cgColor
        alertView.layer.shadowOffset = CGSize(width: sALERT_SHADOW_WIDTH, height: sALERT_SHADOW_HEIGHT)
        alertView.layer.shadowOpacity = Float(sALERT_SHADOW_OPACITY)
        alertView.layer.shadowRadius = CGFloat(sALERT_SHADOW_RADIUS)

        var bottomValue: CGFloat = CGFloat(sALERT_MARGIN_FROM_TOP)

        if let supertitle = supertitle {
            let label = createLabel(title: supertitle, yAxis: bottomValue, textColour: textColour)

            // IMPORTANT if you use markdown styled text, the size set by sALERT_SUPERTITLE_FONT_SIZE is not used!
            // only set label font if it's not an attributed string. Otherwise we would
            // lose the attributed string.
            if label.attributedText == nil {
                label.font = UIFont(name: sALERT_TITLE_FONT_NAME, size: CGFloat(sALERT_SUPERTITLE_FONT_SIZE))
                label.textAlignment = .center
            }
            alertView.addSubview(label)
            bottomValue = label.frame.size.height + label.frame.origin.y + CGFloat(sALERT_MARGIN_FROM_TOP)
        }

        if let title = title {
            let label = createLabel(title: title, yAxis: bottomValue, textColour: titleColour)

            // IMPORTANT if you use markdown styled text, the size set by sALERT_TITLE_FONT_SIZE is not used!
            // only set label font if it's not an attributed string. Otherwise we would
            // lose the attributed string.
            if label.attributedText == nil {
                label.font = UIFont(name: sALERT_TITLE_FONT_NAME, size: CGFloat(sALERT_TITLE_FONT_SIZE))
            }
            label.sizeToFit()
            alertView.addSubview(label)
            bottomValue = label.frame.size.height + label.frame.origin.y + CGFloat(sALERT_MARGIN_FROM_TOP)
        }

        if let image = image {
            let imageView = createImageView(image: image, yAxis: Int(bottomValue), limitImageHeight: limitImageHeight)
            alertView.addSubview(imageView)
            
            bottomValue = imageView.frame.size.height + imageView.frame.origin.y + CGFloat(sALERT_MARGIN_FROM_TOP)
        }

        if let subtitle = subtitle {
            let label = createLabel(title: subtitle, yAxis: bottomValue, textColour: textColour)

            // IMPORTANT if you use markdown styled text, the size set by sALERT_TITLE_FONT_SIZE is not used!
            // only set label font if it's not an attributed string. Otherwise we would
            // lose the attributed string.
            if label.attributedText == nil {
                label.textAlignment = .left
                label.font = UIFont(name: sALERT_TITLE_FONT_NAME, size: CGFloat(sALERT_LABEL_FONT_SIZE))
            }

            label.sizeToFit()
            alertView.addSubview(label)
            bottomValue = label.frame.size.height + label.frame.origin.y + CGFloat(sALERT_MARGIN_FROM_TOP)
        }

        for (index, buttonTitle) in buttons.enumerated() {
            let button = createButton(title: buttonTitle,
                                      buttonForegroundColour: buttonForegroundColour,
                                      buttonBackgroundColour: buttonBackgroundColour,
                                      yAxis: bottomValue,
                                      buttonCount: buttons.count,
                                      buttonIndex: index)

            button.roundCorners()
            alertView.addSubview(button)
            bottomValue = button.frame.size.height + button.frame.origin.y + CGFloat(sALERT_MARGIN_FROM_TOP)
        }

        alertView.clipsToBounds = true
        alertView.frame = CGRect(x: 0, y: 0, width: Int(sALERT_WIDTH), height: Int(bottomValue))

        alertView.center.x = parentView.center.x
        alertView.center.y = parentView.center.y
        return alertView
    }

    /**
     Remove Alert From Screen
     */
    func dismissAlert() {
        completion = nil
        mAlertBackgroundView.subviews.forEach { $0.removeFromSuperview() }
        mAlertBackgroundView.removeFromSuperview()
    }

    // MARK: - Private helpers

    /**
     Create Label
     @param pTitle Title for Custom Alert
     @param pYAxis Y positions of label
     @return customised label
     */
    private func createLabel(title: String, yAxis: CGFloat, textColour: UIColor, isBold: Bool = false) -> UILabel {
        
        let label = UILabel(frame: CGRect(x: sALERT_MARGIN_FROM_LEFT, y: Int(yAxis), width: sALERT_WIDTH - 2 * sALERT_MARGIN_FROM_LEFT, height: sALERT_LABEL_HEIGHT))

        
        // is it an attributed string? hacky way to denote markdown formatting in our UI strings
        if title.contains("markdown:") {
            let md = SwiftyMarkdown(string: String(title.suffix(title.count - 9)))
            label.attributedText = md.attributedString()
            label.lineBreakMode = .byWordWrapping
            label.numberOfLines = 0
        }
        else {
            if isBold {
                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
            }
            label.textColor = textColour
            label.numberOfLines = 0
            label.text = title
        }
        
        
        return label
    }

    /**
     Create ImageView
     @param pImage Image for Custom Alert
     @param pYAxis Y positions of Image
     @return customised UIImageView
     */
    private func createImageView(image: UIImage,
                                 yAxis: Int,
                                 limitImageHeight: Int? = nil) -> UIImageView {

        let imageView = UIImageView(image: image)
        
        let usableWidth = sALERT_WIDTH - 2 * sALERT_MARGIN_FROM_LEFT

        let imageAspectRatio = Float(image.size.width) / Float(image.size.height)

        var aspectFitWidth: Int
        var aspectFitHeight: Int

        // if we have a desired height for image, use that
        if let limitImageHeight = limitImageHeight {
            aspectFitWidth = Int(imageAspectRatio * Float(image.size.height))
            aspectFitHeight = limitImageHeight
        }
        else {
            // no set height for image given, so fit its width to the view, allowing for margins
            aspectFitWidth = usableWidth
            aspectFitHeight = Int(Float(usableWidth) / imageAspectRatio)
        }
            
        imageView.frame = CGRect(x: sALERT_MARGIN_FROM_LEFT,
                y: yAxis,
                width: aspectFitWidth,
                height: aspectFitHeight)
                
//        NSLog("Image size: \(image.size)")
//        NSLog("Image size as int: \(Int(image.size.height))")
        
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true

        imageView.image = image

        return imageView
    }

    /**
     Create button
     @param pTitle Title for button
     @param pYAxis Y positions of label
     @param pButtonNumber Total number of buttons
     @param pIndex Current button index
     @return customised label
     */
    private func createButton(title: String,
                              buttonForegroundColour: UIColor,
                              buttonBackgroundColour: UIColor,
                              yAxis: CGFloat,
                              buttonCount: Int,
                              buttonIndex: Int) -> UIButton {
        var width = 0
        var xAxis = sALERT_MARGIN_FROM_LEFT
        var yAxis = yAxis + CGFloat(sALERT_BUTTON_Y_PADDING)
        if buttonCount == 2 {
            width = (sALERT_WIDTH - Int(sALERT_MARGIN_FROM_LEFT * 3)) / 2
            xAxis = buttonIndex % 2 != 0 ? width + (sALERT_MARGIN_FROM_LEFT * 2) : sALERT_MARGIN_FROM_LEFT
            yAxis = buttonIndex % 2 != 0 ? yAxis - CGFloat(sALERT_BUTTON_HEIGHT + sALERT_MARGIN_FROM_TOP + sALERT_BUTTON_Y_PADDING) : yAxis + CGFloat(sALERT_BUTTON_Y_PADDING)
        } else {
            width = sALERT_WIDTH - Int(sALERT_MARGIN_FROM_LEFT * 2)
        }

        let button = UIButton(frame: CGRect(x: xAxis, y: Int(yAxis), width: width, height: sALERT_BUTTON_HEIGHT))

        button.setTitle(title, for: .normal)
        button.tag = buttonIndex

        // if it's the final or only button (the 'default' button)
        // use the given bg colour for background and fg colour for text
        if buttonIndex == buttonCount - 1 {
            button.backgroundColor = Color.shared.buttonBackgroundColor

            button.setTitleColor(Color.shared.buttonTextColor, for: .normal)
            button.setTitleColor(Color.shared.buttonTextColor, for: .selected)
        }
        else {
            // if it's non-default button, invert usual bg and text colour
            button.backgroundColor = Color.shared.buttonTextColor

            button.setTitleColor(Color.shared.buttonBackgroundColor, for: .normal)
            button.setTitleColor(Color.shared.buttonBackgroundColor, for: .selected)
        }
        button.roundCorners()
        button.addBorder(color: buttonBackgroundColour)

        button.addTarget(self, action: #selector(CMAlertController.buttonTouched), for: .touchUpInside)

        return button
    }

    /**
     Button Clicked action
     @return Callback to completeHandler
     */
    @objc func buttonTouched(_ sender: UIButton) {
        if let completionCallback = completion {
            completionCallback(sender)
        }
        dismissAlert()
    }
}
