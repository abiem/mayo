//
//  ChatViewController.swift
//  Mayo-ios-client
//
//  Created by abiem  on 4/12/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import Alamofire
import Firebase
import IQKeyboardManagerSwift
import JSQMessagesViewController
import UIKit

class ChatViewController: JSQMessagesViewController {
    /// for sample post chat room
    var isFakeTask = false
    
    /// for user's tasks chat room
    var isOwner = false
    
    /// database reference for channels ?
    var channelRef: DatabaseReference?
    var taskRef: DatabaseReference?
    var usersRef: DatabaseReference?
    var taskHandler: DatabaseHandle?
    var channelId: String?
    var channelTopic: String?
    var messages = [Message]()
    var currentUserColorIndex: Int?
    var isParticipated = false
    var isCompleted = false
    var mDeclinedNotification = false
    
    let fakeMessages = [
        "⭐Messages from a post’s owner always show up with a star, like this.",
        "⭐Anyone nearby on the app can chime in.",
        "⭐If you can help, send a message back. Send me a message to try it out.",
        "⭐Great! Thanks for helping.",
        "⭐When you help others, or they help you, you can reward each other with Mayo points.",
        "⭐Here’s a Mayo point for helping us out!"
    ]
    
    //student verified badge
    let studentBadge = JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "iconBadgeEdu"), diameter: 24)

    @IBOutlet var viewQuestCompleted: UIView!

    private lazy var messageRef: DatabaseReference = self.channelRef!.child("messages")
    private var newMessageRefHandle: DatabaseHandle?

    override func viewDidLoad() {
        super.viewDidLoad()
        taskRef = Database.database().reference().child("tasks").child(channelId!)
        usersRef = Database.database().reference().child("users")
        checkNotificationPermission()
        topContentAdditionalInset = 0.0

        // disable swipe to navigate
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        // check if the current user id and channel id match
        // if they match, set gray color for the current user(index 0)
        checkColorIndexForCurrentUserIfOwner()

        
        
        // No avatars
        //collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        //collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 24, height: 24)
        //collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 24, height: 24)
        
        // turns off attachments button
        inputToolbar.contentView.leftBarButtonItem = nil
        inputToolbar.contentView.textView.autocapitalizationType = .sentences
        inputToolbar.contentView.textView.autocorrectionType = .yes
        inputToolbar.contentView.textView.spellCheckingType = .yes

        if #available(iOS 11.0, *) {
            self.inputToolbar.contentView.textView.smartQuotesType = .yes
            self.inputToolbar.contentView.textView.smartDashesType = .yes
            self.inputToolbar.contentView.textView.smartInsertDeleteType = .yes
        }

        // set senderid as current user id
        senderId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)
        senderDisplayName = ""
    }
    
    override func viewWillAppear(_: Bool) {
        // listen for new messages
        IQKeyboardManager.shared.enable = false
        observeMessages()
        if isCompleted == false {
			if self.isFakeTask {
				self.sendFakeMessages()
			}
            checkTaskCompletion()
        } else {
            let parentSize = view.frame.size
            // 44
            viewQuestCompleted.frame = CGRect(x: 0, y: parentSize.height - (parentSize.height * 0.1022), width: parentSize.width, height: parentSize.height * 0.1022)
            inputToolbar.isHidden = true
            view.addSubview(viewQuestCompleted)
        }
    }

    override func viewDidAppear(_: Bool) {
        super.viewDidAppear(true)
        if #available(iOS 11.0, *) {
            if self.inputToolbar.window?.safeAreaLayoutGuide != nil {
                self.inputToolbar.bottomAnchor.constraint(lessThanOrEqualToSystemSpacingBelow: (self.inputToolbar.window?.safeAreaLayoutGuide.bottomAnchor)!, multiplier: 1.0).isActive = true
            }
        }
    }

    override func viewWillDisappear(_: Bool) {
        // remove observer for messages
        IQKeyboardManager.shared.enable = true
        messageRef.removeObserver(withHandle: newMessageRefHandle!)
        if isCompleted == false {
            taskRef?.child("completed").removeObserver(withHandle: taskHandler!)
        }
    }

    private func checkColorIndexForCurrentUserIfOwner() {
        // set the color index to 0 if the current channel was created
        // by the current user
        if isFakeTask == true {
            currentUserColorIndex = 1
        } else {
            taskRef?.child("createdby").observe(.value, with: { (snapshot) -> Void in
                if let user = snapshot.value as? String {
                    if user == UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
                        // if the user id and the channel id match
                        // set the current color index to 0
                        self.currentUserColorIndex = 0
                        self.isOwner = true
                        print("current user color index was set")
                    }
                }
            })
        }
    }

    // create new message
    private func addMessage(withId id: String, name: String, text: String, colorIndex: Int, isStudent: Bool) {
        let message = Message(senderId: id, senderDisplayName: name, text: text, colorIndex: colorIndex)
        message.isStudent = isStudent
        // JSQMessage(senderId: id, displayName: name, text: text) {
        messages.append(message)
    }

    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        print(textView.text != "")
    }

    override func textViewShouldBeginEditing(_: UITextView) -> Bool {
        if #available(iOS 11.0, *) {
            self.topContentAdditionalInset = -64
        }

//        if CLLocationManager.locationServicesEnabled() {
//            let status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
//            if status == .authorizedAlways || status == .authorizedWhenInUse {
//                return true
//            }
//        }
//        showLocationAlert()
//        return false
		
		return UserLocation.shared.shouldBeAuthorized()
    }

    override func didPressSend(_: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        // check if current user color index is set
        if currentUserColorIndex == nil {
            // check if current user is in the conversation
            channelRef?.child("users").observeSingleEvent(of: .value, with: { snapshot in

                // boolean flag to check if current user is in the conversation already
                var currentUserIsInConversation: Bool = false
                let usersValue = snapshot.value as? NSDictionary ?? [:]
                for (userId, colorIndex) in usersValue {
                    // if a key matches the current user's uid
                    if userId as? String == UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
                        // set the color index to the value
                        self.currentUserColorIndex = colorIndex as? Int
                        // break out of the loop
                        currentUserIsInConversation = true
                        break
                    }
                }
                // if the user is not in the conversation
                if currentUserIsInConversation == false {
                    // add the user to the conversation and increment users counter
                    let usersCountAndNewColorIndex = usersValue.allKeys.count
                    
                    // save the index
                    // TODO: this is causing the chat error message
                    print("UID: \(Auth.auth().currentUser?.uid ?? "(currentUser = nil")")
                    self.channelRef?.child("users").child((Auth.auth().currentUser?.uid)!).setValue(usersCountAndNewColorIndex)
                    self.currentUserColorIndex = usersCountAndNewColorIndex
                    self.saveMessageAndUpdate(text: text, senderId: senderId, senderDisplayName: senderDisplayName, date: date)

                    // update the current task with emoji star U+2B50

                } else {
                    // user is already in the conversation and is found
                    // rest of update
                    self.saveMessageAndUpdate(text: text, senderId: senderId, senderDisplayName: senderDisplayName, date: date)
                }

            }, withCancel: { error in
                print(error)
            })

        } else {
            saveMessageAndUpdate(text: text, senderId: senderId, senderDisplayName: senderDisplayName, date: date)
        }
        if !isParticipated {
            setTaskParticipated()
        }
    }

    // TODO: update the task with star emoji for description
    func updateTaskDescriptionWithStarEmoji() {
        let viewControllersCount = navigationController?.viewControllers.count
        if (navigationController?.viewControllers[viewControllersCount! - 2] as? MainViewController) != nil {}
    }

    func saveMessageAndUpdate(text: String!, senderId: String!, senderDisplayName: String!, date _: Date!) {
        // check if the sender is the same as the channel id
        // which means that the user who is sending is part of their own
        // if the ids match, then add the screaming face emoticon
        var textToSend = text
        let dateFormatter = DateStringFormatterHelper()
        let dateCreated = dateFormatter.convertDateToString(date: Date())

        // Show Notification Alert
        

        if isFakeTask {
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed)
            addMessage(withId: self.senderId, name: "", text: text, colorIndex: currentUserColorIndex!, isStudent: false)
            finishSendingMessage()
            sendFakeMessages(reply: true)
        } else {
            if isOwner {
                let screamingFaceEmoji = "\u{2B50} " // U+1F631
                textToSend = screamingFaceEmoji + text!

            } else {
                Database.database().reference().child("tasks").child(channelId!).child("recentActivity").setValue(true)
            }
            Database.database().reference().child("tasks").child(channelId!).child("timeUpdated").setValue(dateCreated)
            
            
            var isStudent = false
            if(UserDefaults.standard.object(forKey: "isStudent") != nil){
                let isStudentValue = UserDefaults.standard.value(forKey: "isStudent") as! Bool
                if(isStudentValue == true){
                    isStudent = true
                }
            }
            
            let itemRef = messageRef.childByAutoId()
            let messageItem = [
                "senderId": senderId!,
                "senderName": senderDisplayName!,
                "text": textToSend!,
                "colorIndex": "\(self.currentUserColorIndex!)",
                "dateCreated": dateCreated,
                "hasVerified": isStudent,
                ] as [String : Any]

            itemRef.setValue(messageItem)

            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            if let channelId = self.channelId {
                sendNotificationToTopic(channelId: channelId)

                Messaging.messaging().subscribe(toTopic: "/topics/\(channelId)")
            }

            Analytics.logEvent(AnalyticsEventShare, parameters: [
                AnalyticsParameterContentType: "message",
            ])
        }
        // get the current user's color index if the user is already in the conversation

        // if the user is not currently in the conversation
        // add the user to the conversation and create an index for them

        // self.channelRef?.child("updatedAt").setValue(dateCreated)
        
        // send notification permission
        DispatchQueue.main.async {
            self.showNotificationAlert()
        }
        
        finishSendingMessage()
    }

    // TODO: send notification to the topic for specific channel id
    func sendNotificationToTopic(channelId: String) {
        // TODO: add application/json and add authorization key
        var channelTopicMessage = ""
        if let channelTopic = self.channelTopic {
            channelTopicMessage = channelTopic
        }
		if let currentUserId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
			let ref = Database.database().reference()
			ref.child("channels").child(channelId).child("users").observeSingleEvent(of: .value, with: { snapshot in
				let users = snapshot.value as! [String: Any]
				for user in Array(users.keys) {
					if user != currentUserId {
						ref.child("users").child(user).child("deviceToken").observeSingleEvent(of: .value, with: { snapshot in
							if let token = snapshot.value as? String {
								PushNotificationManager.sendNotificationToDeviceForMessage(device: token, channelId: channelId, topic: channelTopicMessage, currentUserId: currentUserId)
							}
						})
					}
				}

			})
		}
    }

    // reload messages.
    public func reloadChat() {
        observeMessages()
    }

    // listen for new messags
    private func observeMessages() {
        messageRef = channelRef!.child("messages")
        let messageQuery = messageRef.queryLimited(toLast: 25)

        //  We can use the observe method to listen for new
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! [AnyHashable: Any]
            if let id = messageData["senderId"] as! String?, let name = messageData["senderName"] as! String?, let text = messageData["text"] as! String?, text.count > 0, let colorIndexAsInt = messageData["colorIndex"] as! String?, let isStudent = messageData["hasVerified"] as! Bool?{
        
                self.addMessage(withId: id, name: name, text: text, colorIndex: Int(colorIndexAsInt) ?? 0, isStudent: isStudent)
                self.finishReceivingMessage()
            } else {
                print("Error! Could not decode message data")
            }
        })
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        cell.messageBubbleTopLabel.textColor = .black
        cell.messageBubbleTopLabel.isHidden = false
        
        cell.textView?.textColor = UIColor.white
        
        return cell
    }

    override func collectionView(_: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]

        if message.senderId() == senderId {
            // TODO: get the correct index for the sender
            // pass in the correct index to get the correct color
            let messageColorIndex = message.colorIndex != nil ? message.colorIndex! : 0
            let outgoingBubbleImage = setupOutgoingBubble(colorIndex: messageColorIndex)
            return outgoingBubbleImage
        } else {
            let messageColorIndex = message.colorIndex != nil ? message.colorIndex! : 0
            let incomingBubbleImage = setupIncomingBubble(colorIndex: messageColorIndex)
            return incomingBubbleImage
        }
    }

    private func setupOutgoingBubble(colorIndex: Int) -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        let indexAfterModulo = colorIndex % Constants.chatBubbleColors.count
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.hexStringToUIColor(hex: Constants.chatBubbleColors[indexAfterModulo]))
    }

    // TODO: change the color of the chat user
    // based on who it is
    // annonymous colors
    private func setupIncomingBubble(colorIndex: Int) -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        let indexAfterModulo = colorIndex % Constants.chatBubbleColors.count
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.hexStringToUIColor(hex: Constants.chatBubbleColors[indexAfterModulo])) // UIColor.jsq_messageBubbleLightGray())
        
    }

    override func collectionView(_: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return messages[indexPath.item]
    }

    override func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        if(isOwner){
            return nil
        }
        
        if(messages[indexPath.item].isStudent!){
            return studentBadge
        }
        else{
            return nil
        }
    }

//    MARK: - Custom Methods

    // Update task participate count at user node
    func setTaskParticipated() {
		if let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
			let usersRef = Database.database().reference().child("users")
			usersRef.child(userId).child("taskParticipated").observeSingleEvent(of: .value, with: { snapshot in

				if let arrUsersParticipated = snapshot.value as? [String: Any] {
					if var tasks = arrUsersParticipated["tasks"] as? [String] {
						if !tasks.contains(self.channelId!) {
							tasks.append(self.channelId!)
							let tasksParticipateUpdate = ["tasks": tasks, "count": tasks.count] as [String: Any]
							self.updateTaskAtServer(userId, tasksParticipateUpdate, usersRef)
						}
					} else {
						let tasksParticipateUpdate = ["tasks": [self.channelId], "count": 1] as [String: Any]
						self.updateTaskAtServer(userId, tasksParticipateUpdate, usersRef)
					}
				} else {
					let tasksParticipateUpdate = ["tasks": [self.channelId], "count": 1] as [String: Any]
					self.updateTaskAtServer(userId, tasksParticipateUpdate, usersRef)
				}
			})
			isParticipated = true
		}
    }

    // Update task Participate at Server
    func updateTaskAtServer(_ userID: String, _ taskDetail: [String: Any], _ ref: DatabaseReference) {
        ref.child(userID).child("taskParticipated").setValue(taskDetail)
    }

    /// depreciated
    /// move to sendFakeMessages(reply: true)
    func receiveFakeMessage() {
        let when = DispatchTime.now() // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            let id = "Robot"
            let name = ""
            let text = "\u{2B50}That’s it! We thank you for the help"
            let colorIndexAsInt = 0
            self.addMessage(withId: id, name: name, text: text, colorIndex: colorIndexAsInt, isStudent: false)
            self.finishReceivingMessage()
        }
        
        // Log fakeTask2 complete
        Analytics.logEvent(AnalyticsEventTutorialComplete, parameters: [:])
    }
	
    /// send first wave of fake messages
    func sendFakeMessages(reply: Bool = false) {
        self.inputToolbar.sendButtonOnRight = false
		let when = DispatchTime.now() // change 2 to desired number of seconds
        let r =  reply ? 3..<6: 0..<3
        let multiplier = reply ? 2 : 1.5
        
        for i in r {
            print("in for loop \(i)")
            DispatchQueue.main.asyncAfter(deadline: when + (Double((i % 3) + 1) * multiplier)) {
                let id = "Robot"
                let name = ""
                let text = self.fakeMessages[i]
                let colorIndexAsInt = 0
                self.addMessage(withId: id, name: name, text: text, colorIndex: colorIndexAsInt, isStudent: false)
                self.finishReceivingMessage()
                
                // if it's the last fake message set send button to true
                self.inputToolbar.sendButtonOnRight = i == 2 ? true : false
            }
        }
        
        
        if reply {
            Analytics.logEvent(AnalyticsEventTutorialComplete, parameters: [:])
        }
		
	}

    func checkTaskCompletion() {
        taskHandler = taskRef?.child("completed").observe(.value, with: { (snapshot) -> Void in
            if let isCompleted = snapshot.value as? Bool {
                if isCompleted == true {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }

//    func showLocationAlert() {
//        CMAlertController.sharedInstance.showAlert(nil, Constants.sLOCATION_ERROR, ["Not now", "Settings"]) { sender in
//            if let button = sender {
//                if button.tag == 1 {
//                    // for Move to Settings
//                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//                }
//            }
//        }
//    }

    func checkNotificationPermission() {
        checkStatusOfNotification { status in
            if status == .notDetermined {
                self.mDeclinedNotification = false
            } else {
                self.mDeclinedNotification = true
            }
        }
    }

    func showNotificationAlert() {
        if mDeclinedNotification == false {
            view.endEditing(true)
            CMAlertController.sharedInstance.showAlert(subtitle: NSLocalizedString("notification: chat", comment: ""),
                                                       buttons: ["Not now", "Sure"]) { sender in
                self.mDeclinedNotification = true
                if let button = sender {
                    if button.tag == 1 {
                        UIApplication.shared.applicationIconBadgeNumber = 0
                        requestForNotification()
                    }
                }
                if self.isFakeTask {
                    let when = DispatchTime.now() + 11.5 // change to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        } else {
            if isFakeTask {
                let when = DispatchTime.now() + 10 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

//// Helper function inserted by Swift 4.2 migrator.
//private func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
//    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value) })
//}
