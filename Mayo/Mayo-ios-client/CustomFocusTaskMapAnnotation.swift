//
//  CustomFocusTaskMapAnnotation.swift
//  Mayo-ios-client
//
//  Created by abiem  on 4/25/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import Cluster
import MapKit
import UIKit

class CustomFocusTaskMapAnnotation: Annotation {
    var currentCarouselIndex: Int?
    var taskUserId: String?

    init(currentCarouselIndex: Int, taskUserId: String) {
        self.currentCarouselIndex = currentCarouselIndex
        self.taskUserId = taskUserId
    }
}
