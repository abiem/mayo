//
//  LocationManagerExtension.swift
//  Mayo-ios-client
//
//  Created by abiem  on 5/25/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import CoreLocation
import UIKit

let exitAreaRadius = 200.0
extension MainViewController: UserLocationManagerDelegate {
    /// updating user locations
    /// # What it updates
    ///     Updates sample posts location
    ///
    /// ToDo: I don't what else it does honestly - Jeremy
	func updateLocation(_: CLLocationManager, location: CLLocation) {
		
        // if the user moves 20 m
        addCurrentUserLocationToFirebase()
        if tasks.count == 0, onboarding2Done() {
			
			// TODO: fix; what does this do though? - Jeremy
			let timeStamp = Int(NSDate.timeIntervalSinceReferenceDate * 1000)
			tasks.append(
				Task(userId: currentUserId!, taskDescription: "", latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, completed: false, timeCreated: Date(), timeUpdated: Date(), taskID: "\(timeStamp)", recentActivity: false, userMovedOutside: false, postType: "Asking")
			)
			carouselView.reloadData()
		} else {
			var flag : Bool = false
			for (_, task) in tasks.enumerated() {
                if task?.taskDescription == Constants.SAMPLE_POST.EXAMPLE_TASK.desc {
					if self.calculateDistance(location.coordinate.latitude, location.coordinate.longitude, task?.latitude ?? 0.00, task?.longitude ?? 0.00) > 1000 {
						task?.latitude = location.coordinate.latitude + 0.0003
						task?.longitude = location.coordinate.longitude + 0.0003
						flag = true
					}
				}
				else if task?.taskDescription == Constants.SAMPLE_POST.SWIPE_LEFT.desc {
					if self.calculateDistance(location.coordinate.latitude, location.coordinate.longitude, task?.latitude ?? 0.00, task?.longitude ?? 0.00) > 1000 {
						task?.latitude = location.coordinate.latitude + 0.0001
						task?.longitude = location.coordinate.longitude + 0.0001
						flag = true
					}
				}
				else if task?.taskDescription == Constants.SAMPLE_POST.FIRST_POST_LIVE.desc {
					if self.calculateDistance(location.coordinate.latitude, location.coordinate.longitude, task?.latitude ?? 0.00, task?.longitude ?? 0.00) > 1000 {
						task?.latitude = location.coordinate.latitude + 0.0003
						task?.longitude = location.coordinate.longitude - 0.0003
						flag = true
					}
				}
			}
			if flag {
				carouselView.reloadData()
			}
		}

        // get current time
        let currentTime = Date()
        if lastUpdatedTime != nil {
            let timeDifference = currentTime.seconds(from: lastUpdatedTime!)
            print("time difference for task: \(timeDifference)")

            // if time difference is greater than 10 mins (600 seconds)
            // return and don't add this task to tasks
            if timeDifference > LOCATION_UPDATE_IN_SECOND {
                // Update Location
                lastUpdatedTime = NSDate() as Date
                UpdateUserLocationServer()
            }
			else{
				UserLocation.shared.endBackgroundTask()
			}
        } else {
            // Update Location
            lastUpdatedTime = NSDate() as Date
            UpdateUserLocationServer()
        }
    }

    // Geo Facing for 200 meters
    func setUpGeofenceForTask(_ lat: CLLocationDegrees, _ long: CLLocationDegrees) {
        if UserLocation.shared.getLocation()?.coordinate.latitude == nil, UserLocation.shared.getLocation()?.coordinate.longitude == nil {
            return
        }
        if calculateDistance(lat, long) > exitAreaRadius {
            userMovedAway()
        } else {
            let geofenceRegionCenter = CLLocationCoordinate2DMake(lat, long)
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 1, identifier: "taskDistanceExpired")
            geofenceRegion.notifyOnExit = true
			UserLocation.shared.startMonitoring(geofenceRegion: geofenceRegion)
        }
    }

    // user move away task area
    func exitsFromRegion(_: CLLocationManager, didExitRegion region: CLRegion) {
        userMovedAway()
		UserLocation.shared.stopMonitoring(geofenceRegion: region)
    }
	
	func calculateDistance(_ lat1: Double, _ long1: Double, _ lat2: Double, _ long2: Double) -> Double {
		let loc1 = CLLocation(latitude: lat1, longitude: long1)
		let loc2 = CLLocation(latitude: lat2, longitude: long2)
		return loc2.distance(from: loc1)
	}

    func calculateDistance(_ lat: CLLocationDegrees, _ long: CLLocationDegrees) -> Double {
		if let loc = UserLocation.shared.getLocation(){
			return loc.distance(from: CLLocation(latitude: lat, longitude: long))
		}
		return 0
    }
    
    func userMovedAway() {
        
        guard let currentUserTask = tasks[0] else { return }
        if currentUserTask.taskDescription != "", currentUserTask.userMovedOutside == false {
            checkTaskRecentActivity(currentUserTask, callBack: { isActivity in
                if isActivity {
                    currentUserTask.userMovedOutside = true
                    currentUserTask.recentActivity = isActivity
                    self.tasks[0] = currentUserTask
                    currentUserTask.updateFirebaseTask()
                } else {
                    currentUserTask.completed = true
                    currentUserTask.completeType = Constants.STATUS_FOR_MOVING_OUT
                    self.createLocalNotification(title: "You’re out of range so the post ended :(", body: "📍Post again?", time: Int(0.5))
                    self.removeTaskAfterComplete(currentUserTask)
                    if self.expirationTimer != nil {
                        self.expirationTimer?.invalidate()
                        self.expirationTimer = nil
                    }
                }
				UserLocation.shared.endBackgroundTask()
            })
        }
    }
	
	func updateLocationFailed(_ manager: CLLocationManager, didFailWithError error: Error) {
		if let error = error as? CLError, error.code == .denied {
			UserLocation.shared.stopUpdateLocations()
			isLocationNotAuthorised = true
			UserLocation.shared.endBackgroundTask()
			showLocationAlert()
			return
		}
	}

    func showLocationAlert() {
        CMAlertController.sharedInstance.showAlert(subtitle: Constants.sLOCATION_ERROR,
                                                   buttons: ["Not now", "Settings"]) { sender in
            if let button = sender {
                if button.tag == 1 {
                    // for Move to Settings
					UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                }
            }
        }
    }
}

//// Helper function inserted by Swift 4.2 migrator.
//private func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
//    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value) })
//}
