//
//  ReferralViewController.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 30/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let UserDefaultsReferralURLForUserKey = "userDefaultsReferralURLForUserKey"

class ReferralViewController: UIViewController {
    @IBOutlet weak var insetView: UIView!
    @IBOutlet weak var linkView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var tapToCopyLabel: UILabel!
    
    var request: Request?
    var jsonReferralFullLink: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.shadowColor = UIColor.black.cgColor
        self.view.layer.shadowOpacity = 0.7
        self.view.layer.shadowOffset = .zero
        self.view.layer.shadowRadius = 30
        
//        self.view.layer.shadowPath = UIBezierPath(rect: self.view.bounds).cgPath
//        self.view.layer.shouldRasterize = true
//        self.view.layer.rasterizationScale = UIScreen.main.scale
        
        shareView.backgroundColor = Color.shared.accentGreen
        linkView.backgroundColor = Color.shared.accentGreen
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.linkViewTouched(_:)))
        linkView.addGestureRecognizer(tap1)

        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.shareViewTouched(_:)))
        shareView.addGestureRecognizer(tap2)

        addCloseButton()
        
        guard let referralURL = UserDefaults.standard.value(forKey: UserDefaultsReferralURLForUserKey) as? String else {
            NSLog("No referral URL found!")
            return
        }
        
        jsonReferralFullLink = referralURL
        linkLabel.text = referralURL
    }

    @objc
    func linkViewTouched(_: UITapGestureRecognizer) {
        NSLog("Link view touched")
        
        if jsonReferralFullLink != nil {
            UIPasteboard.general.string = linkLabel.text
            tapToCopyLabel.text = "COPIED!"
        }
    }

    @objc
    func shareViewTouched(_: UITapGestureRecognizer) {
        let string = "Come join me on Mayo. We can help others in times of need. No sign up required. Just tap the link the join instantly."
        let url = URL(string: self.jsonReferralFullLink!)!

        let activityViewController = UIActivityViewController(activityItems: [string, url],
                                     applicationActivities: nil)

        activityViewController.excludedActivityTypes = [.postToFacebook, .postToTwitter, .postToFlickr, .postToTencentWeibo, .postToVimeo, .postToWeibo, .airDrop, .addToReadingList, .assignToContact, .print, .saveToCameraRoll]
                
        activityViewController.completionWithItemsHandler = { activity, success, items, error in

            print("Sharing done. act = \(String(describing: activity)),  success = \(String(describing: success)), items: \(String(describing: items)), err: \(String(describing: error))")
            self.dismiss(animated: true, completion: nil)
        }
        
        present(activityViewController, animated: true)
    }
    
    func addCloseButton() {
        let distFromCorner = CGFloat(25.0)
        let size = CGFloat(12.0)
        let insetButton = CGFloat(20.0)
        
//        let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: size + 2 * insetButton, height: size + 2 * insetButton))
        let closeButton = UIButton()
        
        closeButton.contentMode = .center
        
        let closeImage = UIImage(named: "closeGreen")
        closeButton.isUserInteractionEnabled = true

        closeButton.setImage(closeImage, for: .normal)
//        closeButton.center.x = insetView.bounds.maxX - distFromCorner
//        closeButton.center.y = distFromCorner
        closeButton.addTarget(self, action: #selector(closeReferralTouched(_:)), for: UIControl.Event.touchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.imageView?.translatesAutoresizingMaskIntoConstraints = false
        
        insetView.addSubview(closeButton)
        
        NSLayoutConstraint.activate([
            closeButton.trailingAnchor.constraint(equalTo: insetView.trailingAnchor, constant: -20),
            closeButton.topAnchor.constraint(equalTo: insetView.topAnchor, constant: 20),
            closeButton.imageView!.widthAnchor.constraint(equalToConstant: 15),
            closeButton.imageView!.heightAnchor.constraint(equalToConstant: 15)
        ])
    }

    @objc
    func closeReferralTouched(_: UITapGestureRecognizer) {
        request?.cancel()
        dismiss(animated: true)
    }
}
