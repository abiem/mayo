//
//  MainViewController+Task.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import iCarousel
import CoreLocation
import Firebase

// TODO: - Implement generic remove task func
extension MainViewController {
    func appendNewTask(_ newTask: Task) {
        print("tasks: \(tasks)")
        print("tasks count: \(tasks.count)")

        if newTask.completed == true {
            tasks.append(newTask)
        } else if newTask.userId == currentUserId, newTask.timeUpdated.addingTimeInterval(Double(SECONDS_IN_HOUR)) > Date(), newTask.taskDescription != "loading" {
            tasks[0] = newTask
            addMapPin(task: newTask, carouselIndex: 0)
            currentUserTaskSaved = true
        } else {
            let carouselIndex = getFakeTasksCount()
            tasks.insert(newTask, at: carouselIndex)
            addMapPin(task: newTask, carouselIndex: carouselIndex)
        }
        sortTaskAccordingToTime()
//    self.carouselView.reloadData()
        // add map pin for new task
        // add carousel index
        updateMapAnnotationCardIndexes()
        clusterManager.reload(mapView: mapView)
        // CHECK update all of the map annotation indexes

        if tasks.count == 2, mTaskDescription == nil, currentUserTaskSaved == false {
            carouselView.scrollToItem(at: 1, animated: true)
        }
        updateMapAnnotationCardIndexes()
    }

    // action for close button item
    @objc func discardCurrentUserTask(sender: UIButton) {
        mTaskDescription = nil
        let currentUserTextView = view.viewWithTag(CURRENT_USER_TEXTVIEW_TAG) as! UITextView
        currentUserTextView.resignFirstResponder()
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: Constants.COMPOSE_LONG_PLACEHOLDER_VIEWED) {
            currentUserTextView.text = Constants.COMPOSE_SHORT_PLACEHOLDER
        } else  {
            currentUserTextView.text = Constants.COMPOSE_LONG_PLACEHOLDER
        }
        currentUserTextView.alpha = 0.5

        sender.alpha = 0.5
        sender.isEnabled = false
        if let post_new_task_button = self.view.viewWithTag(self.POST_NEW_TASK_BUTTON_TAG) as? UIButton {
            post_new_task_button.alpha = 0.5
            post_new_task_button.isEnabled = false
        }
    }
    
    /// remove First Post Sample post
    @objc func removeFirstPostLive(sender _:UIButton) {
        for (i, task) in self.tasks.enumerated() {
            if task?.taskDescription == Constants.SAMPLE_POST.FIRST_POST_LIVE.desc {
                // User id is hard coded for now
                // TODO: move fakeUserId to constants
                removeOnboardingFakeTask(carousel: self.carouselView, cardIndex: i, userId: (task?.taskID!)!)
                
                let defaults = UserDefaults.standard
                defaults.set(true, forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.viewed)
                
                carouselView.scrollToItem(at: 0, animated: true)
                
                
            }
        }
    }
    
    func checkAndRemoveOnboardingTasks(carousel: iCarousel, cardIndex: Int) {
        let defaults = UserDefaults.standard

        if cardIndex < 0 || cardIndex >= tasks.count {
            return
        }

        // check if one of the onboarding tasks is at the current index
        let currentTask = tasks[cardIndex]!

        if let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
            if currentTask.taskDescription == Constants.SAMPLE_POST.EXAMPLE_TASK.desc {
                // finishing example post will give user one mayo point
                usersRef?.child(currentUserId!).child("demoTaskShown").setValue(true)
                UpdatePointsServer(1, userId)
                mTaskScore = mTaskScore + 1
                usersRef?.child(userId).child("score").setValue(mTaskScore)
                defaults.set(true, forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed)
            }
            
            removeOnboardingFakeTask(carousel: carousel, cardIndex: cardIndex, userId: currentTask.taskID!)
        }
    }

    // remove the task if it is onboarding task
    // FIXME: for some reason sometimes it doesn't remove the annot on the map
    func removeOnboardingFakeTask(carousel _: iCarousel, cardIndex: Int, userId: String) {
        // delete that task and card and map icon
        tasks.remove(at: cardIndex)
        carouselView.removeItem(at: cardIndex, animated: true)
        // carouselView.reloadData()
        
        let when = DispatchTime.now() // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            for annotation in self.mapView.annotations {
                // why are there two instances of these code blocks?
                if annotation is CustomFocusTaskMapAnnotation {
                    let customAnnotation = annotation as! CustomFocusTaskMapAnnotation
                    print("customFocusAnnotation.taskUserId :  \(customAnnotation.taskUserId) vs userId: \(userId)")
                    
                    if customAnnotation.taskUserId == userId {
                        // if its equal to the current index remove it
                        print("Deleted Id \(userId)")
                        self.clusterManager.remove(customAnnotation)
                        self.mapView.removeAnnotation(customAnnotation)
                        self.clusterManager.reload(mapView: self.mapView)
                    }
                }

                if annotation is CustomTaskMapAnnotation {
                    let customAnnotation = annotation as! CustomTaskMapAnnotation
                    print("customFocusAnnotation.taskUserId :  \(customAnnotation.taskUserId) vs userId: \(userId)")
                    if customAnnotation.taskUserId == userId {
                        // if its equal to the current index remove it
                        print("Deleted Id \(userId)")
                        self.clusterManager.remove(customAnnotation)
                        self.mapView.removeAnnotation(customAnnotation)
                        self.clusterManager.reload(mapView: self.mapView)
                    }
                }
                self.clusterManager.reload(mapView: self.mapView)
            }
            // update the rest of the annotations
            self.updateMapAnnotationCardIndexes()
        }
    }

    func getFakeTasksCount() -> Int {
        var count = 1
        let defaults = UserDefaults.standard
        let boolForTask1 = defaults.bool(forKey: Constants.ONBOARDING_TASK1_VIEWED_KEY)
        let boolForTask2 = defaults.bool(forKey: Constants.ONBOARDING_TASK2_VIEWED_KEY)
        let boolForTask3 = defaults.bool(forKey: Constants.ONBOARDING_TASK3_VIEWED_KEY)
        if boolForTask3 == false && boolForTask1 == true && boolForTask2 == true ||
            (currentUserTaskSaved == true) {
            //            (newItemSwiped == true || currentUserTaskSaved == true) {
            count = 1
        }
        if boolForTask1 != true {
            count += 1
        }
        if boolForTask2 != true {
            count += 1
        }
        if boolForTask3 != true {
            count += 1
        }
        return count
    }
    
    // get tasks around current location
    func queryTasksAroundCurrentLocation() {
        
        /// current user location
        if let center = UserLocation.shared.getLocation() {
            indicatorView.isHidden = false
            let when = DispatchTime.now() + 8
            DispatchQueue.main.asyncAfter(deadline: when) {
                // When No Task available
                self.removeFirebaseLoader()
            }
            
            // Query locations at latitude, longitutde with a radius of queryDistance
            // 200 meters = .2 for geofire units
            tasksCircleQuery = tasksGeoFire?.query(at: center, withRadius: queryDistance / 1000)
            
            removeCircle()
            addRadiusCircle(location: center)
            tasksDeletedCircleQueryHandle = tasksCircleQuery?.observe(.keyExited, with: { (key: String!, _: CLLocation!) in
                
                // when a new task is deleted
                print("a new key was deleted")
                
                // remove task with that id and get its index
                for (index, task) in self.tasks.enumerated() {
                    // if the task's id matches the key that was deleted
                    // and also check that the task is not equal to current user
                    if task?.userId == key, key != Auth.auth().currentUser?.uid {
                        // remove the task from the tasks array
                        self.tasks.remove(at: index)
                        
                        // remove that card based on its key
                        UIView.animate(withDuration: 1, animations: {
                            self.carouselView.removeItem(at: index, animated: true)
                        })
                        
                        // remove the pin for that card from the map
                        for annotation in self.mapView.annotations {
                            // check if its a task map annotation or focused task map annotaiton
                            if annotation is CustomTaskMapAnnotation {
                                let customAnnotation = annotation as! CustomTaskMapAnnotation
                                
                                // check if the index matches the index of the annotation
                                if customAnnotation.currentCarouselIndex == index {
                                    // if it matches remove the annotaiton
                                    self.mapView.removeAnnotation(customAnnotation)
                                    
                                    // and change the index for all the icons that are greater than it
                                    self.updatePinsAfterDeletion(deletedIndex: index)
                                }
                            }
                            
                            if annotation is CustomFocusTaskMapAnnotation {
                                let customFocusAnnotation = annotation as! CustomFocusTaskMapAnnotation
                                
                                // if the index of of annotation is equal to deleted index
                                if customFocusAnnotation.currentCarouselIndex == index {
                                    // remove this annotation
                                    self.mapView.removeAnnotation(customFocusAnnotation)
                                    
                                    // and change the index for all the icons that are greater than it
                                    self.updatePinsAfterDeletion(deletedIndex: index)
                                }
                            }
                            
                            // update annotation indexes
                            self.updateMapAnnotationCardIndexes()
                        }
                    }
                }
                
            })
            
            // listen for changes for when new tasks are created
            tasksCircleQueryHandle = tasksCircleQuery?.observe(.keyEntered, with: { (key: String!, location: CLLocation!) in
                print("Key '\(String(describing: key))' entered the search area and is at location '\(String(describing: location))'")
                
                // Remove loader
                self.removeFirebaseLoader()
                
                let taskRef = self.tasksRef?.child(key)
                self.tasksRefHandle = taskRef?.observe(DataEventType.value, with: { snapshot in
                    
                    let taskDict = snapshot.value as? [String: AnyObject] ?? [:]
                    print("key: \(String(describing: key)) task dictionary: \(taskDict)")
                    
                    let dateformatter = DateStringFormatterHelper()
                    // Check - don't add tasks that are older than 1 hour
                    if !taskDict.isEmpty, taskDict["completed"] as! Bool == false {
                        // get the time created for the current task
                        let taskTimeCreated = dateformatter.convertStringToDate(datestring: taskDict["timeUpdated"] as! String)
                        
                        // get current time
                        let currentTime = Date()
                        
                        // get the difference between time created and current time
                        let timeDifference = currentTime.seconds(from: taskTimeCreated)
                        print("time difference for task: \(timeDifference)")
                        
                        // if time difference is greater than 1 hour (3600 seconds)
                        // return and don't add this task to tasks
                        if timeDifference > self.SECONDS_IN_HOUR {
                            return
                        }
                    }
                    
                    // only process taskDict if not completed
                    // and not equal to own uid
                    // Remove Complete from here
                    // Lakshmi
                    //                && (taskDict["createdby"] as? String  != FIRAuth.auth()?.currentUser?.uid)
                    if !taskDict.isEmpty {
                        //
                        // send the current user local notification
                        // that there is a new task
                        
                        // Check - don't add duplicates
                        
                        // check task exists
                        for task in self.tasks {
                            // the task is already present in the tasks
                            if task?.taskID == taskDict["taskID"] as? String {
                                // for creator of Task or already existing Task
                                if taskDict["completed"] as! Bool == true {
                                    for (index, task) in self.tasks.enumerated() {
                                        if task?.taskID == taskDict["taskID"] as? String, task?.completed == false, self.currentUserId != taskDict["createdby"] as? String {
                                            self.tasks[index]?.completed = true
                                            self.tasks.append(self.tasks[index])
                                            self.tasks.remove(at: index)
                                            
                                            self.removeCarousel(index)
                                            self.removeAnnotationForTask((task?.taskID)!)
                                            self.sortTaskAccordingToTime()
                                            
                                            if self.tasks.count <= 1 {
                                                //self.newItemSwiped = true
                                                self.carouselView.reloadData()
                                            }
                                            
                                            
                                        }
                                    }
                                }
                                // update annotation indexes
                                self.updateMapAnnotationCardIndexes()
                                // return so no duplicates are added
                                return
                            }
                        }
                        
                        
                        // Send Task notification
                        self.sendNewTaskNotification()
                        
                        // adds key for task to chat channels array
                        self.chatChannels.append(taskDict["taskID"] as! String)
                        
                        var taskStartColor: String?
                        var taskEndColor: String?
                        //
                        
                        let newTask = Task(dict: taskDict, location: location)
                        //          let newTask = Task(userId: key, taskDescription: taskDescription, latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, completed: taskCompleted, timeCreated: taskTimeCreated, timeUpdated: taskTimeUpdated, taskID: "\(timeStamp)",recentActivity: recentActivity, userMovedOutside: userMovedOutside)
                        
                        if(taskDict["userVerified"] != nil){
                            if(taskDict["userVerified"] as! Bool == true){
                                newTask.isCreatorStudent = true
                            }
                            else{
                                newTask.isCreatorStudent = false
                            }
                        }
                        else{
                            newTask.isCreatorStudent = false
                        }
                        
                        // check if the task already has start and end colors saved
                        if taskDict["startColor"] != nil, taskDict["endColor"] != nil {
                            // if they have start color and end color
                            taskStartColor = taskDict["startColor"] as? String
                            taskEndColor = taskDict["endColor"] as? String
                            
                            newTask.setGradientColors(startColor: taskStartColor, endColor: taskEndColor)
                        } else {
                            // if they have nil for start and end colors
                            newTask.setGradientColors(startColor: nil, endColor: nil)
                        }
                        if newTask.userMovedOutside == true, newTask.completed == false {
                            self.checkTaskParticipation(newTask, callBack: { isParticipated in
                                if isParticipated {
                                    self.appendNewTask(newTask)
                                }
                            })
                            
                        } else {
                            self.appendNewTask(newTask)
                        }
                        
                        
                    } else if !taskDict.isEmpty, taskDict["completed"] as! Bool == true {
                        
                        
                        
                        for (index, task) in self.tasks.enumerated() {
                            if task?.taskID == taskDict["taskID"] as? String {
                                self.removeCarousel(index)
                                self.tasks.remove(at: index)
                                self.removeAnnotationForTask((task?.taskID)!)
                                self.updateMapAnnotationCardIndexes()
                                
                                if self.tasks.count == 0 {
                                    UserDefaults.standard.set(nil, forKey: Constants.PENDING_TASKS)
                                    if self.tasks.count == 0 {
                                        print("current user task created")
                                        
                                        if let loc = UserLocation.shared.getLocation() {
                                            // TODO: fix
                                            let timeStamp = Date().toMillis()
                                            self.tasks.append(
                                                Task(userId: self.currentUserId!, taskDescription: "", latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude, completed: false, timeCreated: Date(), timeUpdated: Date(), taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false, postType: "Asking")
                                            )
                                            
                                            self.carouselView.reloadData()
                                            self.updateMapAnnotationCardIndexes()
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                })
            })
        }
    }
    
    /// check if tasks is one of the sample posts
    func isSamplePost(_ task: Task) -> Bool{
        return task.description == Constants.SAMPLE_POST.EXAMPLE_TASK.desc || task.description == Constants.SAMPLE_POST.SWIPE_LEFT.desc || task.description == Constants.SAMPLE_POST.FIRST_POST_LIVE.desc
    }

    // send a local notification to user when a new task has been added
    func sendNewTaskNotification() {
        //        self.createLocalNotification(title: "New task nearby", body: "A new task was created nearby")
    }

    // TODO: change implementation to return nothing (will also have to edit everywhere it's being called
    /// creates sample posts
    func addSamplePost(_ fakeTaskShownKey: String, at index: Int, shift theresExampleTask: Bool = false) {
        let defaults = UserDefaults.standard
        let _ = defaults.bool(forKey: fakeTaskShownKey)
        defaults.set(true, forKey: fakeTaskShownKey)
        var taskDesc = ""
        var userId = ""
        var isComplete = false
        switch fakeTaskShownKey {
        case Constants.SAMPLE_POST.EXAMPLE_TASK.shown:
            taskDesc = Constants.SAMPLE_POST.EXAMPLE_TASK.desc
            userId = Constants.SAMPLE_POST.EXAMPLE_TASK.fakeId
        case Constants.SAMPLE_POST.SWIPE_LEFT.shown:
            taskDesc = Constants.SAMPLE_POST.SWIPE_LEFT.desc
            userId = Constants.SAMPLE_POST.SWIPE_LEFT.fakeId
        case Constants.SAMPLE_POST.FIRST_POST_LIVE.shown:
            taskDesc = Constants.SAMPLE_POST.FIRST_POST_LIVE.desc
            userId = Constants.SAMPLE_POST.FIRST_POST_LIVE.fakeId
        case Constants.SAMPLE_POST.HISTORY_TITLE.shown:
            taskDesc = Constants.SAMPLE_POST.HISTORY_TITLE.desc
            userId = Constants.SAMPLE_POST.HISTORY_TITLE.fakeId
            isComplete = true
        default:
            taskDesc = ""
            userId = "fakeUserId5"
        }
        
        let randDouble1 = Double.random(in: 0.00015...0.0003)
        let randDouble2 = Double.random(in: 0.00015...0.0003)
        
        
        
        // update user location
        getCurrentUserLocation()
        
        if let here = UserLocation.shared.getLocation() {
                let timeStamp = Date().toMillis() + Int64(50)
                let fakeTask = Task(userId: userId, taskDescription: taskDesc, latitude: here.coordinate.latitude + randDouble1, longitude: here.coordinate.longitude + randDouble2, completed: isComplete, taskID: "\(timeStamp)", recentActivity: false, userMovedOutside: false, postType: "Sample Post")
                
                if isComplete {
                    fakeTask.setGradientColors(startColor: CardColor.expireCard[0], endColor: CardColor.expireCard[1])
                } else {
                    fakeTask.setGradientColors(startColor: CardColor.samplePostColor[0], endColor: CardColor.samplePostColor[1])
                }
                
                
                // TODO: this caused an error sometimes
                if theresExampleTask {
                    self.tasks.insert(fakeTask, at: 2)
                } else if self.tasks.count > 1 {
                    self.tasks.insert(fakeTask, at: 1)
                }  else {
                    self.tasks.append(fakeTask)
                }
                
                // for testing
                if fakeTask.completed {
                    print("History Title Card desc : \(fakeTask.taskDescription)")
                    print("Before addMapIn")
                }
                addMapPin(task: fakeTask, carouselIndex: index)
        }
        carouselView.reloadData()
    }
    
    /// add history title card
    /// might not need since we can implement this in addSamplePost
    func addHistoryBookmark() {
        let defaults = UserDefaults.standard
        let boolForHistoryBookmark = defaults.bool(forKey: Constants.SAMPLE_POST.HISTORY_TITLE.shown)
        
          // only show if haven't been shown and there's history posts
          // will check again if there's existing history posts refer to theresHistory in MVC.swift
          
        if let here = UserLocation.shared.getLocation() {
            if !boolForHistoryBookmark {
                let timeStamp = Date().toMillis()
                
                let historyBookmark = Task(userId: "fakeuserid4", taskDescription: Constants.SAMPLE_POST.HISTORY_TITLE.desc, latitude: here.coordinate.latitude + 0.0002, longitude: here.coordinate.longitude - 0.0001, completed: true, taskID: "\(timeStamp!)", recentActivity: false, userMovedOutside: false, postType: "Sample Post")
                
                historyBookmark.setGradientColors(startColor: CardColor.expireCard[0], endColor: CardColor.expireCard[1])
                tasks.append(historyBookmark)
                addMapPin(task: historyBookmark, carouselIndex: tasks.count - 1)
            }
        }
          
    }
    
    //TODO into task handling
    func getSavedTask() -> Task? {
        if UserDefaults.standard.object(forKey: Constants.PENDING_TASKS) != nil {
            // Decode data
            let currentTaskData = UserDefaults.standard.object(forKey: Constants.PENDING_TASKS)
            if let task = currentTaskData as? Data {
                if let dicTask = NSKeyedUnarchiver.unarchiveObject(with: task) as? [String: Any] {
                    let userID = dicTask["userId"] as! String
                    let taskDescription = dicTask["taskDescription"] as! String
                    let latitude = dicTask["latitude"] as! CLLocationDegrees
                    let longitude = dicTask["longitude"] as! CLLocationDegrees
                    let completed = dicTask["completed"] as! Bool
                    let startColor = dicTask["startColor"] as! String
                    let endColor = dicTask["endColor"] as! String
                    let timeCreated = dicTask["timeCreated"] as! Date
                    let timeUpdated = dicTask["timeUpdated"] as! Date
                    let taskID = dicTask["taskID"] as! String
                    let recentActivity = dicTask["recentActivity"] as! Bool
                    let userMovedOutside = dicTask["userMovedOutside"] as! Bool
                    let postType = dicTask["postType"] as! String

                    let currentTask = Task(userId: userID, taskDescription: taskDescription, latitude: latitude, longitude: longitude, completed: completed, timeCreated: timeCreated, timeUpdated: timeUpdated, taskID: taskID, recentActivity: recentActivity, userMovedOutside: userMovedOutside, postType: postType)
                    currentTask.startColor = startColor
                    currentTask.endColor = endColor
                    return currentTask
                }
            }
        }
        return nil
    }
    // get Previous Task saved in user Defaults
    func getPreviousTask() {
        if UserDefaults.standard.object(forKey: Constants.PENDING_TASKS) != nil {
            // Decode data
            let currentTaskData = UserDefaults.standard.object(forKey: Constants.PENDING_TASKS)
            if let task = currentTaskData as? Data {
                if let dicTask = NSKeyedUnarchiver.unarchiveObject(with: task) as? [String: Any] {
                    let userID = dicTask["userId"] as! String
                    let taskDescription = dicTask["taskDescription"] as! String
                    let latitude = dicTask["latitude"] as! CLLocationDegrees
                    let longitude = dicTask["longitude"] as! CLLocationDegrees
                    let completed = dicTask["completed"] as! Bool
                    let startColor = dicTask["startColor"] as! String
                    let endColor = dicTask["endColor"] as! String
                    let timeCreated = dicTask["timeCreated"] as! Date
                    let timeUpdated = dicTask["timeUpdated"] as! Date
                    let taskID = dicTask["taskID"] as! String
                    let recentActivity = dicTask["recentActivity"] as! Bool
                    let userMovedOutside = dicTask["userMovedOutside"] as! Bool
                    // This handles posts from previous versions
                    let postType = dicTask["postType"] as? String ?? "Other" // if key doesn't exists just put "Other"

                    let currentTask = Task(userId: userID, taskDescription: taskDescription, latitude: latitude, longitude: longitude, completed: completed, timeCreated: timeCreated, timeUpdated: timeUpdated, taskID: taskID, recentActivity: recentActivity, userMovedOutside: userMovedOutside, postType: postType)
                    currentTask.startColor = startColor
                    currentTask.endColor = endColor

                    // get current time
                    let currentTime = Date()

                    // get the difference between time created and current time
                    let timeDifference = currentTime.seconds(from: currentTask.timeCreated)
                    print("time difference for task: \(timeDifference)")

                    //                            self.newItemSwiped = true
                    currentUserTaskSaved = true
                    tasks.append(currentTask)

                    // add new annotation to the map for the current user's task
                    let currentUserMapTaskAnnotation = CustomCurrentUserTaskAnnotation(currentCarouselIndex: 0)
                    // set location for the annotation
                    currentUserMapTaskAnnotation.coordinate = CLLocationCoordinate2DMake(currentTask.latitude, currentTask.longitude)
                    mapView.addAnnotation(currentUserMapTaskAnnotation)

                    setUpGeofenceForTask(currentTask.latitude, currentTask.longitude)
                    carouselView.reloadData()
                    checkTaskTimeLeft()
                    // }
                } else {
                    currentUserTaskSaved = false
                    isLoadingFirebase = true
                }
            }
        } else {
            currentUserTaskSaved = false
            isLoadingFirebase = true
        }
    }
    
    /// completion function for check mark
    @objc
    func markTaskAsComplete() {
        // show the alert that completes the post
        showAlertForCompletion()
    }

    // delete the task conversation
    func deleteTaskConversationForUser(userId: String) {
        channelsRef?.child(userId).removeValue()
    }

    // delete the current user's task
    func deleteTaskForUser(userId: String) {
        tasksRef?.child(userId).removeValue()
    }

    // delete the task location
    func deleteTaskLocationForUser(userId: String) {
        tasksGeoFire?.removeKey(userId)
    }

    func deleteAndResetCurrentUserTask() {
        let timeStamp = Date().toMillis()
        let currentUserKey = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)
        tasks[0] = Task(userId: currentUserKey!, taskDescription: "", latitude: (UserLocation.shared.getLocation()?.coordinate.latitude)!, longitude: (UserLocation.shared.getLocation()?.coordinate.longitude)!, completed: true, taskID: "\(String(describing: timeStamp))", recentActivity: false, userMovedOutside: false, postType: "Asking")

        // delete the task
        tasksRef?.child(userTaskId!).removeValue()

        // delete the task location
        tasksGeoFire?.removeKey(userTaskId!)

        // delete the task conversation
        channelsRef?.child(userTaskId!).removeValue()

        // reset boolean flags

        // flag for if the current task is saved
        currentUserTaskSaved = false

        // flag for if user swiped for new task
//        newItemSwiped = true

        // reload carousel view for first card
        carouselView.reloadItem(at: 0, animated: true)

        // if the user is currently viewing their own card
        if carouselView.currentItemIndex == 0 {
            // transition to first card if there is another card
            if tasks.count > 1 {
                carouselView.scrollToItem(at: 1, animated: false)
//                newItemSwiped = true
                carouselView.reloadItem(at: 0, animated: true)

                // or else show the the swiped card
            } else {
//                newItemSwiped = true
                carouselView.reloadItem(at: 0, animated: true)
            }
        }
    }

    // MARK: - new task created

    // action for done button item
    // TODO split into UI responder and task work method - this is both
    @objc func createTaskForCurrentUser(sender _: UIButton) {
        // TODO: keyboard bug when user hits home button
        // create/update new task item for current user
        mTaskDescription = nil
        view.endEditing(true)
        showNotificationAlert(prompt: "notification: new post")
        
        

        // get textview
        let currentUserTextView = view.viewWithTag(CURRENT_USER_TEXTVIEW_TAG) as! UITextView
        let postTypeSCView = view.viewWithTag(POST_TYPE_TAG) as! UISegmentedControl
        
        if let currentUserTask = self.tasks[0] {
            // taskdescription to be textView
            guard let latitude = UserLocation.shared.getLocation()?.coordinate.latitude, let longitude = UserLocation.shared.getLocation()?.coordinate.longitude else {
                pleaseTurnOnGeodataAlert()
                return
            }
            currentUserTask.latitude = latitude
            currentUserTask.longitude = longitude
            currentUserTask.completed = false
            currentUserTask.taskDescription = currentUserTextView.text
            currentUserTask.timeCreated = Date()
            currentUserTask.timeUpdated = Date()
            currentUserTask.postType = postTypeSCView.selectedSegmentIndex == 0 ? "Asking" : "Giving"
            
            var isStudent = false
            if(UserDefaults.standard.object(forKey: "isStudent") != nil){
                isStudent = UserDefaults.standard.value(forKey: "isStudent") as! Bool
            }
            
            print("IS STUDENT: \(isStudent)")
            
            if(isStudent) {
                currentUserTask.isCreatorStudent = true
            }
            else {
                currentUserTask.isCreatorStudent = false
            }
            
            // update user task
            tasks[0] = currentUserTask
            // Current Task Created
            currentUserTaskSaved = true
            // save the user's current task
            currentUserTask.save(self)

            Analytics.logEvent(AnalyticsEventShare, parameters: [
                AnalyticsParameterContentType: "task",
            ])
            // Local Notification Implemented

            // Start monitoring Distance
            setUpGeofenceForTask(currentUserTask.latitude, currentUserTask.longitude)

            // TODO: create users list for current user's conversation channel
            // and update the users list by appending the current user's id to the list
            let currentUserChannelId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)
            // update the number of users in the channel
            // and update the current user to the users list
            channelsRef?.child(currentUserTask.taskID!).child("users").child(currentUserChannelId!).setValue(0)

            // self.channelsRef?.child(currentUserChannelId!).child("users_count").setValue(1)

            // add new annotation to the map for the current user's task
            let currentUserMapTaskAnnotation = CustomCurrentUserTaskAnnotation(currentCarouselIndex: 0)
            // set location for the annotation
            currentUserMapTaskAnnotation.coordinate = (UserLocation.shared.getLocation()?.coordinate)!
            mapView.addAnnotation(currentUserMapTaskAnnotation)

            // Send Push notification to nearby users.
            sendPushNotificationToNearbyUsers()

            // save current user task description to check if its
            // the same when timer is done
            //          startTimer(SECONDS_IN_HOUR)
            checkTaskTimeLeft()
            carouselView.reloadItem(at: 0, animated: true)
        }
        
        // add first post live sample post
        let defaults = UserDefaults.standard
        if !defaults.bool(forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.shown) {
            addSamplePost(Constants.SAMPLE_POST.FIRST_POST_LIVE.shown, at: 1, shift: !defaults.bool(forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed))
            // set defaults for swipe left
            defaults.set(true, forKey: Constants.SAMPLE_POST.SWIPE_LEFT.shown)
            defaults.set(true, forKey: Constants.SAMPLE_POST.SWIPE_LEFT.viewed)
        }
        

        carouselView.reloadItem(at: 0, animated: true)
    }


    // Start timer for one hour (Task Expiration)
    func startTimer(_ interval: Int) {
        // save current user task description to check if its
        // the same when timer is done

        // invalidate the current timer
        expirationTimer?.invalidate()

        // reset expiration timer
        expirationTimer = nil

        // start timer to check if it has expired
        expirationTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(interval), repeats: false) { Timer in

            // finish deleting task
            if self.tasks.count == 0 {
                return
            }
            // get the current user's task
            let currentUserTask = self.tasks[0]
            self.needSetExpiredTask(complition: { [weak self] bool in
                if bool == true {
                    // if the current user's task has not been completed
                    // and it is the same task (don't notify expiration if its a different task)
                    if currentUserTask?.completed != true, currentUserTask?.userId == self?.currentUserId {
                        // create notification that the task is out of time
                        currentUserTask?.completed = true
                        currentUserTask?.completeType = Constants.STATUS_FOR_TIME_EXPIRED
                        self?.tasks[0] = currentUserTask
                        // Check Recent Activity
                        self?.checkTaskRecentActivity(currentUserTask!, callBack: { activity in
                            if activity {
                                self?.createMarkCompleteView()
                                self?.createLocalNotification(title: "Your help post expired", body: "Did you get help? Remember to thank them!", time: Int(0.0))
                            } else {
                                UserDefaults.standard.set(nil, forKey: Constants.PENDING_TASKS)
//                                self?.newItemSwiped = true
                                self?.removeTaskAfterComplete(currentUserTask!)
                                self?.createLocalNotification(title: "Your help post expired", body: "🕐 Still need help?", time: Int(0.5))
                            }
                        })
                        // reset the current user's task
                        // delete the task if it has expired
                        // self.deleteAndResetCurrentUserTask()

                        // remove own annotation on the map
                        //  self.removeCurrentUserTaskAnnotation()
                    }
                }
                // reset expiration timer
                self?.expirationTimer = nil

                // invalidate the current timer
                Timer.invalidate()

            })
        }
    }

    func checkTaskTimeLeft() {
        let currentTask = tasks[0]
        tasksExpireObserver = tasksRef?.child((currentTask?.taskID)!).child("timeUpdated").observe(.value, with: { snapshot in
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["taskExpirationNotification"])
            let currentUserTask = self.tasks[0]

            // reset expiration timer
            self.expirationTimer = nil

            // invalidate the current timer
            self.expirationTimer?.invalidate()

            if let timeUpdated = snapshot.value as? String {
                let dateformatter = DateStringFormatterHelper()

                // get current time
                let currentTime = Date()

                // get the difference between time created and current time
                var timeDifference = currentTime.seconds(from: dateformatter.convertStringToDate(datestring: timeUpdated))

                // if time difference is greater than 1 hour (3600 seconds)
                // return and don't add this task to tasks

                if timeDifference > self.SECONDS_IN_HOUR {
                    currentUserTask?.completed = true
                    currentUserTask?.completeType = Constants.STATUS_FOR_TIME_EXPIRED
                    self.tasks[0] = currentUserTask
                    // Remove Card

                    self.tasks[0] = currentUserTask
                    self.checkTaskRecentActivity(currentUserTask!, callBack: { activity in
                        if activity {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                                self?.createMarkCompleteView()
                                self?.createLocalNotification(title: "Your post expired", body: "Did anyone help? Remember to thank them!", time: Int(0.0))
                            }
                        } else {
                            UserDefaults.standard.set(nil, forKey: Constants.PENDING_TASKS)
//                            self.newItemSwiped = true
                            self.removeTaskAfterComplete(currentUserTask!)
                            self.createLocalNotification(title: "Your post expired", body: "🕐 Still need help?", time: Int(0.0))
                        }
                    })
                } else {
                    timeDifference = self.SECONDS_IN_HOUR - timeDifference
                    self.startTimer(self.SECONDS_IN_HOUR)
                    var notificationTime = timeDifference
                    if notificationTime >= self.SECONDS_IN_FIFTEEN {
                        notificationTime = notificationTime - self.SECONDS_IN_FIFTEEN
                    } else {
                        notificationTime = timeDifference
                    }
                    self.createLocalNotification(title: "Your post is expiring soon", body: "👋🏻 Posts inactive for 1hr automatically expires so don’t forget to reply your messages.", time: notificationTime)
                }
            }
        })
    }
    
    /// Check if every sample posts in onboarding part 2 is done
    /// # Sample Post checked
    ///     - Example Task
    ///     - Swipe Left to User task
    ///     - First Post Live
    func onboarding2Done() -> Bool {
        let defaults = UserDefaults.standard
        let exampleTaskBool = defaults.bool(forKey: Constants.SAMPLE_POST.EXAMPLE_TASK.viewed)
        let swipeLeftBool = defaults.bool(forKey: Constants.SAMPLE_POST.SWIPE_LEFT.viewed)
        let firstPostLiveBool = defaults.bool(forKey: Constants.SAMPLE_POST.FIRST_POST_LIVE.viewed)
        
        return exampleTaskBool && swipeLeftBool && firstPostLiveBool
            
    }
}
