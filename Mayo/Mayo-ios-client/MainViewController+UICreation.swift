//
//  MainViewController+UICreation.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import SafariServices

// MARK: - UI creation
extension MainViewController {

//TODO take these UI creation helpers into category
    func createMarkCompleteView() {
        // let users know it was completed
        let currentUserTask = tasks[0]!
        let currentUserKey = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID)

        // get the channel's last messages for each user and then delete
        // the task, conversation channel, and location
        channelsRef?.child(currentUserTask.taskID!).observeSingleEvent(of: .value, with: { snapshot in

            // Get value for snapshot
            let value = snapshot.value as? NSDictionary
            let users = value?["users"] as? NSDictionary ?? [:]
            let messages = value?["messages"] as? NSDictionary ?? [:]

            // if nobody answered to usre's post, we'll not show view
            let messageCount = messages.count
            if messageCount < 1 {
                self.handleNoOneHelpedButtonPressed()
                return
            }

            print("conversation channel value: \(String(describing: value))")
            print("users \(users)")
            print("messages \(messages) keys \(messages.allKeys)")
            // let username = value?["username"] as? String ?? ""
            let messagesSorted = self.sortMessageArray(messages)
            var usersMessagesDictionary: [String: Any] = [:]
            for userKey in users.allKeys {
                if let userKeyString = userKey as? String {
                    if userKeyString == currentUserKey! {
                        // if the user key is the same as the current user
                        // continue to next iteration of loop
                        continue
                    }

                    // if the current userkey is not the current user
                    // use it to find the last message for this user
                    // add the user key and message to usermessages dictionary
                    for messageKey in messagesSorted {
                        // let message = messages[messageKey] as? [String: String]
                        if userKeyString == messageKey["senderId"] as! String {
                            // we found last message for this user,
                            // add data to users messages dictionary
                            usersMessagesDictionary[userKeyString] = messageKey
                            break
                        }
                    }
                }
            }

            // set textView back to editable
            if let currentUserTextView = self.view.viewWithTag(self.CURRENT_USER_TEXTVIEW_TAG) as? UITextView {
                currentUserTextView.isEditable = true
            }
            var startColor: UIColor?
            var endColor: UIColor?

            let state = UIApplication.shared.applicationState
            if state == .background {
                startColor = UIColor.hexStringToUIColor(hex: currentUserTask.startColor!)
                endColor = UIColor.hexStringToUIColor(hex: currentUserTask.endColor!)
            } else if state == .active {
                // get start and end color from card
                let shadowView = self.carouselView.itemView(at: 0)!
                let gradientView = shadowView.subviews[0] as! GradientView
                startColor = gradientView.startColor
                endColor = gradientView.endColor
            }

            // make carousel view invisible
            self.carouselView.isHidden = false

            // complete the current task
//            self.newItemSwiped = true

            // remove task annotation on mapview
            self.removeCurrentUserTaskAnnotation()

            self.carouselView.reloadItem(at: 0, animated: false)

            // create shadow view for completion view
            let completionShadowView = self.createCompletionShadowView()

            // finish present view to select who helped you
            let completionView = self.createCompletionGradientView(startColor: startColor!, endColor: endColor!)

            // add text label to ask who helped the user
            let completionLabel = self.createCompletionViewLabel(completionView: completionView)

            // loop through dictionary of users helped messages
            // maximum of 5 messages
            var count = 0
            for (userKey, userMessage) in usersMessagesDictionary {
                // loop through the dictionary and create
                // a button with message for each of the users up to 5
                let message = userMessage as! NSDictionary
                if count < 5 {
                    // create a button view and add it to the completion view
                    let chatUserMessageButton = self.createMessageToThankUser(messageText: message["text"] as! String, completionView: completionView, tagNumber: count, userId: userKey)
                    let lineView = UIView(frame: CGRect(x: -3,
                                                        y: chatUserMessageButton.frame.size.height - 5,
                                                        width: chatUserMessageButton.frame.size.width + 4,
                                                        height: 5.0))

                    lineView.backgroundColor = UIColor.hexStringToUIColor(hex: Constants.chatBubbleColors[Int(message["colorIndex"] as! String)!])
                    chatUserMessageButton.clipsToBounds = true
                    chatUserMessageButton.addSubview(lineView)

                    completionView.addSubview(chatUserMessageButton)
                    // update the count
                    count += 1
                } else {
                    break
                }
            }

            // add smiley face button for people who helped
            let usersHelpedButton = self.createUsersHelpedButton(completionView: completionView)

            // add frowney face button for no one helped
            let usersNoHelpButton = self.createUsersNoHelpButton(completionView: completionView)

            let lineView = UIView(frame: CGRect(x: 40, y: 0, width: completionView.bounds.width - 80, height: 2))
            lineView.backgroundColor = UIColor(white: 1, alpha: 0.3)
            lineView.frame.origin.y = completionView.bounds.height * 8.5 / 10
            // add subviews to view
            completionView.addSubview(usersHelpedButton)
            completionView.addSubview(lineView)
            completionView.addSubview(usersNoHelpButton)
            completionView.addSubview(completionLabel)

            // TODO: add slide up animation
            completionShadowView.addSubview(completionView)
            self.view.addSubview(completionShadowView)

        }) { error in
            print(error.localizedDescription)
        }
    }

    func createCompletionShadowView() -> UIView {
        let completionShadowView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width * 9 / 10, height: view.bounds.height * 9 / 10))
        completionShadowView.center = view.center
        completionShadowView.layer.shadowColor = UIColor.black.cgColor
        completionShadowView.layer.shadowOffset = CGSize(width: 0, height: 10)
        completionShadowView.layer.shadowOpacity = 0.3
        completionShadowView.layer.shadowRadius = 15.0
        completionShadowView.tag = COMPLETION_VIEW_TAG
        return completionShadowView
    }

    func createCompletionGradientView(startColor: UIColor, endColor: UIColor) -> UIView {
        let completionView = GradientView(frame: CGRect(x: 0, y: 0, width: view.bounds.width * 9 / 10, height: view.bounds.height * 9 / 10))
        completionView.startColor = startColor
        completionView.endColor = endColor
        completionView.layer.cornerRadius = 4
        completionView.clipsToBounds = true
        return completionView
    }

    func createCompletionViewLabel(completionView: UIView) -> UILabel {
        let completionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: completionView.bounds.width * 9 / 10, height: completionView.bounds.height))
        completionLabel.center.x = completionView.center.x
        completionLabel.center.y = completionView.bounds.height * 1 / 10
        completionLabel.font = UIFont.systemFont(ofSize: 24)
        completionLabel.text = "Who helped you out? Say thanks and pay it forward."
        completionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        completionLabel.numberOfLines = 2
        completionLabel.textColor = UIColor.white
        return completionLabel
    }

    func createUsersHelpedButton(completionView: UIView) -> UIButton {
        let usersHelpedButton = UIButton(frame: CGRect(x: 0, y: 0, width: completionView.bounds.width, height: 42))
        usersHelpedButton.titleLabel?.textAlignment = .center
        usersHelpedButton.setImage(UIImage(named: "smileyFace"), for: .normal)
        usersHelpedButton.isEnabled = false
        usersHelpedButton.setTitle("Thanks!", for: .normal)
        usersHelpedButton.tintColor = UIColor.white
        usersHelpedButton.center.x = completionView.center.x
        usersHelpedButton.center.y = completionView.bounds.height * 8 / 10
        usersHelpedButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        usersHelpedButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        usersHelpedButton.tag = USERS_HELPED_BUTTON_TAG
        usersHelpedButton.alpha = 0.5
        usersHelpedButton.addTarget(self, action: #selector(handleUsersHelpedButtonPressed), for: .touchUpInside)
        return usersHelpedButton
    }

    func createUsersNoHelpButton(completionView: UIView) -> UIView {
        let usersNoHelpButton = UIButton(frame: CGRect(x: 0, y: 0, width: completionView.bounds.width, height: 42))
        usersNoHelpButton.setImage(UIImage(named: "frownFace"), for: .normal)
        usersNoHelpButton.titleLabel?.textAlignment = .center
        usersNoHelpButton.setTitle("No one helped me", for: .normal)
        usersNoHelpButton.tintColor = UIColor.white
        usersNoHelpButton.center.x = completionView.center.x
        usersNoHelpButton.center.y = completionView.bounds.height * 9 / 10
        usersNoHelpButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        usersNoHelpButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        usersNoHelpButton.tag = NO_USERS_HELPED_BUTTON_TAG
        usersNoHelpButton.addTarget(self, action: #selector(handleNoOneHelpedButtonPressed), for: .touchUpInside)
        return usersNoHelpButton
    }

    func createMessageToThankUser(messageText: String, completionView: UIView, tagNumber: Int, userId: String) -> UIView {
        var buttonSize: CGFloat = completionView.bounds.width - 40
        let messageWidth = messageText.widthOfString(usingFont: UIFont.systemFont(ofSize: 16)) + 40
        if messageWidth < buttonSize {
            buttonSize = messageWidth
        }

        let chatUserMessageButton = UIButton(frame: CGRect(x: 20, y: 0, width: buttonSize, height: 57))
        chatUserMessageButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        chatUserMessageButton.tag = tagNumber
        chatUserMessageButton.setTitleColor(UIColor.black, for: .normal)
        chatUserMessageButton.center.y = completionView.bounds.height * 3 / 10 + CGFloat(62 * tagNumber)
        //        chatUserMessageButton.center.x = self.view.center.x-20
        chatUserMessageButton.setTitle(messageText, for: .normal)
        chatUserMessageButton.backgroundColor = UIColor.white
        chatUserMessageButton.contentHorizontalAlignment = .left
        chatUserMessageButton.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 5)
        chatUserMessageButton.cornerRadius = 5
        chatUserMessageButton.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingMiddle
        chatUserMessageButton.titleLabel?.numberOfLines = 2
        chatUserMessageButton.alpha = 0.5

        // add user id to the button layer as data passed
        chatUserMessageButton.layer.setValue(userId, forKey: "userId")

        // TODO: add target function for when the user clicks on a message button
        chatUserMessageButton.addTarget(self, action: #selector(handleCompletionViewChatUserMessageButtonPressed(sender:)), for: .touchUpInside)
        return chatUserMessageButton
    }
    
    //allows the user to verify themselves as a student
    @objc func verifyEmail() {
        
        //making alert
        let insertEmailAlert = UIAlertController(title: "Enter your .edu Email", message: "Students trust each other more. By verifying, you are trusted more on campus.\nEmail is only used for verifying your status and storing points", preferredStyle: .alert)
        insertEmailAlert.addTextField(configurationHandler: { (emailTextField) in
            emailTextField.placeholder = "Email"
        })
        //.cancel style is the one that makes it bold, so putting that on the "submit" button even though it's not actually cancelling
        
        //submit button to try and use email for verification
        insertEmailAlert.addAction(UIAlertAction(title: "Submit", style: .default, handler: { (action) in
            
            //gets the textfield from the alert
            if let emailTextField = insertEmailAlert.textFields?[0] {
                if(emailTextField.hasText){
                    
                    //checks to see if it's a valid edu email
                    let isValidEduEmail = self.checkIfEmailIsEdu(email: emailTextField.text!)
                    
                    if(isValidEduEmail){
                        
                        //Action code settings for the link sent through the email
                        let actionCodeSettings = ActionCodeSettings()
                        actionCodeSettings.url = URL(string: "https://heymayo.page.link")
                        actionCodeSettings.handleCodeInApp = true
                        actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
                        
                        //sending email link
                        Auth.auth().sendSignInLink(toEmail: emailTextField.text!, actionCodeSettings: actionCodeSettings, completion: { (error) in
                            if let error = error {
                                let errorAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                                errorAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
                                self.present(errorAlert, animated: true, completion: nil)
                                return
                            }
                        })
                        //saving the email locally
                        UserDefaults.standard.set(emailTextField.text, forKey: "email")
                        self.presentRightEmailAlert()
                    }
                    else{
                        self.presentWrongEmailAlert()
                    }
                }
            }
    
        }))
        insertEmailAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(insertEmailAlert, animated: true, completion: nil)
    }

    
   
    
    func closeWebView() {
        safariVC?.dismiss(animated: true, completion: nil)
    }

    // MARK: Helpers
    //regex to check if it's a valid .edu email
    func checkIfEmailIsEdu(email: String) -> Bool{
        do{
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.+-]+\\.edu$", options: .caseInsensitive)
            let matches = regex.matches(in: email, options: [], range: NSRange(location: 0, length: email.count))
            return matches.count > 0
        } catch {
            return false
        }
    }
}

