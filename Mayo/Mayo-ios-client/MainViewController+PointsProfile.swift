//
//  MainViewController+PointsProfile.swift
//  Mayo-ios-client
//
//  Created by Jeremy Tandjung on 4/28/20.
//  Copyright © 2020 abiem design. All rights reserved.
//

import UIKit
import MapKit
import Firebase

/// This extension handles the points profile
/// menu button on the top right of the app
extension MainViewController {
    
    /// create clickable point cirlce view on the top right
    func createPointsView() -> UIView {
        // create shadow view to superview diagonal gradient
        let shadowView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        shadowView.backgroundColor = UIColor.clear
        shadowView.center.x = view.bounds.maxX - 50
        shadowView.center.y = 73
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 10)
        shadowView.layer.shadowOpacity = 0.3
        shadowView.layer.shadowRadius = 15.0

        // create diagonal gradient to show points
//        let pointsGradientView = DiagonalGradientView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        let pointsGradientView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        pointsGradientView.layer.cornerRadius = pointsGradientView.bounds.width / 2
        // pointsGradientView.center.x = self.view.bounds.maxX - 50
        // pointsGradientView.center.y = 73
        pointsGradientView.layer.masksToBounds = true
        pointsGradientView.tag = POINTS_GRADIENT_VIEW_TAG
        pointsGradientView.backgroundColor = .white

        // create points count label to hold the number of points the current user has
        pointsLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        pointsLabel.text = "0"
        pointsLabel.font = UIFont.systemFont(ofSize: 24)
        pointsLabel.textColor = Color.shared.textGreen
        pointsLabel.textAlignment = .center
        pointsLabel.center.x = pointsGradientView.frame.size.width / 2
        pointsLabel.tag = POINTS_GRADIENT_VIEW_LABEL_TAG

        pointsGradientView.addSubview(pointsLabel)
        shadowView.addSubview(pointsGradientView)

        // add gesture to show points profile view when tapped
        let pointsTapGesture = UITapGestureRecognizer(target: self, action: #selector(pointsViewPressed(_:)))
        shadowView.addGestureRecognizer(pointsTapGesture)

        return shadowView
    }
    
    /// Shows Menu that consists of
    /// - Grey background
    /// - Your Mayo Points view
    /// - Menu
    @objc func pointsViewPressed(_ : UIGestureRecognizer) {
        print("Got to pointsViewPressed!")
        
        // Set map view to current location
        let region = MKCoordinateRegion(center: UserLocation.shared.getLocation()!.coordinate, span: MKCoordinateSpan(latitudeDelta: mapView.region.span.latitudeDelta, longitudeDelta: mapView.region.span.longitudeDelta))
        mapView.setRegion(region, animated: true)
        
        let grayBackground = createGrayBackground()
        let menuTable = createMenuTable()
        let yourMayoPointsView = createYourMayoPointsView()
        
        grayBackground.addSubview(yourMayoPointsView)
        grayBackground.addSubview(menuTable)
        
        
        if #available(iOS 11.0, *) {
            yourMayoPointsView.topAnchor.constraint(equalTo: grayBackground.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        } else {
            yourMayoPointsView.topAnchor.constraint(equalTo: grayBackground.topAnchor, constant: 10).isActive = true
        }
        
        // Constraints
        NSLayoutConstraint.activate([
            // horizontalGradientView Constraints
            yourMayoPointsView.centerXAnchor.constraint(equalTo: grayBackground.centerXAnchor),
            yourMayoPointsView.heightAnchor.constraint(equalToConstant: 350),
            yourMayoPointsView.widthAnchor.constraint(equalToConstant: grayBackground.frame.size.width - 30.0),
            
            // menuTable constraints
            menuTable.topAnchor.constraint(equalTo: yourMayoPointsView.bottomAnchor, constant: 20),
            menuTable.centerXAnchor.constraint(equalTo: yourMayoPointsView.centerXAnchor),
            menuTable.heightAnchor.constraint(equalToConstant: menuTable.rowHeight * CGFloat(menuItems.count) + 10 + 10),
            menuTable.widthAnchor.constraint(equalTo: yourMayoPointsView.widthAnchor)
        ])
        
        view.addSubview(grayBackground)
        
        // Logs tappedPointsTask
        Analytics.logEvent(AnalyticsEventTutorialComplete, parameters: [:])
    }
    
    /// create Menu Table
    /// Refer to MainVC+MenuTable.swift for delegate and data source
    func createMenuTable() -> UITableView {
        let menuTable = UITableView()
        menuTable.cornerRadius = 8
        menuTable.dataSource = self
        menuTable.delegate = self
        menuTable.separatorStyle = .none
        menuTable.translatesAutoresizingMaskIntoConstraints = false
        menuTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        menuTable.isScrollEnabled = false
        menuTable.rowHeight = 50
        menuTable.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        menuTable.backgroundColor = Color.shared.accentGreen

        return menuTable
    }
    
    /// Create gray backgroun view
    /// Overall view for the points display + menu
    func createGrayBackground() -> UIView {
        // show points pofile view
        let pointsProfileView = UIView(frame: CGRect(x: 0, y: 0, width: mapView.frame.size.width, height: view.bounds.height))
        pointsProfileView.center = view.center
        pointsProfileView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        pointsProfileView.tag = POINTS_PROFILE_VIEW_TAG

        return pointsProfileView
    }
    
    /// Creates the Points Profile View on the top of the view
    func createYourMayoPointsView() -> UIView {
        // create horizontal gradient card for showing points
//        let horizontalGradientView = HorizontalGradientView()
        let horizontalGradientView = UIView()
        horizontalGradientView.layer.cornerRadius = 8
        horizontalGradientView.clipsToBounds = true
        horizontalGradientView.translatesAutoresizingMaskIntoConstraints = false
        horizontalGradientView.backgroundColor = .white
        
        /*
         // create label for text
         let textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: horizontalGradientView.frame.size.width - 60, height: 50))
         textLabel.text = "Your mayo points can be exchanged for rewards in the future so hang on to it!"
         textLabel.textAlignment = .left
         textLabel.font = UIFont.systemFont(ofSize: 14)
         textLabel.numberOfLines = 2
         textLabel.adjustsFontSizeToFitWidth = true
         textLabel.center.x = horizontalGradientView.bounds.width / 2 - 10
         textLabel.center.y = 30
         textLabel.textColor = UIColor.white
         textLabel.minimumScaleFactor = 10 / UIFont.labelFontSize
         */
        
        // create label for 'You have'
        let youHaveLabel = UILabel()
        youHaveLabel.font = UIFont.boldSystemFont(ofSize: 22)
        youHaveLabel.textColor = UIColor.black
        youHaveLabel.textAlignment = .center
        youHaveLabel.text = "Your Mayo Points"
//        let closeGesture = UITapGestureRecognizer(target: self, action: #selector(removePointsProfile(_:)))
        youHaveLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // create label for score
        let scoreLabel = UILabel()
        scoreLabel.font = UIFont.systemFont(ofSize: 38)
        scoreLabel.textColor = UIColor.white
        scoreLabel.textAlignment = .center
        // get the score from points label and set it
        let pointsGradientViewLabel = view.viewWithTag(POINTS_GRADIENT_VIEW_LABEL_TAG) as? UILabel
        scoreLabel.text = "\((pointsGradientViewLabel?.text)!)"
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        
        //Create the imageview for the points star
        let pointsStarImageView = UIImageView()
        pointsStarImageView.image = UIImage(named: "mayopoints-star")
        pointsStarImageView.translatesAutoresizingMaskIntoConstraints = false
        
        // create close button
        let closeButton = UIButton()
        let closeImage = UIImage(named: "closeGreen")
        closeButton.setImage(closeImage, for: .normal)
        closeButton.imageView?.heightAnchor.constraint(equalToConstant: 15).isActive = true
        closeButton.imageView?.widthAnchor.constraint(equalToConstant: 15).isActive = true
        closeButton.imageView?.translatesAutoresizingMaskIntoConstraints = false
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(removePointsProfile(_:)), for: UIControl.Event.touchUpInside)
        
        
        //create Redeem Rewards Button (Disabled for now)
        let redeemRewardsButton = UIButton()
        redeemRewardsButton.setTitle("Redeem Rewards (Coming Soon)", for: .normal)
        redeemRewardsButton.setTitleColor(UIColor.white.withAlphaComponent(0.7), for: .normal)
        redeemRewardsButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        redeemRewardsButton.layer.borderWidth = 0
        redeemRewardsButton.layer.borderColor = UIColor.white.withAlphaComponent(0.2).cgColor
        redeemRewardsButton.backgroundColor = Color.shared.accentGreen
        redeemRewardsButton.layer.cornerRadius = 8
        redeemRewardsButton.translatesAutoresizingMaskIntoConstraints = false
        
        /*
        Student verification button
        removed for this release
         
        //checks if user is a studnet
        var isStudent = false
        if(UserDefaults.standard.object(forKey: "isStudent") != nil){
            isStudent = UserDefaults.standard.value(forKey: "isStudent") as! Bool
        }
        
        //create Verify As Student Button
        let verifyStudentButton = UIButton(frame: CGRect(x: 0, y: 0, width: horizontalGradientView.frame.size.width * 0.9, height: 50))
        verifyStudentButton.center.y = redeemRewardsButton.center.y + redeemRewardsButton.frame.height/2 + 40
        verifyStudentButton.center.x = redeemRewardsButton.center.x
        verifyStudentButton.setImage(UIImage(named: "iconBadgeEdu"), for: .normal)
        
        //if verified student, changes the text of the button
        if(isStudent){
            verifyStudentButton.setTitle("You're verified, omg!", for: .normal)
            verifyStudentButton.setTitleColor(UIColor.white.withAlphaComponent(0.7), for: .normal)
        }
        else{
            verifyStudentButton.setTitle("Verify as a student via email", for: .normal)
            verifyStudentButton.addTarget(self, action: #selector(verifyEmail), for: .touchUpInside)
        }
        verifyStudentButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        verifyStudentButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        verifyStudentButton.tintColor = UIColor.white
        verifyStudentButton.layer.borderWidth = 2
        verifyStudentButton.layer.borderColor = UIColor.white.withAlphaComponent(0.3).cgColor
        verifyStudentButton.layer.cornerRadius = 5
        
        //switches the position of the icon to the right, originally on the left
        verifyStudentButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        verifyStudentButton.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        verifyStudentButton.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        */
        
        let earnMayoPointsLabel = UILabel()
        earnMayoPointsLabel.textAlignment = .center
        earnMayoPointsLabel.translatesAutoresizingMaskIntoConstraints = false
        earnMayoPointsLabel.text = "Earn Mayo points when you get thanked for helping others."
        earnMayoPointsLabel.numberOfLines = 0
        earnMayoPointsLabel.textColor = .black
        
        horizontalGradientView.addSubview(youHaveLabel)
        horizontalGradientView.addSubview(pointsStarImageView)
        horizontalGradientView.addSubview(scoreLabel)
        horizontalGradientView.addSubview(redeemRewardsButton)
        horizontalGradientView.addSubview(closeButton)
        horizontalGradientView.addSubview(earnMayoPointsLabel)
//        horizontalGradientView.addGestureRecognizer(closeGesture)
        
        NSLayoutConstraint.activate([
            // closeButton Constraints
            closeButton.heightAnchor.constraint(equalToConstant: 44),
            closeButton.widthAnchor.constraint(equalToConstant: 44),
            closeButton.topAnchor.constraint(equalTo: horizontalGradientView.topAnchor, constant: 10),
            closeButton.trailingAnchor.constraint(equalTo: horizontalGradientView.trailingAnchor, constant: -10),
            
            // youHaveLabel contraints
            youHaveLabel.widthAnchor.constraint(equalToConstant: 200),
            youHaveLabel.heightAnchor.constraint(equalToConstant: 40),
            youHaveLabel.topAnchor.constraint(equalTo: horizontalGradientView.topAnchor, constant: 20),
            youHaveLabel.centerXAnchor.constraint(equalTo: horizontalGradientView.centerXAnchor),
            
            // pointsStarImageView Constraints
            pointsStarImageView.heightAnchor.constraint(equalToConstant: 170),
            pointsStarImageView.widthAnchor.constraint(equalToConstant: 170),
            pointsStarImageView.centerXAnchor.constraint(equalTo: horizontalGradientView.centerXAnchor),
            pointsStarImageView.topAnchor.constraint(equalTo: youHaveLabel.bottomAnchor, constant: -8),
            
            
            // scoreLabel constraints
            scoreLabel.heightAnchor.constraint(equalToConstant: 40),
            scoreLabel.widthAnchor.constraint(equalToConstant: 150),
            scoreLabel.centerXAnchor.constraint(equalTo: pointsStarImageView.centerXAnchor),
            scoreLabel.centerYAnchor.constraint(equalTo: pointsStarImageView.centerYAnchor, constant: -20),
            
            
            // redeemRewardsButton constraints
            redeemRewardsButton.heightAnchor.constraint(equalToConstant: 55),
            redeemRewardsButton.widthAnchor.constraint(equalTo: horizontalGradientView.widthAnchor, multiplier: 0.9),
            redeemRewardsButton.centerXAnchor.constraint(equalTo: horizontalGradientView.centerXAnchor),
            redeemRewardsButton.topAnchor.constraint(equalTo: pointsStarImageView.bottomAnchor, constant: -20),
            
            // earnMayoPointsLabel constraints
            earnMayoPointsLabel.centerXAnchor.constraint(equalTo: horizontalGradientView.centerXAnchor),
            earnMayoPointsLabel.topAnchor.constraint(equalTo: redeemRewardsButton.bottomAnchor, constant: 30),
            earnMayoPointsLabel.widthAnchor.constraint(equalTo: redeemRewardsButton.widthAnchor, constant: -10)
        ])
        
        return horizontalGradientView
    }
    
   /// remove points profile view
   @objc func removePointsProfile(_: UITapGestureRecognizer) {
       let pointsProfileView = view.viewWithTag(POINTS_PROFILE_VIEW_TAG)
       pointsProfileView?.removeFromSuperview()
       
       UIView.animate(withDuration: 1, animations: {
           self.carouselView.reloadItem(at: 0, animated: true)
       })
   }
}
