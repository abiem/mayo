//
//  Colors.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 06/05/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

// Factory for retrieving colours from the Assets file, which
// falls back on hard-coded values before iOS11 (this app supports iOS10)

import Foundation
import UIKit

class Color {

    // Asset file colors
    let accentGreen: UIColor
    let textGreen: UIColor

    static let hardcodedAccentGreen = UIColor(red: 0.31, green: 0.78, blue: 0.44, alpha: 1.00)
    static let hardcodedTextGreen = UIColor(red: 0.57, green: 0.81, blue: 0.35, alpha: 1.00)

    // UI element colours
    let cardBackgroundColor = UIColor.white
    
    /// UIColor.black
    let cardTextColor = UIColor.black

    let alertBackgroundColor = UIColor.white
    let alertTextColor = UIColor.black
    
    let buttonTextColor = UIColor.white

    // must be computed property when we use a color set up in init
    var buttonBackgroundColor: UIColor {
        get { accentGreen }
    }

    static let shared = Color()
    
    init() {
if #available(iOS 11.0, *) {
        accentGreen = UIColor(named: "accentGreen") ?? Color.hardcodedAccentGreen
        textGreen = UIColor(named: "textGreen") ?? Color.hardcodedTextGreen
    } else {
        accentGreen = UIColor(red: 0.31, green: 0.78, blue: 0.44, alpha: 1.00)
        textGreen = UIColor(red: 0.57, green: 0.81, blue: 0.35, alpha: 1.00)
    }
    }
}
