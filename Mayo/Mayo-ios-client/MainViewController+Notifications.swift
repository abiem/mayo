//
//  MainViewController+Notifications.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

extension MainViewController {
    func sendPushNotificationToNearbyUsers() {
        if let currentUserTask = self.tasks[0] {
            for userId in nearbyUsers {
                usersRef?.child(userId).observeSingleEvent(of: .value, with: { snapshot in

                    // get the user dictionary
                    let value = snapshot.value as? NSDictionary

                    // get the user's current deviceToken
                    let deviceToken = value?["deviceToken"] as? String

                    // send the user a notification to nearby users.
                    if deviceToken != nil, userId != self.currentUserId {
                        PushNotificationManager.sendNearbyTaskNotification(deviceToken: deviceToken!, taskID: currentUserTask.taskID!)
                    }

                }, withCancel: { error in
                    print(error.localizedDescription)
                })
            }
        }
    }

    // create custom notification
    func createLocalNotification(title: String, body: String?, time: Int) {
        // create content
        let content = UNMutableNotificationContent()
        content.title = title
        if let contentBody = body {
            content.body = contentBody
        }
        var Interval = 0.5
        if title == "Your post is expiring soon" {
            Interval = Double(time)
        } else {
            removeCurrentUserTaskAnnotation()
        }
        content.sound = UNNotificationSound.default

        // create trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: Interval, repeats: false)
        let request = UNNotificationRequest(identifier: "taskExpirationNotification", content: content, trigger: trigger)

        // schedule the notification
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: { error in
            print(error ?? "Error")
        })
    }
    
    func checkNotificationPermission() {
        checkStatusOfNotification { status in
            if status == .notDetermined {
                self.mShowNotification = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    if UIApplication.shared.applicationIconBadgeNumber == 1 {
                        self?.showNotificationAlert(prompt: "Hello again! Want to be notified of new posts around you in the future?")
                    }
                }
            } else if UserDefaults.standard.bool(forKey: "showNotificationRequestForTesting") {
                self.mShowNotification = true
                
            } else {
                self.mShowNotification = false
            }
        }
    }
    
    // Show Notification Alert
    func showNotificationAlert(prompt: String) {
        if mShowNotification {
            CMAlertController.sharedInstance.showAlert(subtitle: NSLocalizedString(prompt, comment: ""),
                                                       buttons: ["Not now", "Sure"]) { sender in
                if let button = sender {
                    if button.tag == 1 {
                        self.mShowNotification = false
                        UIApplication.shared.applicationIconBadgeNumber = 0
                        requestForNotification()
                    }
                }
            }
        }
    }
}
