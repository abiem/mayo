//
//  UserLocation.swift
//  Mayo-ios-client
//
//  Created by Eric Wolak on 5/27/19.
//  Copyright © 2019 Factor13 LLC. All rights reserved.
//

import CoreLocation
import Foundation

import Firebase
import GeoFire
import os

protocol UserLocationManagerDelegate: class {
	func updateLocation(_ manager: CLLocationManager, location: CLLocation)
	func updateLocationFailed(_ manager: CLLocationManager, didFailWithError error: Error)
	func exitsFromRegion(_ manager: CLLocationManager, didExitRegion region: CLRegion)
}

class UserLocation: NSObject, CLLocationManagerDelegate {
	static let shared = UserLocation()
	weak var delegate: UserLocationManagerDelegate?
	private var currentLocation: CLLocation?
	
	private let locationManager = CLLocationManager()
	private var authorizationCallback: ((_ status: CLAuthorizationStatus) -> Void)?
	
	var backgroundTask: UIBackgroundTaskIdentifier = .invalid
	
	override init() {
		authorizationCallback = { (_: CLAuthorizationStatus) -> Void in return}
		super.init()
		self.printLogs(msg: "UserLocation.swift -> init")
		locationManager.delegate = self
		locationManager.activityType = .automotiveNavigation
		locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
		locationManager.distanceFilter = Constants.minMetersBetweenLocations
		locationManager.allowsBackgroundLocationUpdates = true
		locationManager.pausesLocationUpdatesAutomatically = false
		if #available(iOS 11.0, *) {
			locationManager.showsBackgroundLocationIndicator = false
		}
	}
	
	public func requestAlwaysAuthorization(callback: @escaping (_ status:
		CLAuthorizationStatus) -> Void) {
		self.printLogs(msg: "UserLocation.swift -> requestAlwaysAuthorization callback")
		if !CLLocationManager.locationServicesEnabled() {
            self.printLogs(msg: "Location Service unabled")
			DispatchQueue.main.async {
				callback(CLAuthorizationStatus.notDetermined)
			}
			return
		}
        
        let locAuthStatus = CLLocationManager.authorizationStatus()
        print("loc aut status : \(locAuthStatus)")
        // If location is not set to "Always Allow"
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            self.printLogs(msg: "Location Service enabled not always authorized \(CLLocationManager.authorizationStatus())")
			authorizationCallback = callback
            // FIXME: sometimes doesn't ask users for authoratization??
			locationManager.requestWhenInUseAuthorization()
        } else {
            self.printLogs(msg: "Location service enabled always authorized")
            DispatchQueue.main.async {
                callback(CLAuthorizationStatus.authorizedWhenInUse)
            }
            return
        }
	}
	
	public func shouldBeAuthorized() -> Bool {
		self.printLogs(msg: "UserLocation.swift -> shouldBeAuthorized")
		if CLLocationManager.locationServicesEnabled() {
			let status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
			if status == .authorizedAlways || status == .authorizedWhenInUse {
				return true
			} else {
				showLocationAlert()
				return false
			}
		} else {
			return false
		}
	}
	
	public func backgroundUpdate() {
		self.printLogs(msg: "UserLocation.swift -> backgroundUpdate")
		locationManager.stopUpdatingLocation()
		locationManager.startMonitoringSignificantLocationChanges()
	}
	
	public func foregroundUpdate() {
		self.printLogs(msg: "UserLocation.swift -> foregroundUpdate")
		locationManager.stopMonitoringSignificantLocationChanges()
		locationManager.startUpdatingLocation()
	}
	
	public func stopUpdateLocations(){
		locationManager.stopUpdatingLocation()
		locationManager.stopMonitoringSignificantLocationChanges()
	}
	
	// MARK: CLLocationManagerDelegate
	
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		self.printLogs(msg: "UserLocation.swift -> Location authorization changed to \(String(describing: status))")
		switch status {
		case .authorizedAlways:
			Analytics.logEvent(Constants.EVENT_AUTHORIZATION, parameters: [
				"permission": "location_always",
				])
			locationManager.startMonitoringSignificantLocationChanges()
		case CLAuthorizationStatus.authorizedWhenInUse:
			Analytics.logEvent(Constants.EVENT_AUTHORIZATION, parameters: [
				"permission": "location_when_in_use",
				])
			locationManager.startMonitoringSignificantLocationChanges()
		default:
			break
		}
		
		authorizationCallback?(status)
		authorizationCallback = nil
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		self.printLogs(msg: "UserLocation.swift -> Location did update")
		currentLocation = locations.last!

		var lastLocationsArr: [String] = [String]()
		if UserDefaults.standard.object(forKey: "last_locations") != nil{
			lastLocationsArr = UserDefaults.standard.object(forKey: "last_locations") as! [String]
		}
		lastLocationsArr.append("\(currentLocation?.coordinate.latitude ?? 0),\(currentLocation?.coordinate.longitude ?? 0)")
		UserDefaults.standard.set(lastLocationsArr, forKey: "last_locations")

		var lastLocationsTimeArr: [String] = [String]()
		if UserDefaults.standard.object(forKey: "last_locations_time") != nil{
			lastLocationsTimeArr = UserDefaults.standard.object(forKey: "last_locations_time") as! [String]
		}
		lastLocationsTimeArr.append("\(Date())")
		UserDefaults.standard.set(lastLocationsTimeArr, forKey: "last_locations_time")

		let archived = NSKeyedArchiver.archivedData(withRootObject: locations.last!)
		UserDefaults.standard.setValue(archived, forKey: Constants.LOCATION)

		if(UIApplication.shared.applicationState == .background){
			backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "Finish Location Update", expirationHandler: {() -> Void in
				self.endBackgroundTask()
			})
			assert(backgroundTask != .invalid)

			let usersGeoFire = GeoFire(firebaseRef: Database.database().reference().child("users_locations"))
			if let loc = locations.last, let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
				usersGeoFire.setLocation(loc, forKey: userId)
			}
			
			self.delegate?.updateLocation(manager, location: self.currentLocation!)
		}
		else{
			let usersGeoFire = GeoFire(firebaseRef: Database.database().reference().child("users_locations"))
			if let loc = locations.last, let userId = UserDefaults.standard.string(forKey: Constants.CURRENT_USER_ID) {
				usersGeoFire.setLocation(loc, forKey: userId)
			}
			delegate?.updateLocation(manager, location: currentLocation!)
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		self.printLogs(msg: "UserLocation.swift -> Location did fail with error: \(String(describing: error))")
		authorizationCallback?(.notDetermined)
		authorizationCallback = nil
		
		if(UIApplication.shared.applicationState == .background){
			backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "Location did failed", expirationHandler: {() -> Void in
				self.endBackgroundTask()
			})
			assert(backgroundTask != .invalid)

			self.delegate?.updateLocationFailed(manager, didFailWithError: error)
		}
		else{
			delegate?.updateLocationFailed(manager, didFailWithError: error)
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
		if(UIApplication.shared.applicationState == .background){
			backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "Exits From Region", expirationHandler: {() -> Void in
				self.endBackgroundTask()
			})
			assert(backgroundTask != .invalid)
			
			self.delegate?.exitsFromRegion(manager, didExitRegion: region)
		}
		else{
			delegate?.exitsFromRegion(manager, didExitRegion: region)
		}
	}
	
	public func endBackgroundTask()
	{
		if(self.backgroundTask != .invalid) {
			UIApplication.shared.endBackgroundTask(self.backgroundTask)
			self.backgroundTask = .invalid
		}
	}
	
	// MARK: private stuff
	private func showLocationAlert() {
		self.printLogs(msg: "UserLocation.swift -> showLocationAlert")
        CMAlertController.sharedInstance.showAlert(subtitle: Constants.sLOCATION_ERROR,
                                                   buttons: ["Not now", "Settings"]) { sender in
			if let button = sender {
				if button.tag == 1 {
					// for Move to Settings
					UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
				}
			}
		}
	}
	
	private func printLogs(msg: String){
		let customLog = OSLog(subsystem: "com.mayo", category: "mayo_app")
		os_log("%@", log: customLog, type: .error, msg)
	}
	
	public func startMonitoring(geofenceRegion: CLRegion){
		self.locationManager.startMonitoring(for: geofenceRegion)
	}

	public func stopMonitoring(geofenceRegion: CLRegion){
		self.locationManager.stopMonitoring(for: geofenceRegion)
	}

	public func removeAllObservingRegions() {
		for region in locationManager.monitoredRegions {
			locationManager.stopMonitoring(for: region)
		}
	}

	public func getLocation() -> CLLocation? {
		if(currentLocation == nil)
		{
			let defaults = UserDefaults.standard
			guard let archived = defaults.object(forKey: Constants.LOCATION) as? Data,
				let location = NSKeyedUnarchiver.unarchiveObject(with: archived) as? CLLocation else {
					return CLLocation(latitude: Constants.DEFAULT_LAT, longitude: Constants.DEFAULT_LNG)
			}
			currentLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
		}
		return currentLocation ?? CLLocation(latitude: Constants.DEFAULT_LAT, longitude: Constants.DEFAULT_LNG)
	}
}
