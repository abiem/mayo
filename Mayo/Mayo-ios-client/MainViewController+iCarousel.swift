//
//  MainViewController+iCarousel.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import iCarousel
import CoreLocation
import Firebase
import SwiftMoment
import SafariServices

//MARK: - iCarouselDelegate
extension MainViewController: iCarouselDelegate {
    // change the center of the map based on the currently selected task
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        // test for bugs for map annotations
        testMapAnnotations()

        // use the last item index (it gets updated at the end of the method)
        // and check if the last card is an onboarding task
        let defaults = UserDefaults.standard

        let when = DispatchTime.now() + 0.2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            if self.tasks.count <= 0 { return }
            
            // swiped onto the second onboarding task
            if let currentTask: Task = self.tasks[self.carouselView.currentItemIndex] {
                if currentTask.taskDescription == self.ONBOARDING_TASK_2_DESCRIPTION {
                    // so ask for notification permission
                    if !OncePerLaunch.shared.hasHappendThisLaunch(flag: "notification: third card") {
                        self.showNotificationAlert(prompt: "notification: third card")
                    }
                }
            }
            
            // first post live dismiss case
            if let safeLastCardIndex = self.lastCardIndex, safeLastCardIndex >= self.tasks.count {
                return
            }
            
            // if the index actually changed, and the user hasn't viewed onboarding task #1
            // FIXME: app crashes when dismissing FIRST POST LIVE after dismiss is clicked
            if let lastCardIndex = self.lastCardIndex, lastCardIndex != carousel.currentItemIndex {
                if let swipedTask: Task = self.tasks[self.lastCardIndex!] {
                    if swipedTask.taskDescription == Constants.SAMPLE_POST.SWIPE_LEFT.desc, self.carouselView.currentItemIndex == 0 ||
                        self.carouselView.currentItemIndex == 2 ||
                        self.carouselView.currentItemIndex == 3 {
                        // user swiped away from the *first* onboarding task
                        // so remove it
                        self.checkAndRemoveOnboardingTasks(carousel: carousel, cardIndex: self.lastCardIndex!)
                        // and switch the current index of the carousel since we just killed a task
                        self.carouselView.scrollToItem(at: 0, animated: true)
                        // no need to do anything else in here. Our job is done.
                        
                        // set view key to true
                        defaults.set(true, forKey: Constants.SAMPLE_POST.SWIPE_LEFT.viewed)
                        return
                    }
                }
            }
            
            // if card swiped was history bookmark
            if let lastCardIndex = self.lastCardIndex, lastCardIndex != carousel.currentItemIndex {
                // FIXME: gives index out of range error when history bookmark gets dismissed
                if self.lastCardIndex! < self.tasks.count, let swipedTask: Task = self.tasks[self.lastCardIndex!] {
                    if swipedTask.taskDescription == Constants.SAMPLE_POST.HISTORY_TITLE.desc {
                        defaults.set(true, forKey: Constants.SAMPLE_POST.HISTORY_TITLE.viewed)
                        self.checkAndRemoveOnboardingTasks(carousel: carousel, cardIndex: self.lastCardIndex!)
                        self.carouselView.scrollToItem(at: self.lastCardIndex!, animated: false)
                    }
                }
            }
            
            
            
            // if we swipe onto a completed task, remove it from the map view
            if self.carouselView.currentItemIndex > 0, self.carouselView.currentItemIndex < self.tasks.count {
                if let currentTask = self.tasks[self.carouselView.currentItemIndex] {
                    if currentTask.completed == true {
                        let taskCordinates = CLLocationCoordinate2D(latitude: currentTask.latitude, longitude: currentTask.longitude)
                        self.expiredAnnotation.coordinate = taskCordinates
                        self.mapView.addAnnotation(self.expiredAnnotation)

                        self.mapView.selectAnnotation(self.expiredAnnotation, animated: false)

                    } else {
                        self.mapView.removeAnnotation(self.expiredAnnotation)
                    }
                }
            }

            // now that we messed with tasks, sync everything up
            self.updateMapAnnotationCardIndexes()

            let annotations = self.mapView.annotations
            let currentIndex = self.carouselView.currentItemIndex
            if currentIndex >= 0, currentIndex < self.tasks.count {
                let currentTask = self.tasks[currentIndex]
                // move the map to center on the newly-selected task
                if let task = self.tasks[currentIndex] {
                    if task.taskDescription != "" {
                        self.updateViewsCount(task)
                        //Changing view count label to the correct number
                        //Check if current user is the one who made the task
                        if(task.userId == self.currentUserId){
                            //get the number of views for the current task, and update the label with the correct number
                            self.getCurrentTaskViewCount(taskID: task.taskID!, completion: { count in
                            
                                if let viewCountLabel = self.view.viewWithTag(self.VIEW_COUNT_TAG) as? UILabel{
                                    if(count == 1){
                                        viewCountLabel.text = "0"
                                    }
                                    else{
                                        print("Count: \(count)")
                                        viewCountLabel.text = "\(count-1)"
                                    }
                                }
                            })
                        }
                    }
                    let taskLat = task.latitude
                    let taskLong = task.longitude
                    let taskCoordinate = CLLocationCoordinate2D(latitude: taskLat, longitude: taskLong)
                    self.mapView.setCenter(taskCoordinate, animated: true)
                    print("map center changed to lat:\(task.latitude) long:\(task.longitude)")
                }
                for annotation in annotations {
                    // check if the annotation is a custom current user task annotation
                    if annotation is CustomCurrentUserTaskAnnotation {
                        // remove and add it back on

                        // reload annotation
                        let annotationClone = annotation
                        self.mapView.removeAnnotation(annotation)
                        self.mapView.addAnnotation(annotationClone)
                        if self.carouselView.currentItemIndex == 0 {
                            self.mapView.selectAnnotation(annotationClone, animated: false)
                        }
                    }

                    if annotation is CustomFocusTaskMapAnnotation {
                        let customFocusTaskAnnotation = annotation as! CustomFocusTaskMapAnnotation
                        // get the current index
                        let index = self.carouselView.currentItemIndex

                        // get the user id from the annotation
                        let taskUserId = (customFocusTaskAnnotation.taskUserId != nil) ? customFocusTaskAnnotation.taskUserId! : ""

                        // add regular task icon
                        let taskAnnoation = CustomTaskMapAnnotation(currentCarouselIndex: index, taskUserId: taskUserId)
                        taskAnnoation.coordinate = customFocusTaskAnnotation.coordinate
                        //
                        //                    // remove focus task icon
                        taskAnnoation.style = .color(#colorLiteral(red: 0, green: 0.5901804566, blue: 0.758269012, alpha: 1), radius: 30)
                        self.mapView.removeAnnotation(customFocusTaskAnnotation)
//          self.clusterManager.remove(customFocusTaskAnnotation)
//          self.clusterManager.add(taskAnnoation)
                        self.mapView.addAnnotation(taskAnnoation)
                    }

                    // check for the annotation for current card
                    if annotation is CustomTaskMapAnnotation {
                        let mapTaskAnnotation = annotation as! CustomTaskMapAnnotation
                        if mapTaskAnnotation.taskUserId == currentTask?.taskID {
                            // once the right annotation is found

                            // add the annotation with a different class
                            // create new focus annotation class for the current map icon
                            let index = self.carouselView.currentItemIndex

                            // get user id for the task
                            let taskUserId = (mapTaskAnnotation.taskUserId != nil) ? mapTaskAnnotation.taskUserId! : ""

                            let focusAnnotation = CustomFocusTaskMapAnnotation(currentCarouselIndex: index, taskUserId: taskUserId)
                            focusAnnotation.coordinate = mapTaskAnnotation.coordinate
                            focusAnnotation.style = .color(#colorLiteral(red: 0, green: 0.5901804566, blue: 0.758269012, alpha: 1), radius: 30) // .image(#imageLiteral(resourceName: "newNotificaitonIcon"))
                            self.clusterManager.remove(mapTaskAnnotation)
                            self.mapView.addAnnotation(focusAnnotation)
                            self.mapView.removeAnnotation(mapTaskAnnotation)
//            self.clusterManager.add(focusAnnotation)
                            self.mapView.selectAnnotation(focusAnnotation, animated: false)
                        }
                    }
                }
                self.clusterManager.reload(mapView: self.mapView)
            }

            // update the last carousel card index
            self.lastCardIndex = carousel.currentItemIndex
        }

        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterContentType: "task",
        ])
    }

    func carousel(_: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.spacing {
            return value * 1.03
        }

        return value
    }

    func carouselScroll(_ pTask: String) {
        for (index, task) in tasks.enumerated() {
            if pTask == task?.taskID {
                let when = DispatchTime.now() + 0.2 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) { [weak self] in
                    self?.carouselView.scrollToItem(at: index, animated: false)
                }
            }
        }
    }
}

//MARK: - iCarouselDataSource
extension MainViewController: iCarouselDataSource {
    func numberOfItems(in _: iCarousel) -> Int {
        return tasks.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing _: UIView?) -> UIView {
        print("index hit: \(index)")
        let viewWidth = UIScreen.main.bounds.size.height <= 568 ? 286 : 335 // 335
        
        // MARK: When Loading
        if index == 0 && isLoadingFirebase == true {
            let tempView = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: Int(carousel.frame.size.height - 50)))
            tempView.backgroundColor = UIColor.white
            tempView.tag = LOADER_VIEW
            tempView.layer.shadowColor = UIColor.black.cgColor
            tempView.layer.shadowOffset = CGSize(width: 0, height: 10) // Here you control x and y
            tempView.layer.shadowOpacity = 0.3
            tempView.layer.shadowRadius = 15.0 // Here your control your blur
            tempView.layer.masksToBounds = false
            tempView.alpha = 0.3
            
            // add temp view to shadow view
            let shadowView = createShadowView(carousel: carousel)
            shadowView.addSubview(tempView)

            return shadowView
        }

        // MARK: - User's card
        // if there is only 1 card and no surrounding cards
        // show the first card

        // 1st task if user swiped for new task or there is no other tasks
        if (index == 0) || tasks.count <= 1 { // setup temporary view as gradient view
            // get the first task
            let task = tasks[index]!
            
            // carousel.frame.size.height-15
            let tempView = GradientView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: Int(carousel.frame.size.height - 25)))
            let cardColor = CardColor()
            
            // Subviews
            
            /// postType segmented control
            let postType = createPostTypeSegmentedControl()
            
            /// postType label
            let postTypeLabel = createPostTypeLabel(task)
            
            /// textView that the user use to edit and see their task
            let textView = createUserTextView()
            
            /// noticeLabel that says "Expires in 1hr or if you leave the area"
            let noticeLabel = createNoticeLabel(task)
            
            /// close button ('x' button on bottom left of card)
            let closeView = createCloseButton(container: tempView)
            
            /// post button ('Post' button on bottom right of card)
            let postButton = createPostButton(container: tempView)
            
            /// reply button on the middle of the card when task is saved
            let replyButton = createUserReplyButton()
            
            /// done button to finish saved task
            let doneButton = createDoneButton(container: tempView)
            
            /// view count icon and label to indicate how many people have seen this post
            let (viewCountIcon, viewCountLabel) = createViewCount(task: task)
            

            
            // if it already has start and end color, use it start and end color for the tempview
            if let taskStartColor = task.startColor, let taskEndColor = task.endColor {
                tempView.startColor = UIColor.hexStringToUIColor(hex: taskStartColor)
                tempView.endColor = UIColor.hexStringToUIColor(hex: taskEndColor)
                
                // if the task doesn't have a start and end color yet
                // use the start and end color to save it
            } else {
                let randomColorGradient = cardColor.generateRandomColor()

                // save task random colors to task
                let randomStartColor = randomColorGradient[0]
                let randomEndColor = randomColorGradient[1]

                task.startColor = randomStartColor
                task.endColor = randomEndColor

                tasks[index] = task

                // use task's start and end color for the view
                tempView.startColor = UIColor.hexStringToUIColor(hex: randomStartColor)
                tempView.endColor = UIColor.hexStringToUIColor(hex: randomEndColor)
            }
            

            
            tempView.addSubview(textView)
            tempView.addSubview(noticeLabel)
             
            // MARK: When user's task is posted
            if currentUserTaskSaved {
                // Add subviews
                tempView.addSubview(postTypeLabel)
                tempView.addSubview(replyButton)
                tempView.addSubview(doneButton)
                
                if(task.userId == currentUserId) {
                    tempView.addSubview(viewCountIcon)
                    tempView.addSubview(viewCountLabel)
                    
                    // viewCount constraints
                    NSLayoutConstraint.activate([
                        // viewCountIcon constraints
                        viewCountIcon.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 17),
                        viewCountIcon.topAnchor.constraint(equalTo: tempView.topAnchor, constant: tempView.bounds.height * 3 / 4 + doneButton.frame.height / 4),
                        viewCountIcon.widthAnchor.constraint(equalToConstant: 17),
                        viewCountIcon.heightAnchor.constraint(equalToConstant: 11),
                        
                        // viewCountLabel constraints
                        viewCountLabel.leadingAnchor.constraint(equalTo: viewCountIcon.trailingAnchor, constant: 4),
                        viewCountLabel.topAnchor.constraint(equalTo: viewCountIcon.topAnchor, constant: -2),
                        viewCountIcon.widthAnchor.constraint(equalToConstant: 17),
                        viewCountIcon.heightAnchor.constraint(equalToConstant: 11),
                        
                    ])
                    
                }
                
                // adjust textView
                textView.alpha = 1
                textView.text = task.taskDescription
                textView.isEditable = false
                
                // Contraints
                NSLayoutConstraint.activate([
                    // replyButton constraints
                    replyButton.centerXAnchor.constraint(equalTo: tempView.centerXAnchor, constant: -10),
                    replyButton.topAnchor.constraint(equalTo: tempView.topAnchor, constant: tempView.bounds.height * 3 / 4),
                    replyButton.widthAnchor.constraint(equalToConstant: 110),
                    replyButton.heightAnchor.constraint(equalToConstant: 24),
                    
                    // postTypeLabel Constraints
                    postTypeLabel.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                    postTypeLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
                    
                    // noticeLabel Constraints
                    noticeLabel.leadingAnchor.constraint(equalTo: postTypeLabel.trailingAnchor, constant: 10),
                    noticeLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
                    
                    // textView Constraints
                    textView.widthAnchor.constraint(equalToConstant: tempView.bounds.width * 0.9),
                    textView.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 17),
                    textView.topAnchor.constraint(equalTo: postTypeLabel.bottomAnchor, constant: 0),
                ])
                
            // MARK: When user's editing task
            } else { // when editing
                // Add subviews
                tempView.addSubview(postType)
                tempView.addSubview(closeView)
                tempView.addSubview(postButton)

                // adjusting textView
                textView.isEditable = true
                // show placeholder
                if let prevDescription = mTaskDescription {
                    textView.text = prevDescription
                    textView.alpha = 1
                } else {
                    let defaults = UserDefaults.standard
                    if defaults.bool(forKey: Constants.COMPOSE_LONG_PLACEHOLDER_VIEWED) {
                        textView.text = Constants.COMPOSE_SHORT_PLACEHOLDER
                    } else  {
                        textView.text = Constants.COMPOSE_LONG_PLACEHOLDER
                    }
                    
                    textView.alpha = 0.5
                }
                //textView.becomeFirstResponder()
                
                // Adding constraints
                NSLayoutConstraint.activate([
                    // postTypeSC Constraints
                    postType.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                    postType.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 10),
                    
                    // textView Constraints
                    textView.widthAnchor.constraint(equalToConstant: tempView.bounds.width * 0.9),
                    textView.heightAnchor.constraint(equalToConstant:(carousel.frame.size.height - 50) * 3 / 4),
                    textView.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                    textView.topAnchor.constraint(equalTo: postType.bottomAnchor, constant: 0),
                    
                    // Notice Label Constraints
                    noticeLabel.leadingAnchor.constraint(equalTo: postType.trailingAnchor, constant: 10),
                    noticeLabel.trailingAnchor.constraint(equalTo: tempView.trailingAnchor, constant: 10),
                    noticeLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 10),
                    noticeLabel.widthAnchor.constraint(equalToConstant: 150)
                ])
            }
            
            // Mark: ShadowView
            // add temp view to shadow view
            let shadowView = createShadowView(carousel: carousel)
            shadowView.addSubview(tempView)

            return shadowView
        }

        //MARK: - Other posts' cards
        // if index out of bound
        // Done tasks
        if index >= tasks.count {
            // create invisible card
            print("clear card created")
            let tempView = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: Int(carousel.frame.size.height - 25)))
            tempView.backgroundColor = UIColor.clear
            tempView.layer.masksToBounds = false
            return tempView
        } else {
            // STANDARD VIEW - for most card

            // get the corresponding task
            let task = tasks[index]!
            
           
            

            // setup temporary view as gradient view
            let shadowView = createShadowView(carousel: carousel)
            let tempView = GradientView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: Int(carousel.frame.size.height - 25)))
            let cardColor = CardColor()
            
            let postTypeLabel = createPostTypeLabel(task)
            let taskLabel = createTaskLabel(task: task, index: index)
            let messageButton = createPostReplyButton(task: task)
            let timerLabel = createTimerLabel(task: task)
            let (viewCountIcon, viewCountLabel) = createViewCount(task: task)
            let dismissButton = createDismissButton()
            
            
            
            // TODO: delete this if reporting feature not done
            /// more option for each card button
//            let moreButton = createMoreButton(task)


            // if task doesn't have a  start color and end color
            // create random colors for it
            if (task.startColor == nil) || (task.endColor == nil) {
                // get random star and end colors
                let randomColorGradient = cardColor.generateRandomColor()

                // save the colors to the task
                task.setGradientColors(startColor: randomColorGradient[0], endColor: randomColorGradient[1])

                // set the color gradient colors for the card
                tempView.startColor = UIColor.hexStringToUIColor(hex: randomColorGradient[0])
                tempView.endColor = UIColor.hexStringToUIColor(hex: randomColorGradient[1])
            } else {
                // use the colors that are already saved for the task
                if task.completed {
                    tempView.startColor = UIColor.hexStringToUIColor(hex: CardColor.expireCard[0]).withAlphaComponent(0.6)
                    tempView.endColor = UIColor.hexStringToUIColor(hex: CardColor.expireCard[1]).withAlphaComponent(0.6)
                } else {
                    tempView.startColor = UIColor.hexStringToUIColor(hex: task.startColor!)
                    tempView.endColor = UIColor.hexStringToUIColor(hex: task.endColor!)
                }
            }
            
            //Adding view count icon and count label to tasks that were not *just* made
            // only adding the label to tasks that the current user has posted
            // Adding unique views count for done tasks
            
            // invisible button
            // setup clickable button for gradient view
            let fullButton = UIButton(frame: CGRect(x: 0, y: 0, width: viewWidth, height: Int(carousel.frame.size.height - 25)))
            fullButton.addTarget(self, action: #selector(goToChat(sender:)), for: .touchUpInside)
            
            tempView.addSubview(postTypeLabel)
            tempView.addSubview(timerLabel)
            tempView.addSubview(taskLabel)
            
            
            // MARK: Sample post
            if task.postType == "Sample Post" {
                NSLayoutConstraint.activate([
                    // postTypeLabel constraints
                    postTypeLabel.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                    postTypeLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
                    
                    // timerLabel constraints
                    timerLabel.leadingAnchor.constraint(equalTo: postTypeLabel.trailingAnchor, constant: 10),
                    timerLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
                    
                    // taskLabel constraints
                    taskLabel.widthAnchor.constraint(equalToConstant: tempView.bounds.width * 0.9),
                    taskLabel.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                    taskLabel.topAnchor.constraint(equalTo: postTypeLabel.bottomAnchor, constant: 10),
                ])
                
                if task.taskDescription == Constants.SAMPLE_POST.EXAMPLE_TASK.desc || task.taskDescription == Constants.SAMPLE_POST.FIRST_POST_LIVE.desc{
                    tempView.addSubview(messageButton)
                    NSLayoutConstraint.activate([
                        // messageButton constraints
                        messageButton.centerXAnchor.constraint(equalTo: tempView.centerXAnchor, constant: 0),
                        messageButton.topAnchor.constraint(equalTo: tempView.topAnchor, constant:  (carousel.frame.size.height - 25) * 3 / 4),
                        messageButton.widthAnchor.constraint(equalToConstant: 150),
                        messageButton.heightAnchor.constraint(equalToConstant: 20),
                    ])
                }
                
                if task.taskDescription == Constants.SAMPLE_POST.FIRST_POST_LIVE.desc {
                    tempView.addSubview(dismissButton)
                    
                    NSLayoutConstraint.activate([
                        dismissButton.trailingAnchor.constraint(equalTo: tempView.trailingAnchor, constant: -20),
                        dismissButton.topAnchor.constraint(equalTo: tempView.topAnchor, constant:  (carousel.frame.size.height - 25) * 3 / 4),
                        dismissButton.centerYAnchor.constraint(equalTo: messageButton.centerYAnchor)
                    ])
                }
                shadowView.addSubview(tempView)
                return shadowView
                
            }
            
            
            // Delete / comment this if reporting feature not done
//            tempView.addSubview(moreButton)
            
            tempView.addSubview(fullButton)

            if (task.userId == currentUserId) {
                tempView.addSubview(viewCountIcon)
                tempView.addSubview(viewCountLabel)
                
                NSLayoutConstraint.activate([
                    // viewCountIcon constraints
                    viewCountIcon.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 17),
                    viewCountIcon.topAnchor.constraint(equalTo: tempView.topAnchor, constant: (carousel.frame.size.height - 25) * 3 / 4),
                    viewCountIcon.widthAnchor.constraint(equalToConstant: 17),
                    viewCountIcon.heightAnchor.constraint(equalToConstant: 11),
                    
                    // viewCountLabel constraints
                    viewCountLabel.leadingAnchor.constraint(equalTo: viewCountIcon.trailingAnchor, constant: 4),
                    viewCountLabel.topAnchor.constraint(equalTo: viewCountIcon.topAnchor, constant: -2),
                    viewCountIcon.widthAnchor.constraint(equalToConstant: 17),
                    viewCountIcon.heightAnchor.constraint(equalToConstant: 11),
                ])
            } else {
                let studentBadgeImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
                
                if let isStudent = task.isCreatorStudent {
                    if isStudent {
                        studentBadgeImageView.image = UIImage(named: "iconBadgeEdu")
                        studentBadgeImageView.center.x = tempView.frame.maxX - 40
                        studentBadgeImageView.center.y = messageButton.center.y
                    }
                }
                
                tempView.addSubview(studentBadgeImageView)
            }
            tempView.addSubview(messageButton)
            // Constraints
            NSLayoutConstraint.activate([
                // postTypeLabel constraints
                postTypeLabel.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                postTypeLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
                
                // timerLabel constraints
                timerLabel.leadingAnchor.constraint(equalTo: postTypeLabel.trailingAnchor, constant: 10),
                timerLabel.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
                
                // taskLabel constraints
                taskLabel.widthAnchor.constraint(equalToConstant: tempView.bounds.width * 0.9),
                taskLabel.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                taskLabel.topAnchor.constraint(equalTo: postTypeLabel.bottomAnchor, constant: 10),
                
                // messageButton constraints
                messageButton.centerXAnchor.constraint(equalTo: tempView.centerXAnchor, constant: 0),
                messageButton.topAnchor.constraint(equalTo: tempView.topAnchor, constant:  (carousel.frame.size.height - 25) * 3 / 4),
                messageButton.widthAnchor.constraint(equalToConstant: 150),
                messageButton.heightAnchor.constraint(equalToConstant: 20),
                
                // comment if reporting feature not done
                // moreButton constraints
//                moreButton.trailingAnchor.constraint(equalTo: tempView.trailingAnchor, constant: -20),
//                moreButton.topAnchor.constraint(equalTo: tempView.topAnchor, constant: 20),
//                moreButton.heightAnchor.constraint(equalToConstant: 10),
//                moreButton.widthAnchor.constraint(equalToConstant: 30)
            ])
            
            
            let postType = task.postType!
            // if postType is from earlier versions
            if postType != "Asking" && postType != "Giving" && postType != "Sample Post" {
                
                postTypeLabel.removeFromSuperview()
                
                NSLayoutConstraint.activate([
                    timerLabel.leadingAnchor.constraint(equalTo: tempView.leadingAnchor, constant: 20),
                
                    taskLabel.topAnchor.constraint(equalTo: timerLabel.bottomAnchor, constant: 10),
                ])
            }
            
            

            shadowView.addSubview(tempView)

            return shadowView
        }
    }
    
    //MARK: - Create User's post Subviews
    
    /// Create post type segmented control
    /// Either Offering or Asking
    func createPostTypeSegmentedControl() -> UISegmentedControl {
        let items = ["Asking", "Giving"]
        let postTypeSC = UISegmentedControl(items: items)
        postTypeSC.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        postTypeSC.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        postTypeSC.selectedSegmentIndex = 0
        postTypeSC.setEnabled(true, forSegmentAt: 0)
        postTypeSC.setEnabled(true, forSegmentAt: 1)
        postTypeSC.apportionsSegmentWidthsByContent = true
        postTypeSC.translatesAutoresizingMaskIntoConstraints = false
        postTypeSC.tag = POST_TYPE_TAG
        
        if #available(iOS 13.0, *) {
            postTypeSC.selectedSegmentTintColor = .white
        } else {
            postTypeSC.tintColor = .white
        }
        
        
        
        postTypeSC.addTarget(self, action: #selector(postTypeDidChange(_:)), for: .valueChanged)
        
        return postTypeSC
    }
    
    /// ignore just for testing
    @objc func postTypeDidChange(_ segmentedControl: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            print("Asking")
        default:
            print("Offering")
        }
    }
    
    /// createPostTypeLabel for saved tasks
    func createPostTypeLabel(_ task: Task) -> UILabel {
        let postTypeLabel = UILabel()
        postTypeLabel.text = task.postType
        postTypeLabel.textColor = .white
        postTypeLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 13)
        postTypeLabel.numberOfLines = 1
        postTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return postTypeLabel
    }
    
    /// Create notice label that says "60 mins left or if you leave the area"
    func createNoticeLabel(_ task: Task) -> UILabel {
        let noticeLabel = UILabel()
        noticeLabel.textColor = UIColor.white
        
        noticeLabel.font = UIFont(name: Constants.FONT_NAME, size: 13)
        noticeLabel.numberOfLines = 0
        noticeLabel.adjustsFontSizeToFitWidth = true
        noticeLabel.translatesAutoresizingMaskIntoConstraints = false
        noticeLabel.minimumScaleFactor = 0.5
        
        var timeLeftNotice = "Expires in 1 hr or if you leave the area"
        
        if currentUserTaskSaved {
            let now = moment(Date())
            let oneHr = moment(task.timeUpdated + 3600)
            let dur = oneHr.intervalSince(now)
            
            let timeLeft = Int(dur.minutes)
            
            
            if timeLeft == 1 {
                timeLeftNotice = "\(timeLeft) min left or if you leave the area"
            } else if timeLeft <= 0 {
                createMarkCompleteView() // deletes when time expired (very static)
            } else {
                timeLeftNotice = "\(timeLeft) mins left or if you leave the area"
            }
        }
        
        
        noticeLabel.text = timeLeftNotice
        
        return noticeLabel
    }
    
    /// Create Text View for user task card
    ///
    /// Legacy comments:
    ///     Width 8.5/10
    func createUserTextView() -> UITextView {
        let textView = UITextView()
        textView.textColor = UIColor.white
        textView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        // turn off auto correction
        textView.autocorrectionType = .yes
        textView.showsVerticalScrollIndicator = false
        textView.showsHorizontalScrollIndicator = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        textView.backgroundColor = UIColor.clear
        textView.textAlignment = .left
        let fontSize = UIScreen.main.bounds.size.height <= 568 ? 20 : 22
        textView.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        textView.delegate = self
        textView.isScrollEnabled = false
        textView.tag = CURRENT_USER_TEXTVIEW_TAG
        textView.accessibilityIdentifier = "task_description_entry"
        
        return textView
    }
    
    /// Create close button when creating task
    func createCloseButton(container: UIView) -> UIButton {
        let buttonHeight = CGFloat.init(44.0)
        let bottomSpace = CGFloat.init(50.0)
        
        let closeView = UIButton(frame: CGRect(x: 0, y: container.bounds.height - bottomSpace, width: container.bounds.width * 1 / 2, height: buttonHeight))
        closeView.setImage(UIImage(named: "close"), for: .normal)
        closeView.addTarget(self, action: #selector(discardCurrentUserTask(sender:)), for: .touchUpInside)
        closeView.tag = CURRENT_USER_CANCEL_BUTTON
        if mTaskDescription != nil {
            closeView.alpha = 1
            closeView.isEnabled = true
        } else {
            closeView.alpha = 0.5
            closeView.isEnabled = false
        }
        
        // check if there are more than 1 card
        // if there is only 1 card disable the close button
        
        return closeView
    }
    
    
    /// Create donve view that says "Post" and allows user to post their message
    func createPostButton(container: UIView) -> UIButton {
        let buttonHeight = CGFloat.init(44.0)
        let bottomSpace = CGFloat.init(50.0)
        
        let doneView = UIButton(frame: CGRect(x: container.bounds.width * 1 / 2, y: container.bounds.height - bottomSpace, width: container.bounds.width * 1 / 2, height: buttonHeight))
        doneView.accessibilityIdentifier = "post"
        doneView.setTitle("Post", for: .normal)
        
        // disable the post button at first
        if mTaskDescription != nil {
            doneView.isEnabled = true
            doneView.alpha = 1
        } else {
            doneView.isEnabled = false
            doneView.alpha = 0.5
        }

        doneView.tag = POST_NEW_TASK_BUTTON_TAG
        // change the opacity to 0.5 for the button at first
        let fontSize = UIScreen.main.bounds.size.height <= 568 ? 20 : 24
        doneView.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        doneView.addTarget(self, action: #selector(createTaskForCurrentUser(sender:)), for: .touchUpInside)
        
        return doneView
    }
    
    /// Message Button from post owner's view
    func createUserReplyButton() -> UIButton {
        //Frame x is - 65 to try to get it to the center of the view, 55 is half of the button width and an extra 10 to get it a bit more center
        let messageView = UIButton()
        
        messageView.setImage(UIImage(named: "messageImage"), for: .normal)
        messageView.setTitle("Reply", for: .normal)
        messageView.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        messageView.addTarget(self, action: #selector(goToChat(sender:)), for: .touchUpInside)
        messageView.translatesAutoresizingMaskIntoConstraints = false
        
//        // invisible button??
//        let messageBtn = UIButton(
//            frame: CGRect(
//                x: 0,
//                y: 0,
//                width: container.bounds.width * 1 / 2,
//                height: container.bounds.height)
//        )
//        messageBtn.addTarget(self, action: #selector(goToChat(sender:)), for: .touchUpInside)
//        messageBtn.translatesAutoresizingMaskIntoConstraints = false
        
        return messageView
    }
    
    /// Done button to finish and save task
    func createDoneButton(container: UIView) -> UIButton {
        let doneButton = UIButton(
            frame: CGRect(
                x: container.frame.maxX - 45 - 20,
                y: container.bounds.height * 3 / 4,
                width: 45,
                height: 24)
        )
        doneButton.setTitle("Done", for: .normal)
        doneButton.accessibilityIdentifier = "mark_complete"
        doneButton.addTarget(self, action: #selector(markTaskAsComplete), for: .touchUpInside)
        
        return doneButton
    }
    
    /// Adding view count icon and label to task there were *just* made
    /// Only adding the label to tasks that the current user has posted
    func createViewCount(task: Task) -> (UIImageView, UILabel) {
        //View count icon
        let viewCountIcon = UIImageView()
        viewCountIcon.image = UIImage(named: "iconViews")
        viewCountIcon.translatesAutoresizingMaskIntoConstraints = false
        
        //default to 0 because all tasks will have 1 view at first (us) which we dont want to count
        var uniqueViews: Int = 0
        
        //view count number label
        let viewCountLabel = UILabel()
        getCurrentTaskViewCount(taskID: task.taskID!, completion: { count in
            uniqueViews = count - 1
            viewCountLabel.text = "\(uniqueViews)"
        })
        viewCountLabel.text = "\(uniqueViews)"
        viewCountLabel.font = UIFont(name: "SanFranciscoText-Regular", size: 12)
        viewCountLabel.textColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        viewCountLabel.tag = self.VIEW_COUNT_TAG
        viewCountLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return (viewCountIcon, viewCountLabel)
    }
    
    //MARK: - Other post's subviews
    
    /// Creates tasks description label
    func createTaskLabel(task: Task, index: Int) -> UILabel {
        let taskLabel = UILabel()
        taskLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        taskLabel.numberOfLines = 4
        taskLabel.text = task.taskDescription
        let fontSize = UIScreen.main.bounds.size.height <= 568 ? 20 : 22
        taskLabel.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        taskLabel.textColor = .white
        taskLabel.accessibilityIdentifier = "task_description\(index)"
        taskLabel.textAlignment = .left
        taskLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return taskLabel
    }
    
    /// Creates post reply button
    /// when pressed user will go to ChatVC of the attached post
    func createPostReplyButton(task: Task) -> UIButton {
        let messageButton = UIButton()
        
        var messageImage: UIImage?

        if task.completed {
            messageImage = #imageLiteral(resourceName: "completeMessage")
            messageButton.setTitle("See history", for: .normal)
        } else {
            if task.postType == "Sample Post" {
                messageButton.setTitle("Learn more", for: .normal)
            } else {
                messageButton.setTitle("Reply", for: .normal)
            }
            messageImage = UIImage(named: "iconChat") as UIImage?
        }
        
        if task.taskDescription == Constants.SAMPLE_POST.FIRST_POST_LIVE.desc {
            messageButton.addTarget(self, action: #selector(goToReferNewUserUI(sender:)), for: .touchUpInside)
        } else {
            messageButton.addTarget(self, action: #selector(goToChat(sender:)), for: .touchUpInside)
        }
        
        
        messageButton.setImage(messageImage, for: .normal)
        messageButton.imageView?.contentMode = .scaleAspectFit
        messageButton.setTitleColor(UIColor.darkGray, for: .highlighted)
        messageButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        messageButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        messageButton.alpha = 1.0
        messageButton.tintColor = UIColor.white
        messageButton.translatesAutoresizingMaskIntoConstraints = false
        
        return messageButton
    }
    
    /// Creates timer label e.g. "32 min left" or "completed 32 mins ago"
    func createTimerLabel(task: Task?) -> UILabel {
        let timerLabel = UILabel()
        timerLabel.numberOfLines = 1
        timerLabel.translatesAutoresizingMaskIntoConstraints = false
        timerLabel.minimumScaleFactor = 0.5
        timerLabel.textColor = UIColor.white
        timerLabel.alpha = 1.0
        timerLabel.font = UIFont(name: Constants.FONT_NAME, size: 13)
        
        var textTaskUpdatedTime = "Expires in 1 hr or if you leave the area"
        
        if let safeTask = task {
            // use Moment to get the time ago for task at current index
            let taskTimeUpdated = moment(safeTask.timeUpdated)
            let oneHr = moment(safeTask.timeUpdated + 3600)
            let dur = oneHr.intervalSince(taskTimeUpdated)
            let timeLeft = Int(dur.minutes)
            if safeTask.completed {
                textTaskUpdatedTime = "Completed \(taskTimeUpdated.fromNow())"
            } else {
                textTaskUpdatedTime = "\(timeLeft) minutes left"
            }
        }
        
        timerLabel.text = textTaskUpdatedTime
        
        return timerLabel
    }
    
    /// dismiss button for first post live sample post
    func createDismissButton() -> UIButton {
        let dismissButton = UIButton()
        dismissButton.setTitle("Dismiss", for: .normal)
        dismissButton.tintColor = .white
        dismissButton.addTarget(self, action: #selector(removeFirstPostLive), for: .touchUpInside)
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        
        return dismissButton
        
    }
    
    func createInvisibleButton() -> UIButton {
        return UIButton()
    }
    
    /// ShadowView container for every card
    func createShadowView(carousel: iCarousel) -> UIView {
        let viewWidth = UIScreen.main.bounds.size.height <= 568 ? 286 : 335

        let shadView = UIView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: Int((carousel.frame.size.height - 50) + 20)))
        shadView.backgroundColor = UIColor.clear
        shadView.layer.shadowColor = UIColor.black.cgColor
        shadView.layer.shadowOffset = CGSize(width: 0, height: 10)
        shadView.layer.shadowOpacity = 0.3
        shadView.layer.shadowRadius = 15.0

        return shadView
    }
    
    /// Create more options button (thee dots)
    func createMoreButton(_ task: Task) -> UIButton {
        let btn = UIButton()
        let threeDotsImage = UIImage(named: "moreButton") as UIImage?
//        btn.setTitle("MORE OPTIONS", for: .normal)
        btn.setImage(threeDotsImage, for: .normal)
        btn.imageView?.contentMode = .scaleToFill
//        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
//       btn.backgroundColor = .red
//        btn.alpha = 1.0
//        btn.tintColor = .white
        btn.addTarget(self, action: #selector(moreOptionsPressed), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        
        return btn
    }
    
    @objc func moreOptionsPressed(sender: UIButton) {
        let alert = UIAlertController()
        print("More Options Please!")
        
        alert.addAction(UIAlertAction(title: "Report", style: .default, handler: { action in
            self.goToReportPage() } )
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func goToReportPage() {
        let url = URL(string: "https://app.heymayo.com/report")
        safariVC = SFSafariViewController(url: url!)
        present(safariVC!, animated: true, completion: nil)
    }
}
