//
//  StringExtension.swift
//  Mayo-ios-client
//
//  Created by Lakshmi Kodali on 28/12/17.
//  Copyright © 2017 Weijie. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]
        let size = (self as NSString).size(withAttributes: convertToOptionalNSAttributedStringKeyDictionary(fontAttributes))
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]
        let size = (self as NSString).size(withAttributes: convertToOptionalNSAttributedStringKeyDictionary(fontAttributes))
        return size.height
    }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value) })
}
