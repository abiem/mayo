//
//  NotificationManager.swift
//  Mayo-ios-client
//
//  Created by Lakshmi Kodali on 27/12/17.
//  Copyright © 2017 Weijie. All rights reserved.
//

import Firebase
import Foundation
import UIKit
import UserNotifications

enum NotificationStatus {
    case allowedNotification
    case denied
    case notDetermined
}

func checkStatusOfNotification(_ result: @escaping (_ status: NotificationStatus) -> Void) {
    var status: NotificationStatus = .denied
    let current = UNUserNotificationCenter.current()

    current.getNotificationSettings(completionHandler: { settings in
        if settings.authorizationStatus == .notDetermined {
            status = .notDetermined
            result(status)
        }

        if settings.authorizationStatus == .denied {
            status = .denied
            result(status)
        }

        if settings.authorizationStatus == .authorized {
            status = .allowedNotification
            result(status)
        }
    })
}

func requestForNotification() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { granted, err in
        if granted {
            Analytics.logEvent(Constants.EVENT_AUTHORIZATION, parameters: [
                "permission": "notification",
            ])
        }
        if err != nil {
            print("Error requesting notification authorization: \(err!)")
        }
    })

    UIApplication.shared.registerForRemoteNotifications()
}
