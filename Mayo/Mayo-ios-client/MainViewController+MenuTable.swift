//
//  MainViewController+MenuTable.swift
//  Mayo-ios-client
//
//  Created by Jeremy Tandjung on 4/27/20.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        if indexPath.row == 0 {
            ReferralUIPresenter.showReferralUI(onViewController: self)
            return
        }
        
        var urlString = ""
        switch indexPath.row {
        case 1:
            urlString = "https://www.heymayo.com/mayo-feedback"
        case 2:
            urlString = "https://www.heymayo.com/faq"
        case 3:
            urlString = "https://www.heymayo.com"
        case 4:
            urlString = "https://www.heymayo.com/eula"
        default:
            urlString = "https://www.heymayo.com"
        }
        
        let url = URL(string: urlString)
        safariVC = SFSafariViewController(url: url!)
        present(safariVC!, animated: true, completion: nil)
    }
}

extension MainViewController: UITableViewDataSource {
    
    /// determine how many items in the table at a time
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    /// render each row/cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.selectionStyle = .none
        cell.textLabel?.text = "\(self.menuItems[indexPath.row])"
        cell.textLabel?.textColor = .white
//        cell.backgroundColor = UIColor(red: 0.24, green: 0.24, blue: 0.24, alpha: 1.00)
        cell.backgroundColor = Color.shared.accentGreen
        
        return cell
    }
    
    
}
