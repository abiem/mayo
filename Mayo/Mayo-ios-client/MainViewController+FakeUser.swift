//
//  MainViewController+FakeUser.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 24/04/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import MapKit

extension MainViewController {
    func createFakeUser() {
        // update current user's location
        getCurrentUserLocation()

        // generate random lat offet
        let randomLatOffset = generateRandomDegreesOffset()

        // generate random long offset
        let randomLongOffset = generateRandomDegreesOffset()

        // create new location coordinate
//        let newLoc = CLLocationCoordinate2D(latitude: userLatitude! + randomLatOffset, longitude: userLongitude! + randomLongOffset)
        let newLoc = CLLocationCoordinate2D(latitude: UserLocation.shared.getLocation()!.coordinate.latitude + randomLatOffset, longitude: UserLocation.shared.getLocation()!.coordinate.longitude + randomLongOffset)

        // Add Exipre Time for users
        let calendar = Calendar.current
        let minutesAgo = generateRandomNumber(endingNumber: 7)
        let date = calendar.date(byAdding: .minute, value: -minutesAgo, to: Date())

        // annotation for user markers
        addUserPin(latitude: newLoc.latitude, longitude: newLoc.longitude, userId: Constants.FAKE_USER_ID, updatedTime: date!)
    }

    // creates fake users nearby
    func createFakeUsers() {
        // get a random number from 2 to 5 for number of users
        let randomNumberOfUsers = generateRandomNumber(endingNumber: 4) + 2

        for _ in 1 ... randomNumberOfUsers {
            // call create fake users
            createFakeUser()
        }
    }

    func deleteFakeUser(_ annotation: MKAnnotation) {
        // removes annotation from mapview a user
        mapView.removeAnnotation(annotation)
    }
       
    // creates random degrees offset from .0001 to .001
    func generateRandomDegreesOffset() -> Double {
        let offset = Double(1 + generateRandomNumber(endingNumber: 10)) * 0.0001

        // create random number either 0 or 1
        let sign = generateRandomNumber(endingNumber: 2)

        // if sign is 0, return positive
        if sign == 0 {
            return offset
        } else {
            // else, if sign is 1 return negative
            return -offset
        }
    }

    // generate random number from 0 to endingNumber
    func generateRandomNumber(endingNumber: Int) -> Int {
        // creates random number between 0 and endingNumber
        // not including randomNumber
        let randomNum: UInt32 = arc4random_uniform(UInt32(endingNumber))
        return Int(randomNum)
    }
}
