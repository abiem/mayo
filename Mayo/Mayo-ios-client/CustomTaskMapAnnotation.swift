//
//  CustomTaskMapAnnotation.swift
//  Mayo-ios-client
//
//  Created by abiem  on 4/21/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import Cluster
import MapKit
import UIKit

class CustomTaskMapAnnotation: Annotation {
    var currentCarouselIndex: Int?
    var taskUserId: String?

    init(currentCarouselIndex: Int, taskUserId: String) {
        self.currentCarouselIndex = currentCarouselIndex
        self.taskUserId = taskUserId
    }
}
