//
//  UIView+style.swift
//  Mayo-ios-client
//
//  Created by Alex Hunsley on 06/05/2020.
//  Copyright © 2020 abiem design. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addBorder(color: UIColor) {
        layer.borderWidth = CGFloat(Constants.ALERT_VIEW_BORDER_WIDTH)
        layer.borderColor = color.cgColor
    }
    
    func roundCorners() {
        layer.cornerRadius = CGFloat(Constants.ALERT_VIEW_CORNER_RADIUS)
    }
}
