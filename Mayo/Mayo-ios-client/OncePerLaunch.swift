//
//  OncePerLaunch.swift
//  Mayo-ios-client
//
//  Created by Eric Wolak on 5/20/19.
//  Copyright © 2019 Factor13 LLC. All rights reserved.
//

import Foundation

class OncePerLaunch {
    static let shared = OncePerLaunch()
    private var flags = Dictionary<String, Bool>()
    
    func hasHappendThisLaunch(flag: String) -> Bool {
        if flags[flag] == nil {
            flags[flag] = true
            return false
        } else {
            return true
        }
    }
}
