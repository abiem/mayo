//
//  Global.swift
//  Mayo-ios-client
//
//  Created by star on 8/26/17.
//  Copyright © 2017 abiem. All rights reserved.
//

import UIKit

struct Constants {
    /// Firebase Cloud Server Key
    static let FIREBASE_CLOUD_SERVER_KEY = "AAAAYgVZ9lU:APA91bExQ_X8TFudFkv5_5VJ9E70YQ6uB6hlZgkQENBNCOOZl8e_EHsD-WUkGQ2pFz78qZwLtwPvA_kJRNJfYK6r_tpANKwrOn7ZJeeVmCoJBLyO-aqOPQYEncPD05-UleyFfkiVYsPh"

    static let NOTIFICATION_MESSAGE = 0
    static let NOTIFICATION_WERE_THANKS = 1
    static let NOTIFICATION_TOPIC_COMPLETED = 2
    static let NOTIFICATION_NEARBY_TASK = 3

    static let DEFAULT_LAT = 47.612441
    static let DEFAULT_LNG = -122.337463

	static let minMetersLocationAccuracy : Double = 25
	static let minMetersLocationAccuracyBackground : Double = 100
	static let minMetersBetweenLocations : Double = 15
	static let minMetersBetweenLocationsBackground : Double = 100
	static let minSecondsBetweenLocations : Double = 15
	
    static let THANKS_RIPPLE_ANIMATION_DURATION = 2.0
    static let THANKS_ANIMATION_DURATION = 5.0
    static let SECOND_PIN_ANIMATION_DURATION = 18.0
    static let THIRD_PIN_ANIMATION_DURATION = 22.0
    static let PENDING_TASKS = "PendingTask"
    static let LOCATION = "location"
    // Identiifier
    static let INTRO_COLLECTION_VIEW_CELL_IDENTIFIER = "introCell"

    static let FONT_NAME = "SanFranciscoText-Regular"

    static let STATUS_FOR_THANKED = "Owner marked as done and the people all thanked for"
    static let STATUS_FOR_NOT_HELPED = "Owner marked as done and no one helped"
    static let STATUS_FOR_TIME_EXPIRED = "Expired due to time limit"
    static let STATUS_FOR_MOVING_OUT = "Expired due to moving out of area"

    static let sLOCATION_ERROR = "Oh oh! We need your help with location to help us help you. You can enable location for Mayo in Settings"
    static let sNOTIFICATION_ERROR = "Thanks for the help! Do you want to be notified when you get a response back?"
    static let sTASK_CREATE_NOTIFICATION_ERROR = "Your post is ready! Do you want to be notified when others can help?"
    static let sTASK_EXPIRED_ERROR = "The post you’re looking for has completed."

    // Static ID
    static let FAKE_USER_ID = "fakeUser"

    static let INTRO_TITLE_ARRAY = [
        "Mayo cares deeply about the wellness and privacy of our users. To proceed, you must read and agree to the Terms of Use.",
        "Sometimes you need a little help from someone nearby. Or you want to lend a hand.",
        "Mayo makes helping the people around you easier, even when you can't see them.",
        "Mayo is proximity based. We need your location to show you nearby posts."
    ];

    static let INTRO_BUTTON_TITLE_ARRAY = ["I have read and agree", "OK", "Let's Get Started", "OK", "OK"]

    static let INTRO_LOCATION_ALERT_TITLE = "markdown:Choose **Allow While Using App** in the next pop up so we can notify you of nearby posts."

//    static let INTRO_LOCATION_ALERT_TITLE = "TIP: Mayo works best if you choose “Always share location”."
//    static let INTRO_LOCATION_ALERT_SUBTITLE = "We value your privacy and battery life so ‘Always’ simply allows us to periodically locate nearby help posts even if you forget to open the app : )"
//    static let INTRO_LOCATION_ALERT_BUTTON_TITLE = "Got it"

    // onboarding constants for standard user defaults.
    // depreciated; getFakeTasksCount still uses this
    // TODO: look at getFakeTasksCount
    static let ONBOARDING_TASK1_VIEWED_KEY = "onboardingTask1Viewed"
    static let ONBOARDING_TASK2_VIEWED_KEY = "onboardingTask2Viewed"
    static let ONBOARDING_TASK3_VIEWED_KEY = "onboardingTask3Viewed"
    static let ONBOARDING_HISTORY_BOOKMARK_VIEWED_KEY = "historyBookMarkViewed"

    static let REFERRAL_TITLE_FONT_SIZE = 28
    static let REFERRAL_MEDIUM_FONT_SIZE = 22
    static let REFERRAL_SMALL_FONT_SIZE = 18
    
    static let COMPOSE_LONG_PLACEHOLDER_VIEWED = "longPhViewed"
    
    static let COMPOSE_LONG_PLACEHOLDER = "Your post is always on the ver left. Tap here to type your first post. 🤜🏻✨🤛🏻."
    
    static let COMPOSE_SHORT_PLACEHOLDER = "Type your post here"
    

    /// For onboarding sample post
    /// - fakeId      : used to access UserDefault bool and addSamplePost param
    /// - shown     : user default key to  determine 
    /// - desc : used for task desc
    struct SAMPLE_POST {
        /// First sample post: Example task/post
        struct EXAMPLE_TASK {
            static let fakeId = "fakeUserId1"
            static let shown = "exampleTaskShown"
            static let viewed = "exampleTaskViewed"
            static let desc = "Mayo posts from other users will look like this. You can respond to each post. Tap learn more to try it out."
        }
        /// Second sample post: swipe left to user task
        struct SWIPE_LEFT {
            static let fakeId = "fakeUserId2"
            static let shown = "swipeLeftShown"
            static let viewed = "swipeLeftViewed"
            static let desc = "Swipe to the left 👈🏻 to create your own post."
        }
        /// Third sample post: shown after user posts first ever post
        struct FIRST_POST_LIVE {
            static let fakeId = "fakeUserId3"
            static let shown = "firstPostLiveShown"
            static let viewed = "firstPostLiveViewed"
            static let desc = "Your first post is live!\nMayo works better together. Learn how to invite others to join Mayo. Help us help others."
        }
        /// Fourth sample post: shown after install if there's any history posts around the area
        struct HISTORY_TITLE {
            static let fakeId = "fakeUserId4"
            static let shown = "historyTitleShown"
            static let viewed = "historyTitleViewed"
            static let desc = "Past posts in your area are always grey, like this. You can see what you missed, but you can't respond in the post👉🏻."
        }
        
    }
    
    // array of color hex colors for chat bubbles
    static let chatBubbleColors = [
        "C2C2C2", // task owner's bubble color gray
        "08BBDB",
        "FC8FA3",
        "9CD72F",
        "ED801F",
        "B664C4",
        "4A4A4A",
        "4FB5B2",
        "2F96FF",
        "E86D5D",
        "1DAE73",
        "AC664C",
        "508FBC",
        "BCCB4C",
        "7C3EC1",
        "D36679",
        "5AC7CF",
        "CAA63C",
    ]

    static let ALERT_VIEW_CORNER_RADIUS = 8
    static let ALERT_VIEW_BORDER_WIDTH = 2

    // Firebase Analytics events

    // Emitted when a user authorizes a particular permission for the app
    static let EVENT_AUTHORIZATION = "user_authorization"
	
	static let CURRENT_USER_ID = "currentUserId"
    static let CURRENT_USER_TOKEN = "currentUserToken"

    static let REFRESH_TOKEN = "refresh_token"

    static let USER_WAS_REFERRED_BY_USER_ID = "userWasReferredByUserID"
}

struct ReferralConstants {
    static let REFERRAL_URL_PARAMATER = "referral_program"
}

enum locationIconTime: Int {
    case first = 60 // active users
    case second = 120
    case third = 180
    case fourth = 240
    case fifth = 300
    case sixth = 360 // in active users
}
